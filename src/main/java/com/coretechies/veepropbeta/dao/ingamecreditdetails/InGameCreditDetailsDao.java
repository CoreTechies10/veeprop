/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.ingamecreditdetails;

import com.coretechies.veepropbeta.domain.InGameTraderDetail;

/**
 *
 * @author Noppp
 */
public interface InGameCreditDetailsDao {

    public void saveIGCD(InGameTraderDetail igtd);

    public InGameTraderDetail retrieveIGCD(Integer identificationNumber);

    public void updateIGTD(InGameTraderDetail IGTD);
    
    public void updateConvertIGC(Double vCredit,Double IGC,int UserId);
    
    public void purchaseShare(Double IGC,Integer shareHold,int shareId);

    public void tripleShare();
}
