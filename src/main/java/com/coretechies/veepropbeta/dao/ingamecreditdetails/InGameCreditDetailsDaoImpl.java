/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.ingamecreditdetails;

import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("InGameCreditDetailsDao")
public class InGameCreditDetailsDaoImpl implements InGameCreditDetailsDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveIGCD(InGameTraderDetail igtd) {
        try{
            entityManager.persist(igtd);
            logger.info("Game Credit Details Added Successfully");
        }catch(Exception e){
            logger.info("Game Credit Details Added Failed:"+e.getStackTrace());
        }
    }

    @Override
    public InGameTraderDetail retrieveIGCD(Integer identificationNumber) {
        TypedQuery<InGameTraderDetail> query = entityManager.createQuery("select i from InGameTraderDetail i where i.identificationNumber=:identificationNumber", InGameTraderDetail.class);
        query.setParameter("identificationNumber", identificationNumber);
        return query.getSingleResult();
    }

    @Override
    @Transactional(readOnly = false)
    public void updateIGTD(InGameTraderDetail IGTD) {
        try{
            entityManager.merge(IGTD);
        }catch(Exception e){
            logger.info("error in update is:"+e.getStackTrace());
        }
    }
    @Override
    @Transactional(readOnly = false)
    public void updateConvertIGC(Double vCredit, Double IGC, int UserId) {
         InGameTraderDetail game=retrieveIGCD(UserId);
         game.setvCredit((double)vCredit);
         game.setIGC((double)IGC);
        try
        {
            entityManager.merge(game);
            
        }
        catch(Exception e)
        {
            logger.info("Error"+e);
        }
    }
    @Override
    @Transactional
    public void purchaseShare(Double IGC, Integer shareHold, int shareId) {
        
        InGameTraderDetail ShareGame=retrieveIGCD(shareId);
        ShareGame.setIGC((double)IGC);
        ShareGame.setShareHold(shareHold);
        try{
            entityManager.merge(ShareGame);
            logger.info("Update purchase Share part");
        }catch(Exception e3){
            logger.info("Error this time Update Share part"+e3);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void tripleShare() {
        Query query = entityManager.createQuery("UPDATE InGameTraderDetail i SET i.shareHold=i.shareHold*3");
        query.executeUpdate();
    }   
}
