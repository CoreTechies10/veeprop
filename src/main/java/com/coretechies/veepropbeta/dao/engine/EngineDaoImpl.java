/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.engine;

import com.coretechies.veepropbeta.domain.Engine;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository("EngineDao")
public class EngineDaoImpl implements EngineDao {
   
    private final Logger logger =LoggerFactory.getLogger(getClass());
    
    
    private EntityManager entitymanager;
  
    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager) {
        this.entitymanager = entitymanager;
    }
    
    @Override
    public Engine retieveAll(Engine engine) {
        return entitymanager.find(Engine.class,"");
    }
    
    @Override
    public Engine updateEngine(Engine engine) {
        return null;
    }
    
}
