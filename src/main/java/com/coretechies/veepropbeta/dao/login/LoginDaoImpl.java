/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.login;

import com.coretechies.veepropbeta.domain.Login;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("LoginDao")
public class LoginDaoImpl implements LoginDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Boolean isCredential(Login login) {
        Login loginCheck = retrieveUser(login.getUserName());
        if (loginCheck == null) {
            logger.info("User failed in DAO user " + login.getUserName() + "Not found|");
            return false;
        }
        
        return loginCheck.getPassword().compareTo(login.getPassword()) == 0;
    }
    
    @Override
    public Login retrieveUser(String userName) {
        TypedQuery<Login> query = entityManager.createQuery(
                "select l from Login l where l.userName=:userName", Login.class);
        query.setParameter("userName", userName);
        try {
            return query.getSingleResult();
        } catch (Exception exc) {
            logger.error("User failed in DAO user count not retrieve user " +
                    userName + " " + exc.getMessage());
            return null;
        }    
    }

    @Transactional(readOnly = false)
    @Override
    public void saveTraderCredentials(Login log) {
        try{
            entityManager.persist(log);
            logger.info("Loggin Details Added Successfully");
        }catch(Exception e){
            logger.info("Loggin Details Added Failed:"+e.getStackTrace());
        }
    }
    
    @Override
    public Long countUser(String userRole){
        TypedQuery<Long> query = entityManager.createQuery("select count(l) from Login l where l.userRole=:userRole",Long.class);
        query.setParameter("userRole",userRole);
        Long count = query.getSingleResult();
        logger.info("trader is:"+count);
        return count;
    }

    @Override
    public List<Integer> retrieveUserByRole(String userRoleValue) {
        TypedQuery<Integer> query = entityManager.createQuery("Select l.identificationNumber from Login l where l.userRole=:userRoleValue", Integer.class);
        query.setParameter("userRoleValue", userRoleValue);
        return query.getResultList();
    }

    @Override
    public Login retrieveUser(Integer buyerIN) {
        TypedQuery<Login> query = entityManager.createQuery("select l from Login l where l.identificationNumber=:buyerIN",Login.class);
        query.setParameter("buyerIN", buyerIN);
        return query.getSingleResult();
    }

    @Override
    @Transactional
    public void updateLogin(Login login) {
        try{
            entityManager.merge(login);
        }catch(Exception e){
            logger.info("Error in Login Update is:"+e.getStackTrace());
        }
    }
}
