/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.dispute;

import com.coretechies.veepropbeta.domain.Dispute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("DisputeDao")
public class DisputeDaoImpl implements DisputeDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveDispute(Dispute dispute) {
        try{
            entityManager.persist(dispute);
        }catch(Exception e){
            logger.info("error in dispute save is as :" +e.getStackTrace());
        }
    }

    @Override
    public Dispute retrieveDisputeForModByVCT(Integer vctId) {
        try{
            TypedQuery<Dispute> query = entityManager.createQuery("select d from Dispute d where d.veeCreditTransactionId=:vctId", Dispute.class);
            query.setParameter("vctId", vctId);
            return query.getSingleResult();
        }catch(Exception e){
            logger.info("error is:"+e.getStackTrace());
            return null;
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteDispute(Dispute dispute) {
        try{
            Dispute disputeNew = retrieveDisputeForModByVCT(dispute.getVeeCreditTransactionId());
            entityManager.remove(disputeNew);
        }catch(Exception e){
            logger.info("error in dispute Delete is:"+e.getStackTrace());
        }
    }
}
