/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.dispute;

import com.coretechies.veepropbeta.domain.Dispute;

/**
 *
 * @author Noppp
 */
public interface DisputeDao {

    public void saveDispute(Dispute dispute);

    public Dispute retrieveDisputeForModByVCT(Integer vctId);

    public void deleteDispute(Dispute dispute);
    
}
