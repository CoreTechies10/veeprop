/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.announcement;
import com.coretechies.veepropbeta.domain.Announcement;
import com.coretechies.veepropbeta.dao.announcement.announcementDao;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rohit
 */
@Repository("announcementDao")
public class announcementDaoImpl implements announcementDao
{
    private final Logger logger= LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    
    @Override
    @Transactional
    public List<Announcement> Retriveannouncement(Date date) 
    {
       TypedQuery<Announcement> query=entityManager.createQuery("SELECT e FROM Announcement e where e.expiryDate>:date",Announcement.class);
      query.setParameter("date", date);
       return query.getResultList();
    }

    @Override
    @Transactional(readOnly = false)
    public void saveAnnouncement(Announcement announcement) {
        try{
            entityManager.persist(announcement);
        }catch(Exception e){
            logger.info("error creating announcement is:"+e.getStackTrace());
        }
    }
     
}
