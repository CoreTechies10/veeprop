/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.transaction;

import com.coretechies.veepropbeta.domain.Transaction;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("TransactionDao")
public class TransactionDaoImpl implements TransactionDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveTransaction(Transaction transaction) {
        try{
            entityManager.persist(transaction);
        }catch(Exception e){
            logger.info("Error in transaction submission is:"+e.getStackTrace());
        }
    }

    @Override
    public Transaction retrieveTransaction(Integer transactionId) {
        return entityManager.find(Transaction.class, transactionId);
    }

    @Override
    @Transactional(readOnly = false)
    public void updateTransaction(Transaction transaction) {
        try{
            entityManager.merge(transaction);
        }catch(Exception e){
            logger.info("error in update Transaction is:"+e.getStackTrace());
        }
    }
    
    @Override
    public List<Transaction> retrieveSellTransactionByDates(int indentificationNumber, Date initialDate, Date endDate,int transactionType) {
        TypedQuery<Transaction> query=entityManager.createQuery("Select t from Transaction t where t.sellerIN=:indentificationNumber and t.TransactionDate >=:initialDate and t.TransactionDate<=:endDate and t.TransactionType=:transactionType",Transaction.class);
        query.setParameter("indentificationNumber",indentificationNumber);
        query.setParameter("initialDate", initialDate);
        query.setParameter("endDate",endDate);
        query.setParameter("transactionType", transactionType);
        return query.getResultList();
        
    }

    @Override
    public List<Transaction> retrieveBuyTransactionByDates(int indentificationNumber, Date initialDate, Date endDate,int transactionType) {
        TypedQuery<Transaction> query=entityManager.createQuery("Select t from Transaction t where t.buyerIN=:indentificationNumber and t.TransactionDate >=:initialDate and t.TransactionDate<=:endDate and t.TransactionType=:transactionType",Transaction.class);
        query.setParameter("indentificationNumber",indentificationNumber);
        query.setParameter("initialDate", initialDate);
        query.setParameter("endDate",endDate);
        query.setParameter("transactionType", transactionType);
        return query.getResultList();  
    }

    @Override
    public List<Transaction> retrieveTransactionsPastTenDates(Date date) {
        TypedQuery<Transaction> query = entityManager.createQuery("select t from Transaction t where t.TransactionDate <=:date", Transaction.class);
        query.setParameter("date", date);
        return query.getResultList();
    }
}
