/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.sharetradingtransaction;

import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("ShareTradingTransactionDao")
public class ShareTradingTransactionDaoImpl implements ShareTradingTransactionDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveShareTrade(ShareTradingTransaction shareTradingTransaction) {
        try{
            entityManager.persist(shareTradingTransaction);
        }catch(Exception e){
            logger.info("error in share trading transaction is:"+e.getStackTrace());
        }
    } 

    @Override
    public List<ShareTradingTransaction> retrievePendingTransaction(int transactionStatusValue) {
        TypedQuery<ShareTradingTransaction> query = entityManager.createQuery("select s from ShareTradingTransaction s where s.transactionStatus=:transactionStatusValue", ShareTradingTransaction.class);
        query.setParameter("transactionStatusValue", transactionStatusValue);
        try{
            return query.getResultList();
        }catch(Exception e){
            logger.info("error in this is:"+e.getMessage());
            return null;
        }
    }

    @Override
    public ShareTradingTransaction retrieveSTT(Integer shareTransactionId) {
        return entityManager.find(ShareTradingTransaction.class, shareTransactionId);
    }

    @Override
    @Transactional(readOnly = false)
    public void updateSTT(ShareTradingTransaction STT) {
        try{
            entityManager.merge(STT);
        }catch(Exception e){
            logger.info("error in share transaction update is:"+e.getStackTrace());
        }
    }

    @Override
    public ShareTradingTransaction retrieveSTTByTransaction(Integer transactionId) {
        TypedQuery<ShareTradingTransaction> query = entityManager.createQuery("Select s from ShareTradingTransaction s where s.shareTradingTransactionToken=:transactionId",ShareTradingTransaction.class);
        query.setParameter("transactionId", transactionId);
        return query.getSingleResult();       
    }
}
