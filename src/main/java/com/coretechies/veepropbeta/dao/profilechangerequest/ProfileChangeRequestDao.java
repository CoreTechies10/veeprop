/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.profilechangerequest;

import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import java.util.List;

/**
 *
 * @author Arvind
 */
public interface ProfileChangeRequestDao {
    
    public void SavePCR(ProfileChangeRequest pcr);
    
    public ProfileChangeRequest retievePCR(Integer identificationNumber);

    public List<ProfileChangeRequest> retieveAllPendingRequest(Integer identificationNumber);

    public void deletePCRS(ProfileChangeRequest retievePCR);

    public void updatePCRS(ProfileChangeRequest retievePCR);

    public List<ProfileChangeRequest> retieveAllApprovedRequest(Integer moderatorIN);
    
   
}
