
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.profilechangerequest;

import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("ProfileChangeRequestDao")
 public class ProfileChangeRequestDaoImpl implements ProfileChangeRequestDao {
    
    private final Logger logger =LoggerFactory.getLogger(getClass());
    private EntityManager entityManager;
    
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
    @Transactional
    public void SavePCR(ProfileChangeRequest pcr) {
     try{
        entityManager.merge(pcr);
        logger.info("profile change request");
     }catch(Exception e){
        logger.info("Error Profile Request Change");
     }
   }
    public ProfileChangeRequest retievePCR(Integer identificationNumber) {
      try{
        TypedQuery<ProfileChangeRequest> query=entityManager.createQuery("Select p from ProfileChangeRequest p where p.IdentificationNumber=:identificationNumber",ProfileChangeRequest.class);
        query.setParameter("identificationNumber", identificationNumber);
          return query.getSingleResult();
      }catch(Exception e){
          return null;
      }
    }

    @Override
    public List<ProfileChangeRequest> retieveAllPendingRequest(Integer identificationNumber) {
        TypedQuery<ProfileChangeRequest> query = entityManager.createQuery("select p from ProfileChangeRequest p where p.requestStatus=:status and p.verifiedBy=:identificationNumber", ProfileChangeRequest.class);
        query.setParameter("status", Boolean.FALSE);
        query.setParameter("identificationNumber", identificationNumber);
        return query.getResultList();
    }

    @Override
    public List<ProfileChangeRequest> retieveAllApprovedRequest(Integer identificationNumber) {
        TypedQuery<ProfileChangeRequest> query = entityManager.createQuery("select p from ProfileChangeRequest p where p.requestStatus=:status and p.verifiedBy=:identificationNumber", ProfileChangeRequest.class);
        query.setParameter("status", Boolean.TRUE);
        query.setParameter("identificationNumber", identificationNumber);
        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = false)
    public void deletePCRS(ProfileChangeRequest retievePCR) {
        ProfileChangeRequest PCR = retievePCR(retievePCR.getIdentificationNumber());
        try{
            entityManager.remove(PCR);
        }catch(Exception e){
            logger.info("Error in delete PCRS is"+e.getStackTrace());
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void updatePCRS(ProfileChangeRequest retievePCR) {
        try{
            entityManager.merge(retievePCR);
        }catch(Exception e){
            logger.info("Error in delete PCRS is"+e.getStackTrace());
        }        
    }    
}
