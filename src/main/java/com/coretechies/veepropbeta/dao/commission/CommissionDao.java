/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.commission;

import com.coretechies.veepropbeta.domain.Commission;

/**
 *
 * @author Noppp
 */
public interface CommissionDao {

    public void saveCommission(Commission commission);
    
}
