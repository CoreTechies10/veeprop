/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.verificationmoderator;

import com.coretechies.veepropbeta.domain.VerificationModerator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Noppp
 */
@Repository("VerificationModeratorDao")
public class VerificationModeratorDaoImpl implements VerificationModeratorDao{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveVerificationModerator(VerificationModerator verificationModerator) {
        try{
            entityManager.persist(verificationModerator);
        }catch(Exception e){
            logger.info("Error in Save verify request is:"+e.getStackTrace());
        }
    }

    @Override
    public VerificationModerator retrieveVM(Integer transactionId) {
        TypedQuery<VerificationModerator> query = entityManager.createQuery("Select v from VerificationModerator v where v.transactionId=:transactionId", VerificationModerator.class);
        query.setParameter("transactionId", transactionId);
        return  query.getSingleResult();
    }

    @Override
    @Transactional(readOnly = false)
    public void updateVM(VerificationModerator retrieveVM) {
        try{
            entityManager.merge(retrieveVM);
        }catch(Exception e){
            logger.info("error in updation VM is:"+e.getStackTrace());
        }
    }
    
    @Override
    @Transactional(readOnly = false)
    public void deleteVM(VerificationModerator retrieveVM) {
        VerificationModerator vm = retrieveVM(retrieveVM.getTransactionId());
        try{
            entityManager.remove(vm);
        }catch(Exception e){
            logger.info("error in deletion VM is:"+e.getStackTrace());
        }
    }    

    @Override
    public List<VerificationModerator> retrieveVMApprovedByModerator(Integer moderatorIN) {
        TypedQuery<VerificationModerator> query = entityManager.createQuery("select v from VerificationModerator v where "
                + "v.verificationStatus=:complete and v.identificationNumber=:moderatorIN", VerificationModerator.class);
        query.setParameter("moderatorIN", moderatorIN);
        query.setParameter("complete", Boolean.TRUE);
        return query.getResultList();
    }
    
}
