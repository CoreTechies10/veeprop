/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.feedback;
import com.coretechies.veepropbeta.domain.Feedback;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author Rohit
 */
@Repository("FeedbackDao")
public class FeedbackDaoImpl implements FeedbackDao{
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private EntityManager entityManager;
     
    @PersistenceContext
    public void setEm(EntityManager entityManager){
        this.entityManager = entityManager;
    }
    @Override
    @Transactional
    public void saveFeedBack(Feedback feedBack,int feedBackId){
      try{
         feedBack.setIdentificationNumber(feedBackId);
         entityManager.persist(feedBack);
     }
     catch(Exception e){
       logger.info("Error in feedback form" +e);
     }
  }

    @Override
    public List<Feedback> retrieveFeedBack() {
        TypedQuery<Feedback> query = entityManager.createQuery("select f from Feedback f",Feedback.class);
        return query.getResultList();
    }
    
}
