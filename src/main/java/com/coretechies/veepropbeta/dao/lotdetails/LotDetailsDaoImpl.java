/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.lotdetails;

import com.coretechies.veepropbeta.dao.ingamecreditdetails.InGameCreditDetailsDao;
import com.coretechies.veepropbeta.domain.Engine;
import com.coretechies.veepropbeta.domain.GenerationDetails;
import com.coretechies.veepropbeta.domain.LotDetails;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("LotDetailsDao")
public class LotDetailsDaoImpl implements LotDetailsDao {
    
    private final Logger  logger=LoggerFactory.getLogger(getClass());
    
    private EntityManager entitymanager;
    
    @Autowired
    private InGameCreditDetailsDao inGameCreditDetailsDao;
    
    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager) {
        this.entitymanager = entitymanager;
    }
    @Override
    @Transactional
    public Integer retrieveDetail() {
        try{
            TypedQuery<Integer> query = entitymanager.createQuery("select  max(l.lotNumber) from LotDetails l", Integer.class);
            return query.getSingleResult();
        }
        catch(Exception e){
            logger.info("Error"+e.getStackTrace()); 
        }
        return 0; 
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateLotDetail(LotDetails lotdetail) {
        try{
            lotdetail.setEndDate(new Date());
            lotdetail.setEndTime(new Date(System.currentTimeMillis()));
            entitymanager.merge(lotdetail);
            
            LotDetails lotDetails = new LotDetails();
            lotDetails.setNumberOfShare(300000);
            
            Engine engine = entitymanager.find(Engine.class, 1);
            
            if(lotdetail.getLotNumber()%41!=0){
                lotDetails.setPricePerShare((float)lotdetail.getPricePerShare()+(float).01);
                lotDetails.setGenerationNumber(lotdetail.getGenerationNumber());
                
                engine.setGenerationNumber(lotDetails.getGenerationNumber());
            }else{
                lotDetails.setPricePerShare((float)0.20);
                
                GenerationDetails generationDetails = new GenerationDetails();
                generationDetails.setReleaseDate(new Date());
                generationDetails.setReleaseTime(new Date(System.currentTimeMillis()));
                entitymanager.persist(generationDetails);
                
                engine.setGenerationNumber(generationDetails.getGenerationNumber());
                
                engine.setShareSoldInGeneration(0);
                
                inGameCreditDetailsDao.tripleShare();
                
                GenerationDetails find = entitymanager.find(GenerationDetails.class, generationDetails.getGenerationNumber()-1);
                find.setEndTime(new Date(System.currentTimeMillis()));
                find.setEndDate(new Date());
                entitymanager.merge(find);
                
                lotDetails.setGenerationNumber(generationDetails.getGenerationNumber());
            }
            
            lotDetails.setReleaseDate(new Date());
            lotDetails.setReleaseTime(new Date(System.currentTimeMillis()));
            entitymanager.persist(lotDetails);
            
            engine.setShareAvailable(lotDetails.getNumberOfShare());
            engine.setLotNumber(lotDetails.getLotNumber());
            entitymanager.merge(engine);
        }catch(Exception e4){
            logger.info("Error update lotDetail"+e4);
        }
    }
    
    @Transactional
    @Override
    public LotDetails retrieveLotDetail() {
        Integer retrieveDetail = retrieveDetail();
        try{
            TypedQuery<LotDetails> query=entitymanager.createQuery("SELECT L FROM LotDetails L WHERE L.lotNumber=:LotNumber", LotDetails.class);
            query.setParameter("LotNumber", retrieveDetail);
            return query.getSingleResult();
        }catch(Exception e3){
            logger.info("Error"+e3);
            return null;
        }
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateShareCount(LotDetails lot,Integer shareSold) {
        try{
            entitymanager.merge(lot);
            if(lot.getNumberOfShare()<=0){
                updateLotDetail(lot);
            }
            Engine engine = entitymanager.find(Engine.class, 1);
            engine.setShareAvailable(lot.getNumberOfShare());
            engine.setShareSoldInGeneration(engine.getShareSoldInGeneration()+shareSold);
            
            entitymanager.merge(engine);
        }catch(Exception e3){
            logger.info("Error"+e3);
        }
    }

    @Override
    public List<LotDetails> retrieveAllLotEntries() {
        TypedQuery<LotDetails> query = entitymanager.createQuery("select l from LotDetails l", LotDetails.class);
        return query.getResultList();
    }
}
