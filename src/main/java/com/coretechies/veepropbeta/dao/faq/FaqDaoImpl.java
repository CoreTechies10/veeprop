/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.faq;

import com.coretechies.veepropbeta.domain.Faq;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("FaqDao")
public class FaqDaoImpl implements FaqDao {

    private final Logger logger=LoggerFactory.getLogger(getClass());
    
    private EntityManager entitymanager;
    
    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager) {
        this.entitymanager = entitymanager;
    }
    
    
    @Override
    @Transactional
    public void saveFaqData(Faq faq) {
        try{  
            entitymanager.persist(faq);
            logger.info("Save Faq Data in Faq Table");
        }catch(Exception e1){
            logger.info("Error At that Time SaveFaqData"+e1);
        }
    }

    @Override
    public List<Faq> retrieveFAQs() {
        TypedQuery<Faq> query = entitymanager.createQuery("select f from Faq f", Faq.class);
        return query.getResultList();
    }
    
}
