/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.dao.profile;

import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author Rohit
 */
@Repository("ProfileDao")
public class profileDaoImpl implements ProfileDao
{
    private final Logger logger= LoggerFactory.getLogger(getClass());
    
    private EntityManager entitymanager;
    
    @PersistenceContext
    public void setEntitymanager(EntityManager entitymanager){
        this.entitymanager = entitymanager;
    }
    
    @Override
    @Transactional
    public User retrivePersonal(Integer identificationNumber){     
        TypedQuery<User> query = entitymanager.createQuery("Select u from User u where u.IdentificationNumber=:identificationNumber",User.class);
        query.setParameter("identificationNumber", identificationNumber);
        return query.getSingleResult();
    }
    
    @Override
    public UserAddressInfo retriveAddress(Integer identificationNumber){
        TypedQuery<UserAddressInfo> query=entitymanager.createQuery("Select a From UserAddressInfo a where a.IdentificationNumber=:identificationNumber", UserAddressInfo.class);
        query.setParameter("identificationNumber", identificationNumber);
        return query.getSingleResult();
    }

    @Override
    public UserBankDetail retriveBank(Integer identificationNumber){
        TypedQuery<UserBankDetail> query=entitymanager.createQuery("Select b From UserBankDetail b where b.identificationNumber=:identificationNumber", UserBankDetail.class);
        query.setParameter("identificationNumber",identificationNumber);
        return query.getSingleResult();
    }

    @Override
    @Transactional(readOnly = false)
    public void addAddresssInfo(UserAddressInfo userAddressinfo) {
        try{
            entitymanager.persist(userAddressinfo);
        }catch(Exception e){
            logger.info("error in add address is:"+e.getStackTrace());
        }
    }

    @Override
    @Transactional(readOnly = false)
    public void updateProfile(UserAddressInfo address, User personal) {
        try{
            entitymanager.merge(address);
            entitymanager.merge(personal);
        }catch(Exception e){
            logger.info("error in profile update is"+e.getStackTrace());
        }
    }
    
}
