/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.ingamecreditdetails;

import com.coretechies.veepropbeta.dao.ingamecreditdetails.InGameCreditDetailsDao;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class InGameCreditDetailsServiceImpl implements InGameCreditDetailsService{
    
    private final Logger logger =LoggerFactory.getLogger(getClass());
    
    @Autowired
    private InGameCreditDetailsDao inGameCreditDetailsDao;

    @Override
    public void saveIGCD(InGameTraderDetail igtd) {
        inGameCreditDetailsDao.saveIGCD(igtd);
    }   

    @Override
    public InGameTraderDetail retrieveIGCD(Integer identificationNumber) {
        return inGameCreditDetailsDao.retrieveIGCD(identificationNumber);
    }

    @Override
    public void updateIGTD(InGameTraderDetail IGTD) {
        inGameCreditDetailsDao.updateIGTD(IGTD);
    }

    @Override
    public void updateConvertIGC(Double vCredit,Double IGC,int UserId) {
        inGameCreditDetailsDao.updateConvertIGC(vCredit, IGC, UserId);
    }

    @Override
    public void purchaseShare(Double IGC, Integer shareHold, int shareId) {
        inGameCreditDetailsDao.purchaseShare(IGC, shareHold, shareId);
    }
}
