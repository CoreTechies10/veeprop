/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.verificationmoderator;

import com.coretechies.veepropbeta.dao.verificationmoderator.VerificationModeratorDao;
import com.coretechies.veepropbeta.domain.VerificationModerator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class VerificationModeratorServiceImpl implements VerificationModeratorService{
    
    @Autowired
    private VerificationModeratorDao verificationModeratorDao;

    @Override
    public void saveVerificationModerator(VerificationModerator verificationModerator) {
        verificationModeratorDao.saveVerificationModerator(verificationModerator);
    }

    @Override
    public VerificationModerator retrieveVM(Integer transactionId) {
        return verificationModeratorDao.retrieveVM(transactionId);
    }

    @Override
    public void updateVM(VerificationModerator retrieveVM) {
        verificationModeratorDao.updateVM(retrieveVM);
    }
    
    @Override
    public void deleteVM(VerificationModerator retrieveVM) {
        verificationModeratorDao.deleteVM(retrieveVM);
    }

    @Override
    public List<VerificationModerator> retrieveVMApprovedByModerator(Integer moderatorIN) {
        return verificationModeratorDao.retrieveVMApprovedByModerator(moderatorIN);
    }
}
