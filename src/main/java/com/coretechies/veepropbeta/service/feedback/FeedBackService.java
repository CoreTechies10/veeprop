/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.feedback;

import com.coretechies.veepropbeta.domain.Feedback;
import java.util.List;

/**
 *
 * @author Arvind
 */
public interface FeedBackService {
    
    public void saveFeedBack(Feedback feedBack,int feedBackId);

    public List<Feedback> retrieveFeedBack();
    
}
