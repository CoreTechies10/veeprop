/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.veecredittransaction;

import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface VeeCreditTransactionService {

    public void saveVCT(VeeCreditTransaction vct);

    public List<VeeCreditTransaction> retrievePendingTransaction(int transactionStatusValue);

    public VeeCreditTransaction retrieveVCT(Integer veeCreditTransactionId);

    public void updateVCT(VeeCreditTransaction VCT); 

    public VeeCreditTransaction retrieveVCTByTransaction(Integer transactionId);
}
