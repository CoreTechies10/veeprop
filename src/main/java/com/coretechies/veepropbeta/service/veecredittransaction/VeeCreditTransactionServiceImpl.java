/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.veecredittransaction;

import com.coretechies.veepropbeta.dao.veecredittransaction.VeeCreditTransactionDao;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class VeeCreditTransactionServiceImpl implements VeeCreditTransactionService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private VeeCreditTransactionDao veeCreditTransactionDao;
    
    @Override
    public void saveVCT(VeeCreditTransaction vct) {
        veeCreditTransactionDao.saveVCT(vct);
    }

    @Override
    public List<VeeCreditTransaction> retrievePendingTransaction(int transactionStatusValue) {
        return veeCreditTransactionDao.retrievePendingTransaction(transactionStatusValue);
    }

    @Override
    public VeeCreditTransaction retrieveVCT(Integer veeCreditTransactionId) {
        return veeCreditTransactionDao.retrieveVCT(veeCreditTransactionId);
    }

    @Override
    public void updateVCT(VeeCreditTransaction VCT) {
        veeCreditTransactionDao.updateVCT(VCT);
    }

    @Override
    public VeeCreditTransaction retrieveVCTByTransaction(Integer transactionId) {
       return veeCreditTransactionDao.retrieveVCTByTransaction(transactionId);
    }

}
