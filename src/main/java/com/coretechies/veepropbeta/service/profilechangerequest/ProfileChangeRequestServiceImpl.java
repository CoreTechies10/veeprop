
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.profilechangerequest;

import com.coretechies.veepropbeta.dao.profilechangerequest.ProfileChangeRequestDao;
import com.coretechies.veepropbeta.domain.ProfileChangeRequest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileChangeRequestServiceImpl implements ProfileChangeRequestService {

    @Autowired
    private ProfileChangeRequestDao pcrd;
     
    @Override 
    public void SavePCR(ProfileChangeRequest pcr) {
        pcrd.SavePCR(pcr);
    }
    @Override 
    public ProfileChangeRequest retievePCR(Integer identificationNumber) {
       return pcrd.retievePCR(identificationNumber);
    }

    @Override
    public List<ProfileChangeRequest> retieveAllPendingRequest(Integer identificationNumber) {
        return pcrd.retieveAllPendingRequest(identificationNumber);
    }

    @Override
    public void updatePCRS(ProfileChangeRequest retievePCR) {
        pcrd.updatePCRS(retievePCR);
    }

    @Override
    public void deletePCRS(ProfileChangeRequest retievePCR) {
        pcrd.deletePCRS(retievePCR);
    }

    @Override
    public List<ProfileChangeRequest> retieveAllApprovedRequest(Integer moderatorIN) {
        return pcrd.retieveAllApprovedRequest(moderatorIN);
    }
   
    
}
