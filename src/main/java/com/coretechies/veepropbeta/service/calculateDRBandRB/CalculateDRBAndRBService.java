/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.calculateDRBandRB;

import com.coretechies.veepropbeta.domain.InGameTraderDetail;


/**
 *
 * @author Noppp
 */
public interface CalculateDRBAndRBService {

    public void calculateDRB(InGameTraderDetail IGTD, Integer vCredit, Double bonusPercentage, Integer commisionType);
    
}
