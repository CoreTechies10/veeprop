/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.faq;

import com.coretechies.veepropbeta.domain.Faq;
import java.util.List;

/**
 *
 * @author Arvind
 */
public interface FaqService {
    
    public void saveFaqData(Faq faq);

    public List<Faq> retrieveFAQs();
}
