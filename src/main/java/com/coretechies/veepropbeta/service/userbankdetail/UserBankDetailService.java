/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.userbankdetail;

import com.coretechies.veepropbeta.domain.UserBankDetail;

/**
 *
 * @author Arvind
 */
public interface UserBankDetailService {
    
    public void addBankDeatil(UserBankDetail  bankDetail, int UserId);
    
    public UserBankDetail retriveBank(Integer identificationNumber);
    
    public void updateBankDetail(UserBankDetail bankDetail);
    
}
