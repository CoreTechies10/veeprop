/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.userbankdetail;

import com.coretechies.veepropbeta.dao.userbankdetail.UserBankDetailDao;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserBankDetailServiceImpl implements UserBankDetailService {

    @Autowired
    private UserBankDetailDao bankDetailDao;
    
    @Override
    public void addBankDeatil(UserBankDetail bankDetail,int UserId) {
        bankDetailDao.addBankDeatil(bankDetail, UserId);
    }
    
    @Override
    public UserBankDetail retriveBank(Integer identificationNumber) {
       return  bankDetailDao.retriveBank(identificationNumber);
    }
    
    @Override
    public void updateBankDetail(UserBankDetail bankDetail ) {
       bankDetailDao.updateBankDetail(bankDetail);
    }
    
}
