/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.traderrelativedata;

import com.coretechies.veepropbeta.domain.TraderRelativeData;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface TraderRelativeDataService {

    public List<TraderRelativeData> retrieveChild(Integer identificationNumber);

    public void saveTRD(TraderRelativeData trd);

    public Integer retrieveReferalParent(Integer identificationNumber);
    
    public TraderRelativeData retrieveBinaryParent(Integer bp);
    
}
