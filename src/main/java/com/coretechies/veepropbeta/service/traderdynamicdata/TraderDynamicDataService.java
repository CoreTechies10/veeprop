/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.traderdynamicdata;

import com.coretechies.veepropbeta.domain.TraderDynamicData;

/**
 *
 * @author Noppp
 */
public interface TraderDynamicDataService {

    public void saveTDD(TraderDynamicData tdd);

    public TraderDynamicData retrieveTDD(Integer identificationNumber);

    public void updateTDD(TraderDynamicData TDD);
    
}
