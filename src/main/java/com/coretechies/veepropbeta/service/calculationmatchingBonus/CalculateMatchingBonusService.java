/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.calculationmatchingBonus;

/**
 *
 * @author Noppp
 */
public interface CalculateMatchingBonusService {

    public void calculateMatchingBonus(Integer bp, Integer amount, Integer position);
    
}
