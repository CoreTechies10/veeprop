/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.calculationmatchingBonus;

import com.coretechies.veepropbeta.dao.bonustransaction.BonusTransactionDao;
import com.coretechies.veepropbeta.dao.commission.CommissionDao;
import com.coretechies.veepropbeta.dao.ingamecreditdetails.InGameCreditDetailsDao;
import com.coretechies.veepropbeta.dao.notification.NotificationDao;
import com.coretechies.veepropbeta.dao.traderdynamicdata.TraderDynamicDataDao;
import com.coretechies.veepropbeta.dao.traderrelativedata.TraderRelativeDataDao;
import com.coretechies.veepropbeta.dao.transaction.TransactionDao;
import com.coretechies.veepropbeta.domain.BonusTransaction;
import com.coretechies.veepropbeta.domain.Commission;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.TraderDynamicData;
import com.coretechies.veepropbeta.domain.TraderRelativeData;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionPercentage;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionType;
import com.coretechies.veepropbeta.web.controller.algorithm.ERentalDecide;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class CalculateMatchingBonusServiceImpl implements CalculateMatchingBonusService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private InGameCreditDetailsDao inGameCreditDetailsDao;
    
    @Autowired
    private TraderDynamicDataDao traderDynamicDataDao;
    
    @Autowired
    private TraderRelativeDataDao traderRelativeDataDao;

    @Autowired
    private TransactionDao transactionDao;
    
    @Autowired
    private BonusTransactionDao bonusTransactionDao;
    
    @Autowired
    private CommissionDao commissionDao;
    
    @Autowired
    private NotificationDao notificationDao;
    
    @Override
    public void calculateMatchingBonus(Integer bp, Integer amount, Integer position) {
        
        TraderDynamicData binaryParentTDD = traderDynamicDataDao.retrieveTDD(bp);
        Integer matchingBonusPaid = binaryParentTDD.getMatchingBonusPaid();
        logger.info("MBP:"+matchingBonusPaid);
        logger.info("position is:"+position);
        logger.info("amount is"+amount);
        Integer moneyOnLeft = binaryParentTDD.getMoneyOnLeft();
        Integer moneyOnRight = binaryParentTDD.getMoneyOnRight();
        if(position == 0){
            binaryParentTDD.setMoneyOnLeft(moneyOnLeft+amount);
        }else if(position == 1){
            binaryParentTDD.setMoneyOnRight(moneyOnRight+amount);
        }
        traderDynamicDataDao.updateTDD(binaryParentTDD);
        logger.info("money on left:"+binaryParentTDD.getMoneyOnLeft());
        logger.info("money on right:"+binaryParentTDD.getMoneyOnRight());
        int minimum = findMinimun(binaryParentTDD.getMoneyOnLeft(),binaryParentTDD.getMoneyOnRight());
        logger.info("minimum is:"+minimum);
        double packageAmount = binaryParentTDD.getPackageAmount();
        Integer payableBonus = minimum - matchingBonusPaid;   //TBP_Session
        if(matchingBonusPaid < minimum){
            InGameTraderDetail binaryParentIGCD = inGameCreditDetailsDao.retrieveIGCD(bp);
            
            float bonusPercentage = (float) (CommisionPercentage.LEVEL.getCommisionPercentageByLevel(binaryParentIGCD.getLevelIN()) + binaryParentIGCD.getACP());
            logger.info("bonus percentage is:"+bonusPercentage);
            float matchingBonus = (float) payableBonus * bonusPercentage/100; //MB_Session
            logger.info("MB_Session is:"+matchingBonus);
            int date = binaryParentIGCD.getLTD().getDate();
            int month = binaryParentIGCD.getLTD().getMonth();
            int year = binaryParentIGCD.getLTD().getYear();
            logger.info("Last transaction date modified is:"+date+" "+month+" "+year);
            int date1 = new Date().getDate();
            int year1 = new Date().getYear();
            int month1 = new Date().getMonth();
            logger.info("Current date modified is:"+date1+" "+month1+" "+year1);
            //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            //String format = df.format(new Date());
            
            //logger.info("current date is:"+format);
            logger.info("last Transaction Date is:"+binaryParentIGCD.getLTD().toString());
            if(date==date1 && year == year1 && month==month1){
                logger.info("Its true:");
            }else{
                logger.info("I am always here");
                binaryParentIGCD.setDMB(0.0);
                binaryParentIGCD.setLTD(new Date());
            }
            Double oldDMB = binaryParentIGCD.getDMB();
            logger.info("old DMB:"+oldDMB);
            logger.info("Package Amount:"+packageAmount+"And DMB is:"+binaryParentIGCD.getDMB());
            if(packageAmount > binaryParentIGCD.getDMB()){
                if((packageAmount - binaryParentIGCD.getDMB()) > matchingBonus){
                    binaryParentIGCD.setDMB(oldDMB + matchingBonus);
                }else{
                    binaryParentIGCD.setDMB((double)packageAmount);
                }
            }else{
                binaryParentIGCD.setDMB((double)packageAmount);
            }
            logger.info("new DMB:"+binaryParentIGCD.getDMB());
            if(binaryParentIGCD.getDMB() > oldDMB){
                float MB = (float) (binaryParentIGCD.getDMB()-oldDMB);
                binaryParentIGCD.setvCredit(binaryParentIGCD.getvCredit()+(double)MB*70/100);
                binaryParentIGCD.setIGC(binaryParentIGCD.getIGC()+(double)MB*30/100);
                logger.info("Matching Bonus Paid to user "+bp+" is: "+MB);
                inGameCreditDetailsDao.updateIGTD(binaryParentIGCD);
                binaryParentTDD.setMatchingBonusPaid(binaryParentTDD.getMatchingBonusPaid()+payableBonus);
                traderDynamicDataDao.updateTDD(binaryParentTDD);
                Integer rp = traderRelativeDataDao.retrieveReferalParent(bp);
                if(rp!=0){
                    InGameTraderDetail referalParenDetail = inGameCreditDetailsDao.retrieveIGCD(rp);
                    calculateERental(MB,1, referalParenDetail);
                }
                createTransaction(MB, bp,CommisionType.MB.getCommisionTypeValue());
            }
            
            TraderRelativeData retrieveBinaryParent = traderRelativeDataDao.retrieveBinaryParent(bp);
            if(retrieveBinaryParent.getpTP()!= null){
                calculateMatchingBonus(retrieveBinaryParent.getbP(), amount, retrieveBinaryParent.getpTP());
            }
            
        }else{
            TraderRelativeData retrieveBinaryParent = traderRelativeDataDao.retrieveBinaryParent(bp);
            if(retrieveBinaryParent.getpTP()!= null){
                calculateMatchingBonus(retrieveBinaryParent.getbP(), amount, retrieveBinaryParent.getpTP());
            }
        }
    }

    private int findMinimun(Integer moneyOnLeft, Integer moneyOnRight) {
        if(moneyOnLeft < moneyOnRight){
            return moneyOnLeft;
        }else{
            return moneyOnRight;
        }
    }

    private void createTransaction(float MB,Integer Buyer, Integer commissionType) {
        Transaction transaction = new Transaction();
        transaction.setAmount((double)MB);
        transaction.setBuyerIN(Buyer);
        transaction.setSellerIN(2);
        transaction.setTransactionDate(new Date());
        transaction.setTransactionTime(new Date(System.currentTimeMillis()));
        transaction.setTransactionType(TransactionType.BONUSTRANSACTION.getTransactionTypeValue());
        
        transactionDao.saveTransaction(transaction);
                
        Notification notification = new Notification();
        notification.setNotificationStatus(Boolean.FALSE);
        notification.setIdentificationNumber(Buyer);
        notification.setNotificationDate(new Date());
        notification.setNotification("You have recieved "+MB+" USD as "+CommisionType.BONUS.getCommisionType(commissionType));
        String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
        notification.setNotificationLink(link);
        
        notificationDao.saveNotification(notification);
        
        BonusTransaction bonusTransaction = new BonusTransaction();
        bonusTransaction.setBonusTransactionToken(transaction.getTransactionId());
        bonusTransaction.setBonusType(commissionType);
        
        bonusTransactionDao.saveBonus(bonusTransaction);
        
        Commission commission = new Commission();
        commission.setAmmount(MB);
        commission.setCommissionType(commissionType);
        commission.setTransactionId(transaction.getTransactionId());
        commission.setUserIn(Buyer);
        
        commissionDao.saveCommission(commission);   
    }    

    private void calculateERental(float MB, int count, InGameTraderDetail root) {
        ERentalDecide erd = new ERentalDecide(root.getLevelIN());
        int BERG = erd.getERentalDecided();
        int AERG = root.getAERG();
        logger.info("BERG:"+BERG);
        logger.info("AERG:"+AERG);
        int elligilbleERenatal = BERG+AERG;
        if(elligilbleERenatal>=count){
            float eRentalGiven= MB* elligilbleERenatal/100;
            float eRentalAsVCredit = eRentalGiven * 70/100;
            float eRentalAsIGC = eRentalGiven * 30/100;
            root.setvCredit(root.getvCredit()+eRentalAsVCredit);
            root.setIGC(root.getIGC()+eRentalAsIGC);
            logger.info("ERental Given is:"+eRentalGiven);
            logger.info("ERental Given as vCredit is:"+eRentalAsVCredit);
            logger.info("ERental Given as IGC is:"+eRentalAsIGC);
            inGameCreditDetailsDao.updateIGTD(root);
            createTransaction(eRentalGiven, root.getIdentificationNumber(), CommisionType.ERB.getCommisionTypeValue());
        }
        if(count<=11){
            Integer referalParent = traderRelativeDataDao.retrieveReferalParent(root.getIdentificationNumber());
            logger.info("further referal parent is:"+referalParent);
            if(referalParent!=0){
                InGameTraderDetail binaryParentIGCD = inGameCreditDetailsDao.retrieveIGCD(referalParent);
                calculateERental(MB, count+1,binaryParentIGCD);
            }
        }
    }
    
}
