/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.user;

import com.coretechies.veepropbeta.dao.user.UserDao;
import com.coretechies.veepropbeta.domain.SecurityQuestion;
import com.coretechies.veepropbeta.domain.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;
    
    @Override
    public User retrieveUser(Integer identificationNumber) {
        return userDao.retrieveUser(identificationNumber);
    }

    @Override
    public List<SecurityQuestion> retrieveSecurityQuestion() {
        return userDao.retrieveSecurityQuestion();
    }

    public void saveUser(User trader) {
        userDao.saveUser(trader);
    }

    public List<User> retrieveUser(String str) {
        return userDao.retrieveUser(str);
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    public User retrieveUserByMail(String str) {
        return userDao.retrieveUserByMail(str);
    }

    @Override
    public List<String> retrieveAllUserEmail() {
        return userDao.retrieveAllUserEmail();
    }
    
}
