/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.directreferaldecide;

import com.coretechies.veepropbeta.dao.ingamecreditdetails.InGameCreditDetailsDao;
import com.coretechies.veepropbeta.dao.notification.NotificationDao;
import com.coretechies.veepropbeta.dao.traderrelativedata.TraderRelativeDataDao;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.web.controller.algorithm.UserTitle;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class DirectReferalDecideServiceImpl implements DirectReferalDecideService{
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private TraderRelativeDataDao traderRelativeDataDao;
    
    @Autowired
    private InGameCreditDetailsDao inGameCreditDetailsDao;
    
    @Autowired
    private NotificationDao notificationDao;
    
    public InGameTraderDetail DirectReferalDecide(int level,InGameTraderDetail IGTD){
        Integer oldTitle = IGTD.getTitle();
        logger.info("old title is :"+oldTitle);
        if(level == 4){
            IGTD.setDR1000(IGTD.getDR1000()+1);
        }else if(level == 5){
            IGTD.setDR2000(IGTD.getDR2000()+1);
        }else if(level == 6){
            IGTD.setDR5000(IGTD.getDR5000()+1);
        }
        
        InGameTraderDetail IGTD_NEW = setTitle(IGTD);
        logger.info("id is:"+IGTD_NEW.getIdentificationNumber());
        if(IGTD_NEW.getTitle() > oldTitle){
            IGTD.setTitle(IGTD_NEW.getTitle());
            //update title notification
            Notification notification = new Notification();
            notification.setIdentificationNumber(IGTD.getIdentificationNumber());
            notification.setNotificationStatus(Boolean.FALSE);
            notification.setNotificationDate(new Date());
            notification.setNotification("Your Title has been updated as : "+IGTD.getTitle());
            String link = "/veepropbeta/notification/"+0+"/"+0;
            notification.setNotificationLink(link);
            
            notificationDao.saveNotification(notification);
        }else{
            IGTD.setTitle(oldTitle);
        }
        
        if(oldTitle != IGTD_NEW.getTitle()){
            InGameTraderDetail root = IGTD_NEW;
            logger.info("new title of user is:"+IGTD_NEW.getTitle());
            Integer referalParentIN = traderRelativeDataDao.retrieveReferalParent(IGTD_NEW.getIdentificationNumber());
            if(referalParentIN != 0){
                InGameTraderDetail referalParent = inGameCreditDetailsDao.retrieveIGCD(referalParentIN);
                updateTitle(root,referalParent);
            }
        }
        
        return IGTD_NEW;
    }
    
    public InGameTraderDetail setTitle(InGameTraderDetail IGTD){
        if(IGTD.getDRD() >= 6){
            IGTD.setTitle(UserTitle.CROWN.getUserTitleValue());
            IGTD.setAERG(5);
            IGTD.setACP(1.5);
        }else if(IGTD.getDRG() >= 6){
            IGTD.setTitle(UserTitle.DIAMOND.getUserTitleValue());
            IGTD.setAERG(5);
            IGTD.setACP(1.0);
        }else if(IGTD.getDRS() >= 6){
            IGTD.setTitle(UserTitle.PLATINUM.getUserTitleValue());
            IGTD.setAERG(4);
            IGTD.setACP(0.5);
        }else if(IGTD.getDR5000() >= 6){
            IGTD.setTitle(UserTitle.GOLD.getUserTitleValue());
            IGTD.setAERG(3);
        }else if(IGTD.getDR2000() >= 6){
            IGTD.setTitle(UserTitle.SILVER.getUserTitleValue());
            IGTD.setAERG(2);
        }else if(IGTD.getDR1000() >= 6){
            IGTD.setTitle(UserTitle.BRONZE.getUserTitleValue());
            IGTD.setAERG(1);
        }
        return IGTD;
    }

    private void updateTitle(InGameTraderDetail root, InGameTraderDetail referalParent) {
       logger.info("child id is:"+root.getIdentificationNumber());
       logger.info("referal parent id is:"+referalParent.getIdentificationNumber());
        int oldTitle = referalParent.getTitle();
        logger.info("child title is"+root.getTitle());
        logger.info("old title is:"+oldTitle);
        logger.info("now we are here:1");
        if(root.getTitle() == UserTitle.DIAMOND.getUserTitleValue()){
            referalParent.setDRD(referalParent.getDRD()+1);
        }else if(root.getTitle() == UserTitle.GOLD.getUserTitleValue()){
            referalParent.setDRG(referalParent.getDRG()+1);
        }else if(root.getTitle() == UserTitle.SILVER.getUserTitleValue()){
            referalParent.setDRS(referalParent.getDRS()+1);
        }
        inGameCreditDetailsDao.updateIGTD(referalParent);
        logger.info("now we are here:2");
        InGameTraderDetail IGTD = setTitle(referalParent);
        
        if( IGTD.getTitle() > oldTitle){
            inGameCreditDetailsDao.updateIGTD(IGTD);
            Notification notification = new Notification();
            notification.setIdentificationNumber(IGTD.getIdentificationNumber());
            notification.setNotificationStatus(Boolean.FALSE);
            notification.setNotificationDate(new Date());
            notification.setNotification("Your Title has been updated as : "+IGTD.getTitle());
            String link = "/veepropbeta/notification/"+0+"/"+0;
            notification.setNotificationLink(link);
            
            notificationDao.saveNotification(notification);
        }
        logger.info("now we are here:30");
        if(oldTitle!=IGTD.getTitle()){
            logger.info("now we are here:4");
            Integer referalParentIN = traderRelativeDataDao.retrieveReferalParent(IGTD.getIdentificationNumber());
            if(referalParentIN != 0){
                InGameTraderDetail referalParentNEW = inGameCreditDetailsDao.retrieveIGCD(referalParentIN);
                updateTitle(IGTD,referalParentNEW);
            }
        }
    }
    
}
