package com.coretechies.veepropbeta.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

@Component
public class MailMail {

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage simpleMailMessage;

    public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
        this.simpleMailMessage = simpleMailMessage;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendMail(String subject, String content, String to) {
        SimpleMailMessage message = new SimpleMailMessage(simpleMailMessage);
        message.setTo(to);
        message.setText(content);
        message.setSubject(subject);
        mailSender.send(message);
    }
}
