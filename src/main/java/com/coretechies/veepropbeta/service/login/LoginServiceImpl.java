/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.login;

import com.coretechies.veepropbeta.dao.login.LoginDao;
import com.coretechies.veepropbeta.domain.Login;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Noppp
 */
@Service
public class LoginServiceImpl implements LoginService{
    
    @Autowired
    private LoginDao loginDao;

    public Boolean isCredential(Login login) {
        return loginDao.isCredential(login);
    }

    public Login retrieveUser(String userName){
       return loginDao.retrieveUser(userName);
    }

    public void saveTraderCredentials(Login log) {
        loginDao.saveTraderCredentials(log);
    }
    
    public Long countUser(String userRole){
        return loginDao.countUser(userRole);
    }

    public List<Integer> retrieveUserByRole(String userRoleValue) {
        return loginDao.retrieveUserByRole(userRoleValue);
    }

    public Login retrieveUser(Integer buyerIN) {
        return loginDao.retrieveUser(buyerIN);
    }

    public void updateLogin(Login login) {
        loginDao.updateLogin(login);
    }
}
