/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.notification;

import com.coretechies.veepropbeta.domain.Notification;
import java.util.List;


/**
 *
 * @author Noppp
 */
public interface NotificationService {
    
    public void saveNotification(Notification notification);

    public List<Notification> retreiveNotification(Integer identificationNumber);

    public void updateNotification(Integer notificationId);

    public Notification retreiveNotificationById(Integer notificationId);
    
}
