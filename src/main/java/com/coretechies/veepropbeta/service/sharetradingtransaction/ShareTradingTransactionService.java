/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.sharetradingtransaction;

import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import java.util.List;

/**
 *
 * @author Noppp
 */
public interface ShareTradingTransactionService {

    public void saveShareTrade(ShareTradingTransaction shareTradingTransaction);

    public List<ShareTradingTransaction> retrievePendingTransaction(int transactionStatusValue);

    public ShareTradingTransaction retrieveSTT(Integer shareTransactionId);

    public void updateSTT(ShareTradingTransaction STT);

    public ShareTradingTransaction retrieveSTTByTransaction(Integer transactionId);
    
}
