/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.engine;

import com.coretechies.veepropbeta.domain.Engine;

/**
 *
 * @author Arvind
 */
public interface EngineService {
    
    public Engine retieveAll(Engine engine);
    
    public Engine updateEngine(Engine engine );
    
}
