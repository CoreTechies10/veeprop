/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.service.profile;

import com.coretechies.veepropbeta.dao.profile.ProfileDao;
import com.coretechies.veepropbeta.domain.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.UserBankDetail;

/**
 *
 * @author Rohit
 */
@Service 
public class ProfileServiceImpl implements ProfileService{
    @Autowired
    private ProfileDao profileDao;
    
    @Override
    public User retrivePersonal(Integer identificationNumber){
        return profileDao.retrivePersonal(identificationNumber);
    }
 
    @Override
    public UserAddressInfo retriveAddress(Integer identificationNumber){
       return profileDao.retriveAddress(identificationNumber);
    }

    @Override
    public UserBankDetail retriveBank(Integer identificationNumber){
        return profileDao.retriveBank(identificationNumber);
    }    

    @Override
    public void addAddresssInfo(UserAddressInfo userAddressinfo) {
        
        profileDao.addAddresssInfo(userAddressinfo);
    }

    @Override
    public void updateProfile(UserAddressInfo address, User personal) {
        profileDao.updateProfile(address,personal);
    }
}
