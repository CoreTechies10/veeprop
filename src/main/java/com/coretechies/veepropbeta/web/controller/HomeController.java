/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Faq;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.Notification;

import com.coretechies.veepropbeta.service.faq.FaqService;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;

import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.web.controller.webdomain.ChangePassword;
import com.coretechies.veepropbeta.web.controller.webdomain.LotGraph;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class HomeController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());

    
    @Autowired
    private LotDetailService lotdetailservice;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private UserService userService;
    @Autowired 
    private InGameCreditDetailsService ingameservice;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private FaqService faqService;
    
    @RequestMapping(value = "/home", method = RequestMethod.GET)
     public ModelAndView displayHomePage(ModelMap model) throws ParseException{
        Login user = (Login)model.get("user");
        if(user != null){
            model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
            if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
                model.addAttribute("notification", notification);
                model.addAttribute("message","Login Successfull");
                InGameTraderDetail retrieveIGCD = ingameservice.retrieveIGCD(user.getIdentificationNumber());
                LotDetails retrieveLotDetail = lotdetailservice.retrieveLotDetail();
                 model.addAttribute("name",userService.retrieveUser(user.getIdentificationNumber()).getName());
                 model.addAttribute("image",userService.retrieveUser(user.getIdentificationNumber()).getImage());
                List<LotDetails> retrieveAllLotEntries = lotdetailservice.retrieveAllLotEntries();
                 List<LotGraph> graph=new ArrayList<LotGraph>();
                 for(LotDetails ld:retrieveAllLotEntries){
                 LotGraph lotgraph=new LotGraph();
                 lotgraph.setLotNumber(ld.getLotNumber());
                 Calendar cal1 = new GregorianCalendar();
                Calendar cal2 = new GregorianCalendar();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                 Date date = sdf.parse(ld.getReleaseDate().toString());
                 cal1.setTime(date);
                 try{
                 if(!ld.getEndDate().equals(null)){
                 date = sdf.parse(ld.getEndDate().toString());    
                 }else
                 {
                     date=sdf.parse(new Date().toString());
                 }
                 }
                 catch(NullPointerException npe){
                     
                 }
                
                    cal2.setTime(date);
                    int days = daysBetween(cal1.getTime(),cal2.getTime());
                    lotgraph.setDays(days);
                    graph.add(lotgraph);
                 }
               model.addAttribute("graph", graph);
                return new ModelAndView("home");
            }else if(user.getUserRole().equals(UserRole.SUPERADMIN.getUserRoleValue())){
                model.addAttribute("message","Login Successfull");
                return new ModelAndView("companydashboard");
            }else if(user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                model.addAttribute("message","Login Successfull Moderator");
                return  new ModelAndView("moderatorHome");
            }else if(user.getUserRole().equals(UserRole.ADMIN.getUserRoleValue())){
                List<LotDetails> lots = lotdetailservice.retrieveAllLotEntries();
                model.addAttribute("lots", lots);
                LotDetails lot = lotdetailservice.retrieveLotDetail();
                model.addAttribute("lot", lot);
                List<Integer> trader = loginService.retrieveUserByRole(UserRole.TRADER.getUserRoleValue());
                List<Integer> moderator = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
                model.addAttribute("trader",trader.size());
                model.addAttribute("moderator",moderator.size());
                model.addAttribute("message","Login Successfull ADMIN");
                return  new ModelAndView("adminHome");
            }
        }
        return new ModelAndView("redirect:/login");
    }
     
                        public int daysBetween(Date d1, Date d2){
             return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
     }
     
     @RequestMapping(value = "/faq", method = RequestMethod.GET)
     public ModelAndView retrieveFAQ(ModelMap model){
         List<Faq> faq = faqService.retrieveFAQs();
         model.addAttribute("faq",faq);
         return new ModelAndView("faq");
     }
     
    @RequestMapping(value = "/lotinformation")
    public 
        @ResponseBody
        List retievelotInformation(ModelMap model){
            List<String> list=new ArrayList<String>();
        LotDetails lots = lotdetailservice.retrieveLotDetail();
        list.add("Lot Number: "+lots.getLotNumber().toString());
        list.add("Share Available: "+lots.getNumberOfShare().toString());
        list.add("Price Per Share: "+lots.getPricePerShare().toString());
        list.add("Generation Number: "+lots.getGenerationNumber().toString());
        return list;
    }     
        
    @RequestMapping(value = "/changepassword",method = RequestMethod.GET)
    public ModelAndView showChangePassword(@ModelAttribute("changepass") ChangePassword changepass,BindingResult result,ModelMap model){
        Login user=(Login) model.get("user");
        if(user !=null){
           return new ModelAndView("changepassword");
        }
        return new ModelAndView("changepassword");
        
    }
    
    @RequestMapping(value = "/changepassword",method = RequestMethod.POST)
    public ModelAndView saveChangePassword(@Valid @ModelAttribute("changepass") ChangePassword changepass,
            BindingResult result,SessionStatus session,ModelMap model){
        Login user=(Login) model.get("user");
        if(user !=null){
          if(result.hasErrors()){
           return new ModelAndView("changepassword");
          }
          if(user.getPassword().equals(changepass.getCurrentPassword())){
              if(changepass.getPassword().equals(changepass.getReTypePassword())){
                  user.setPassword(changepass.getPassword());
                  loginService.updateLogin(user);
                  session.setComplete();
                  return new ModelAndView("redirect:/login");
              }else{
                  model.addAttribute("message","Password Not matched");
              }
          }else{
              model.addAttribute("message", "You entered wrong current password");
          }
        }
        return new ModelAndView("changepassword");
        
    }
}
