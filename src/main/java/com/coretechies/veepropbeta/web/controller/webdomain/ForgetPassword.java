/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 *
 * @author Noppp
 */
public class ForgetPassword {
    
    @Email(message = "Enter Valid Email")
    private String email;
    
    @Range(min = 1, message = "Select A Question")
    private Integer questionId;
    
    @NotBlank(message = "Provide Answer")
    private String answer;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "ForgetPassword{" + "email=" + email + ", questionId=" + questionId + ", answer=" + answer + '}';
    }
}
