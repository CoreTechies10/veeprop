/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum UserLevel {
    
    NONE(0),
    
    LEVEL1(1),

    LEVEL2(2),

    LEVEL3(3),

    LEVEL4(4),

    LEVEL5(5),

    LEVEL6(6),

    LEVEL7(7),

    LEVEL8(8);
    
    private final int userLevelValue;

    private UserLevel(int userLevelValue) {
        this.userLevelValue = userLevelValue;
    }

    public int getUserLevelValue() {
        return userLevelValue;
    }
}
