/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;


import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class NotificationController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private NotificationService notificationService;
    
    @RequestMapping(value = "/notification/{transactionId}/{transactionType}/{notificationId}", method = RequestMethod.GET)
    public ModelAndView showNotification(@PathVariable Integer transactionId,
            @PathVariable Integer transactionType,@PathVariable Integer notificationId,ModelMap model){
        Login user = (Login) model.get("user");
        if(user != null){
            notificationService.updateNotification(notificationId);
            Notification notification = notificationService.retreiveNotificationById(notificationId);
            model.addAttribute("notification",notification.getNotification());
            List<Notification> notificationList = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notificationList", notificationList);
            
            model.addAttribute("transactionDate", "Transaction Performed on : "+notification.getNotificationDate());
            if(transactionId > 0){
                Transaction transaction = transactionService.retrieveTransaction(transactionId);
                if(transactionType == TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue()){
                    model.addAttribute("vCredit","vCredit Transacted : "+transaction.getAmount());
                }else if(transactionType == TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue()){
                        model.addAttribute("vCredit","Share Traded of : "+transaction.getAmount()+" USD");
                }else if(transactionType == TransactionType.BONUSTRANSACTION.getTransactionTypeValue()){
                    model.addAttribute("vCredit","Bonus Recieved "+transaction.getAmount()+" USD");
                }
            }else if(transactionId == 0){
                model.addAttribute("vCredit","Title updated, for complete detail check title upgradation rules.");
            }else if(transactionId == -1){
                model.addAttribute("vCredit","Level updated, for complete detail check Level upgradation rules.");
            }else if(transactionId == -2){
                model.addAttribute("vCredit","Profile Updated");
            }
            return new ModelAndView("notification");
        }
        return new ModelAndView("redirect:/login");
    }
}
