/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;

/**
 *
 * @author Noppp
 */
public class ModeratorData {
    
    private String name;
    
    private String email;
    
    private String contact;
    
    private Integer securityQuestionId;
    
    private String securityAnswer;
    
    private Integer gender;
    
    private Date dateOfBirth;
    
    private String houseNumber;
    
    private String streetName1;
    
    private String streetName2;
    
    private String city;
    
    private String landmark;
    
    private String userstate;
    
    private String country;
    
    private String zipcode;    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName1() {
        return streetName1;
    }

    public void setStreetName1(String streetName1) {
        this.streetName1 = streetName1;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getUserstate() {
        return userstate;
    }

    public void setUserstate(String userstate) {
        this.userstate = userstate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "ModeratorData{" + "name=" + name + ", email=" + email + ", contact=" + contact + ", securityQuestionId=" + securityQuestionId + ", securityAnswer=" + securityAnswer + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", houseNumber=" + houseNumber + ", streetName1=" + streetName1 + ", streetName2=" + streetName2 + ", city=" + city + ", landmark=" + landmark + ", userstate=" + userstate + ", country=" + country + ", zipcode=" + zipcode + '}';
    }
    
}
