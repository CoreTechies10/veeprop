/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum CommisionType {
    
    DRB(0),
    
    RB(1),
    
    MB(2),
    
    ERB(3),
    
    SHARE(4),
    
    BONUS(5);
    
    private final int CommisionTypeValue;
    
    private CommisionType(int CommisionTypeValue){
        this.CommisionTypeValue = CommisionTypeValue;
    }
    
    public int getCommisionTypeValue(){
        return CommisionTypeValue;
    }
    
    public String getCommisionType(int commissionType){
        if(commissionType==CommisionType.DRB.getCommisionTypeValue()){
            return "Direct Referal Bonus";
        }else if(commissionType==CommisionType.MB.getCommisionTypeValue()){
            return "Matching Bonus";
        }else if(commissionType==CommisionType.ERB.getCommisionTypeValue()){
            return "E Rental Bonus";
        }else if(commissionType==CommisionType.RB.getCommisionTypeValue()){
            return "Residual Bonus";
        }
        return null;
    }
}
