/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.SecurityQuestion;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.web.controller.algorithm.UserStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.UserTitle;
import com.coretechies.veepropbeta.web.controller.webdomain.ForgetPassword;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user","loginValue"})
public class LoginController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private SimpleMailMessage simpleMailMessage;    
    
    @Autowired
    private InGameCreditDetailsService inGameCreditDetailsService;
    
    @Autowired
    private LotDetailService lotDetailService;
    
    private static final int ID_LENTH = 10;
    
    @RequestMapping(value="/index", method = RequestMethod.GET)
    public ModelAndView Showindex(ModelMap model){
        List<LotDetails> lots = lotDetailService.retrieveAllLotEntries();
        model.addAttribute("lots", lots);
        return new ModelAndView("index");
    }
    
    
    @RequestMapping(value="/login", method = RequestMethod.GET )
    public ModelAndView displayLogin(@ModelAttribute("login") Login login ,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            return new ModelAndView("redirect:/home");
        }
        model.addAttribute("loginValue",false);
        return new ModelAndView("login");
    }
    
    @RequestMapping(value = "/loginSubmit", method = RequestMethod.POST)
    public ModelAndView checkCredentials(@Valid @ModelAttribute("login") Login login,BindingResult result,ModelMap model){
        if(result.hasErrors()){
            return new ModelAndView("login");
        }
        Boolean check= loginService.isCredential(login);
        if(check){
            Login user = loginService.retrieveUser(login.getUserName());
            model.addAttribute("loginValue",true);
            User retrieveUser = userService.retrieveUser(user.getIdentificationNumber());
            if(retrieveUser.getUserStatus()==UserStatus.Active.getUserStatusValue()){
                model.addAttribute("user",user);
            }else{
                model.addAttribute("message","Blocked Account");
            }
            
        }else{
            model.addAttribute("message","Login Failure.");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logoutFunction(SessionStatus status, ModelMap model){
        status.setComplete();
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/forgetpassword",method = RequestMethod.GET)
    public ModelAndView showResetForm(@ModelAttribute("resetpassword")ForgetPassword resetpassword,
            ModelMap model){
        List<SecurityQuestion> security = userService.retrieveSecurityQuestion();
        model.addAttribute("security",security);
        return new ModelAndView("forgetpassword");
    }

    @RequestMapping(value = "/forgetpassword", method = RequestMethod.POST)
    public ModelAndView retrievePassword(@Valid @ModelAttribute("resetpassword")ForgetPassword resetpassword,
            BindingResult result,ModelMap model){
        List<SecurityQuestion> security = userService.retrieveSecurityQuestion();
        model.addAttribute("security",security);
        if(result.hasErrors()){
            return new ModelAndView("forgetpassword");
        }
        User user = userService.retrieveUserByMail(resetpassword.getEmail());
        if(user == null){
            model.addAttribute("message","No User Registered with this email");
        }else{
            if(user.getSecurityQuestionId().equals(resetpassword.getQuestionId()) && 
                    user.getSecurityAnswer().toUpperCase().equals(resetpassword.getAnswer().toUpperCase())){
                
                String password = RandomStringUtils.randomAlphanumeric(ID_LENTH);
                
                Login login = loginService.retrieveUser(user.getIdentificationNumber());
                login.setPassword(password);
                loginService.updateLogin(login);
                
                String Subject = "New Password for Veeprop";
                String Content = "Dear" + " " + user.getName() + "\n" + 
                        "You Requested for new Password" + "\n "
                        + "Youe new Auto Generated Password is :"  +password +
                        "\n visit localhost:8080\\veepropbeta";


                MimeMessage message = mailSender.createMimeMessage();   

                try {

                    MimeMessageHelper helper = new MimeMessageHelper(message, false);
                    helper.setFrom(simpleMailMessage.getFrom());
                    helper.setTo(resetpassword.getEmail());
                    helper.setSubject(Subject);
                    helper.setText(Content);

                } catch (MessagingException e) {
                    //throw new MailParseException(e);
                    logger.error(Arrays.toString(e.getStackTrace()));
                }

                //mailSender.send(message);        

                model.addAttribute("message","Password has been sent to your email");
            }else{
                model.addAttribute("message","Incorrect Question or Answer");            
            }
        }
        return new ModelAndView("forgetpassword");
    }
    
    @RequestMapping(value = "/login/userInfo", method = RequestMethod.GET)
    public
    @ResponseBody
    List showUserInformation(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            List l = new ArrayList();
            User userInfo = userService.retrieveUser(user.getIdentificationNumber());
            logger.info("user is:"+userInfo.getName());
            InGameTraderDetail IGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            l.add(userInfo.getName());
            l.add("v-Credit :"+IGCD.getvCredit());
            l.add("IGC :"+IGCD.getIGC());
            l.add("v-Credit on hold:"+IGCD.getvCreditHold());
            l.add("Level :"+IGCD.getLevelIN());
            l.add("Share In Account :"+IGCD.getShareHold());
            l.add("Title :"+UserTitle.parse(IGCD.getTitle()));
            LotDetails lot = lotDetailService.retrieveLotDetail();
            l.add("Generation :"+lot.getGenerationNumber());
            int lotNumber= lot.getLotNumber();
            if(lotNumber%41==0){
                lotNumber = 41;
            }else{
                lotNumber = lotNumber%41;
            }
            l.add("Lot Number :"+lotNumber);
            l.add("Current Share Price:"+lot.getPricePerShare());
            l.add("Share in Lot :"+lot.getNumberOfShare());
            return l;
        }
        return null;
    }
    
    @RequestMapping(value = "/checkEmail/{email}/{loginId}", method = RequestMethod.GET)
    public
    @ResponseBody
    Integer checkEmail(@PathVariable String email, @PathVariable String loginId){
        logger.info("email retrieve is:"+email);
        email=email.replace("&", ".");
        logger.info("email retrieve is:"+email);
        List<String> emailList = userService.retrieveAllUserEmail();
        Login login = loginService.retrieveUser(loginId);
        for (String emailCheck : emailList){
            if (emailCheck.equalsIgnoreCase(email)){
                return 0;
            }
        }
        if(login != null){
            return 1;
        }
        return 2;
    }    
}
