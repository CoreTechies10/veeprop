/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum TransactionStatus {
    
    PENDING(0),
    
    COMPLETE(1),
    
    HOLD(2),
    
    LOCK(3),
    
    DISPUTE(4),
    
    STATUS(5);
    
    private final int TransactionStatusValue;
    
    private TransactionStatus(int TransactionStatusValue){
        this.TransactionStatusValue = TransactionStatusValue;
    }
    
    public int getTransactionStatusValue(){
        return TransactionStatusValue;
    }
    
    public String getStatusType(int TransactionStatusValue){
        if(TransactionStatusValue==TransactionStatus.COMPLETE.getTransactionStatusValue()){
            return "Complete";
        }else if(TransactionStatusValue==TransactionStatus.HOLD.getTransactionStatusValue()){
            return "Hold";
        }else if(TransactionStatusValue==TransactionStatus.LOCK.getTransactionStatusValue()){
            return "Lock";
        }else if(TransactionStatusValue==TransactionStatus.PENDING.getTransactionStatusValue()){
            return "Pending";
        }
        return null;
    }    
}
