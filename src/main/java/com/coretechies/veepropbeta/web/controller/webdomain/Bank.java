/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import javax.persistence.Column;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
public class Bank {
    
    @NotBlank(message = "Please Enter a BankName")
    private String bankName;

    @NotBlank(message = "Please Enter a Account Number")
    private String accountNumber;

    @NotBlank(message = "Please Enter a IFSC Code")
    private String iFSC;

    @NotBlank(message = "Please Select Account Type")
    private String accountType;

    @NotBlank(message = "Please Enter a Bank Address")
    private String bankAddress;

   @NotBlank(message = "Please Enter a Branch Name")
    private String branch;

   @NotBlank(message = "Please Enter a Swift Code")
    private String swift;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "Bank{" + "bankName=" + bankName + ", accountNumber=" + accountNumber + ", iFSC=" + iFSC + ", accountType=" + accountType + ", bankAddress=" + bankAddress + ", branch=" + branch + ", swift=" + swift + '}';
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getiFSC() {
        return iFSC;
    }

    public void setiFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
   
   
    
}
