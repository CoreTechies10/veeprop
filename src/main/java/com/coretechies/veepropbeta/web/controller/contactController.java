/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.dao.contact.ContactDao;
import com.coretechies.veepropbeta.domain.ContactUs;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Arvind
 */
@Controller
public class contactController {
    
    private final Logger logger=LoggerFactory.getLogger(getClass());
    
    @Autowired
    private ContactDao contactdao;
    
    @RequestMapping(value="/contact", method = RequestMethod.GET)
    public ModelAndView addContact(@ModelAttribute("contact") ContactUs contact ,ModelMap model){
        return new ModelAndView("contact");
    }
        
    @RequestMapping(value="/contact",method = RequestMethod.POST)
    public ModelAndView SaveContact(@Valid @ModelAttribute("contact") ContactUs contact ,BindingResult error,ModelMap model){
        logger.info("Error"+error);
        if(error.hasErrors()){
            return new ModelAndView("contact");
        }
        contactdao.addContact(contact);
        return new ModelAndView("contact");
    }
    
    @RequestMapping(value = "/aboutUs")
    public ModelAndView showAbout(){
        return new ModelAndView("aboutUs");
    }
   
    @RequestMapping(value = "/policy")
    public ModelAndView showPolicy(){
      return new ModelAndView("policy");
        
    }
    
    @RequestMapping(value = "/comapnytermandconditions")
    public ModelAndView showTermsConditions(){
        return new ModelAndView("comapnytermandconditions");
        
    }
    @RequestMapping(value = "/howitworks")
    public ModelAndView showhowitworks(){
        return new ModelAndView("howitworks");
        
    }
    @RequestMapping(value = "/makeprofit")
    public ModelAndView showmakeprofit(){
        return new ModelAndView("makeprofit");
        
    }
            
}
