/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
public class Registration {
    
    @Pattern(regexp = "[A-Za-z]+[\\s A-Za-z]+{5,15}", message = "InApproppriate Name")
    private String name;
    
    @NotBlank(message = "Please Enter a Email")
    @Email(message = "Enter a valid Email")
    private String email;
    
    @NotBlank(message = "Please Enter a LoginId")
    @Pattern(regexp = "[A-Za-z0-9_]+[\\s A-Za-z0-9_]+{5,15}", message = "In Approppriate Name")    
    private String loginId;
    
    @NotBlank(message = "Please Enter Contact")
    private String contact;
    
    private Integer securityQuestionId;
    
    private String securityAnswer;
    
    @NotNull(message = "Please Select Gender")
    private Integer gender;
    
    private Date dateOfBirth;
    
    @NotNull(message = "Please Enter vee-credit amount")
    private Integer vCredit;
    
    private Integer BP;
    
    private Integer RP;
    
    private Integer PTP;
    
    @NotBlank(message = "Please Enter Country")
    private String residentCountry;
    
    private String country;
    
    @NotBlank(message = "Please Enter Identity Proof")
    private String PassportNo;

    private Integer IGC;

    public Integer getIGC() {
        return IGC;
    }

    public void setIGC(Integer IGC) {
        this.IGC = IGC;
    }
   
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
   

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Integer getvCredit() {
        return vCredit;
    }

    public void setvCredit(Integer vCredit) {
        this.vCredit = vCredit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getBP() {
        return BP;
    }

    public void setBP(Integer BP) {
        this.BP = BP;
    }

    public Integer getRP() {
        return RP;
    }

    public void setRP(Integer RP) {
        this.RP = RP;
    }

    public Integer getPTP() {
        return PTP;
    }

    public void setPTP(Integer PTP) {
        this.PTP = PTP;
    }

    public String getResidentCountry() {
        return residentCountry;
    }

    public void setResidentCountry(String residentCountry) {
        this.residentCountry = residentCountry;
    }

    public String getPassportNo() {
        return PassportNo;
    }

    public void setPassportNo(String PassportNo) {
        this.PassportNo = PassportNo;
    }

    @Override
    public String toString() {
        return "Registration{" + "name=" + name + ", email=" + email + ", loginId=" + loginId + ", contact=" + contact + ", securityQuestionId=" + securityQuestionId + ", securityAnswer=" + securityAnswer + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", vCredit=" + vCredit + ", BP=" + BP + ", RP=" + RP + ", PTP=" + PTP + ", residentCountry=" + residentCountry + ", country=" + country + ", PassportNo=" + PassportNo + ", IGC=" + IGC + '}';
    }
}
