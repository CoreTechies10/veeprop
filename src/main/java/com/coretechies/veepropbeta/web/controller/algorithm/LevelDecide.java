/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public class LevelDecide {

    private int LevelDecided;

    public int getLevelDecided() {
        return LevelDecided;
    }
    
    public LevelDecide(Double packageAmount) {
        if(packageAmount >= 100 && packageAmount <=200){
            this.LevelDecided=1;
        }else if( packageAmount >=200 && packageAmount <500){
            this.LevelDecided=2;
        }else if(packageAmount >=500 && packageAmount <1000){
            this.LevelDecided=3;
        }else if(packageAmount >=1000 && packageAmount <2000){
            this.LevelDecided=4;
        }else if(packageAmount >=2000 && packageAmount <5000){
            this.LevelDecided=5;
        }else if(packageAmount >=5000 && packageAmount <10000){
            this.LevelDecided=6;
        }else if(packageAmount >=10000 && packageAmount <100000){
            this.LevelDecided=7;
        }else if(packageAmount >=100000){
            this.LevelDecided=8;
        }
    }
}
