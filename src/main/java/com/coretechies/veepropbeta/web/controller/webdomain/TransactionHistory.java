/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
public class TransactionHistory {
    
    @NotNull(message = "    Please Select Transaction Type")
    private Integer transactionType;
    
    @NotNull(message = "Please Select Initial Date")
    private Date initialDate;
    
    @NotNull(message = "Please Select End Date")
    private Date endDate;
    
    private Integer userIN;

    public Integer getUserIN() {
        return userIN;
    }

    public void setUserIN(Integer userIN) {
        this.userIN = userIN;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "TransactionHistory{" + "transactionType=" + transactionType + ", initialDate=" + initialDate + ", endDate=" + endDate + ", userIN=" + userIN + '}';
    }
}
