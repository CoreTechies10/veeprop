/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum UserTitle {
   
    NONE(0),
    
    BRONZE(1),
    
    SILVER(2),
    
    GOLD(3),
    
    PLATINUM(4),
    
    DIAMOND(5),
    
    CROWN(6);
    
    private final int userTitleValue;

    private UserTitle(int userTitleValue) {
        this.userTitleValue = userTitleValue;
    }

    public int getUserTitleValue() {
        return userTitleValue;
    }
    public static UserTitle parse(Integer userTitleValue) {
        UserTitle userTitle = null;
        for (UserTitle item : UserTitle.values()) {
            if (item.getUserTitleValue()==userTitleValue) {
                userTitle = item;
                break;
            }
        }
        return userTitle;
    }    
}
