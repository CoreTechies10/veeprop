/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.Commission;
import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.LotDetails;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import com.coretechies.veepropbeta.domain.TraderRelativeData;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.domain.VeeCreditTransactionType;
import com.coretechies.veepropbeta.domain.VerificationModerator;
import com.coretechies.veepropbeta.service.commission.CommissionService;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.lotdetails.LotDetailService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.sharetradingtransaction.ShareTradingTransactionService;
import com.coretechies.veepropbeta.service.traderdynamicdata.TraderDynamicDataService;
import com.coretechies.veepropbeta.service.traderrelativedata.TraderRelativeDataService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.service.userbankdetail.UserBankDetailService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.service.verificationmoderator.VerificationModeratorService;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionType;
import com.coretechies.veepropbeta.web.controller.algorithm.LevelDecide;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.algorithm.UserTitle;
import com.coretechies.veepropbeta.domain.Dispute;
import com.coretechies.veepropbeta.service.dispute.DisputeService;
import com.coretechies.veepropbeta.web.controller.webdomain.Sellers;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */
@Controller
@SessionAttributes({"user"})
public class WalletController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private InGameCreditDetailsService inGameCreditDetailsService;
    
    @Autowired
    private LotDetailService lotDetailService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private ShareTradingTransactionService shareTradingTransactionService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private VerificationModeratorService verificationModeratorService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private TraderDynamicDataService traderDynamicDataService;
    
    @Autowired
    private TraderRelativeDataService traderRelativeDataService;
    
    @Autowired
    private UserBankDetailService userBankDetailService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private CommissionService commissionService;
    
    @Autowired
    private DisputeService disputeService;
    
    @RequestMapping(value = "/wallet", method = RequestMethod.GET)
    public ModelAndView viewWallet(ModelMap model){
        Login user = (Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);           
            InGameTraderDetail gameDetails = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            model.addAttribute("gameDetails",gameDetails);
            UserTitle title = UserTitle.parse(gameDetails.getTitle());
            model.addAttribute("title",title);
            return new ModelAndView("wallet");
        }
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.GET)
    public ModelAndView viewTransaction(ModelMap model){
        Login user = (Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);                  
            LotDetails lot = lotDetailService.retrieveLotDetail();
            model.addAttribute("generaion",lot.getGenerationNumber());
            return new ModelAndView("transaction");
        }
        return new ModelAndView("redirect:/login");
    }    
    
    @RequestMapping(value = "/retreiveSellRequests", method = RequestMethod.GET)
    public ModelAndView retrieveSellers(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification); 
            
            List<Sellers> sellersShare = new ArrayList<Sellers>();
            List<ShareTradingTransaction> pendingList = shareTradingTransactionService.retrievePendingTransaction(TransactionStatus.PENDING.getTransactionStatusValue());
            logger.info("pending is"+pendingList.size());
            
            for(ShareTradingTransaction stt : pendingList){
                Transaction transaction = transactionService.retrieveTransaction(stt.getShareTradingTransactionToken());
                User users = userService.retrieveUser(transaction.getSellerIN());
                
                Sellers seller = new Sellers();
                seller.setAmount(transaction.getAmount());
                seller.setContact(users.getContact());
                seller.setEmail(users.getEmail());
                seller.setName(users.getName());
                seller.setNumberOfShare(stt.getNumberOfShare());
                seller.setSharePrice(stt.getCostPerShare());
                seller.setShareTransactionId(stt.getShareTradingTransactionId());
                seller.setIdentificationNumber(users.getIdentificationNumber());
                sellersShare.add(seller);
            }
            
            model.addAttribute("sellersShare", sellersShare);
            
            List<Sellers> sellersVCredit = new ArrayList<Sellers>();
            List<VeeCreditTransaction> pendingVCreditList = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.PENDING.getTransactionStatusValue());
            logger.info("pending is"+pendingList.size());
            
            for(VeeCreditTransaction vct : pendingVCreditList){
                Transaction transaction = transactionService.retrieveTransaction(vct.getVeeCreditTransactionToken());
                User users = userService.retrieveUser(transaction.getSellerIN());
                
                Sellers seller = new Sellers();
                seller.setAmount(transaction.getAmount());
                seller.setContact(users.getContact());
                seller.setEmail(users.getEmail());
                seller.setName(users.getName());
                seller.setvCredit(transaction.getAmount());
                seller.setShareTransactionId(vct.getVeeCreditTransactionId());
                seller.setIdentificationNumber(users.getIdentificationNumber());
                sellersVCredit.add(seller);
            }
            
            model.addAttribute("sellersVCredit", sellersVCredit);            
            return new ModelAndView("sellerboarddata");
        }
        List<Sellers> sellersVCredit = new ArrayList<Sellers>();
        model.addAttribute("sellersVCredit", sellersVCredit);
        return new ModelAndView("sellerboarddata");
    }
    
    @RequestMapping(value = "/seller/{identificationNumber}/{shareTransactionId}", method = RequestMethod.GET)
    public ModelAndView showSeller(@ModelAttribute("seller") Sellers seller,@PathVariable Integer identificationNumber,@PathVariable Integer shareTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);                
            InGameTraderDetail userIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            model.addAttribute("userIGCD",userIGCD);
            ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTT(shareTransactionId);
            User userInfo = userService.retrieveUser(identificationNumber);
            seller.setAmount(Math.floor(STT.getCostPerShare() * STT.getNumberOfShare()));
            seller.setContact(userInfo.getContact());
            seller.setEmail(userInfo.getEmail());
            seller.setIdentificationNumber(identificationNumber);
            seller.setName(userInfo.getName());
            seller.setNumberOfShare(STT.getNumberOfShare());
            seller.setSharePrice((float)STT.getCostPerShare());
            seller.setShareTransactionId(shareTransactionId);

            return new ModelAndView("sellerdata");
        }
        return new ModelAndView("redirect/login");
    }
    
    @RequestMapping(value = "/seller/vCredit/{identificationNumber}/{veeCreditTransactionId}", method = RequestMethod.GET)
    public ModelAndView showSellerVCredit(@ModelAttribute("seller") Sellers seller,@PathVariable Integer identificationNumber,@PathVariable Integer veeCreditTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(veeCreditTransactionId);
            if(VCT.getTransactionStatus().equals(TransactionStatus.PENDING.getTransactionStatusValue()) || VCT.getTransactionStatus().equals(TransactionStatus.LOCK.getTransactionStatusValue())){
                if(VCT.getTransactionStatus().equals(TransactionStatus.PENDING.getTransactionStatusValue())){
                    model.addAttribute("prop", "pending");
                }else{
                    model.addAttribute("prop", "lock");    
                }
                User userInfo = userService.retrieveUser(identificationNumber);
                seller.setvCredit(VCT.getVeeCredit());
                seller.setContact(userInfo.getContact());
                seller.setEmail(userInfo.getEmail());
                seller.setIdentificationNumber(identificationNumber);
                seller.setName(userInfo.getName());
                seller.setShareTransactionId(veeCreditTransactionId);
                UserBankDetail sellerBankDetail = userBankDetailService.retriveBank(identificationNumber);
                model.addAttribute("sellerBankDetail",sellerBankDetail);
                if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                    List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
                    model.addAttribute("notification", notification);                
                    InGameTraderDetail userIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                    model.addAttribute("userIGCD",userIGCD);
                    return new ModelAndView("sellervcreditdata");
                }else if(user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                    return new ModelAndView("moderatorsellervcreditdata");
                }
            }
        }
        return new ModelAndView("redirect/login");
    }
    
    @RequestMapping(value = "/shareSold", method = RequestMethod.POST)
    public String sellShare(@ModelAttribute("seller") Sellers seller,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            if(user.getIdentificationNumber() != seller.getIdentificationNumber()){
                logger.info("seller detail is"+seller.getName());
                ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTT(seller.getShareTransactionId());
                if(STT.getTransactionStatus() == TransactionStatus.PENDING.getTransactionStatusValue()){

                    LotDetails lot = lotDetailService.retrieveLotDetail();
                    Integer oldShare = lot.getNumberOfShare();
                    if(oldShare >= seller.getNumberOfShare()){
                        Transaction t = transactionService.retrieveTransaction(STT.getShareTradingTransactionToken());

                        t.setBuyerIN(user.getIdentificationNumber());

                        transactionService.updateTransaction(t);

                        STT.setTransactionStatus(TransactionStatus.COMPLETE.getTransactionStatusValue());
                        shareTradingTransactionService.updateSTT(STT);
                        InGameTraderDetail sellerDetail = inGameCreditDetailsService.retrieveIGCD(seller.getIdentificationNumber());
                        InGameTraderDetail buyerDetail = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());

                        float amount = seller.getNumberOfShare()*seller.getSharePrice();

                        buyerDetail.setIGC(buyerDetail.getIGC()-amount);
                        buyerDetail.setShareHold(buyerDetail.getShareHold()+seller.getNumberOfShare());
                        inGameCreditDetailsService.updateIGTD(buyerDetail);

                        float amountGiven = (float) (.90 * amount);
                        float amountAsVCredit = (float)0.70 * amountGiven;
                        float amountAsIGC = (float)0.30 * amountGiven;

                        float commision = (float).10 * amount;
                        sellerDetail.setvCredit(sellerDetail.getvCredit()+amountAsVCredit);
                        sellerDetail.setIGC(sellerDetail.getIGC()+amountAsIGC);

                        inGameCreditDetailsService.updateIGTD(sellerDetail);

                        VerificationModerator retrieveVM = verificationModeratorService.retrieveVM(t.getTransactionId());

                        verificationModeratorService.deleteVM(retrieveVM);

                        Notification notification = new Notification();
                        notification.setIdentificationNumber(seller.getIdentificationNumber());
                        notification.setNotificationStatus(Boolean.FALSE);
                        notification.setNotificationDate(new Date());
                        String link = "/veepropbeta/notification/"+STT.getShareTradingTransactionToken()+"/"+t.getTransactionType();
                        notification.setNotificationLink(link);
                        notification.setNotification("Your share request to sold "+seller.getNumberOfShare()+" share has been Completed and amount "+commision+""
                                + " USD has been detected as transaction charge to company.");

                        notificationService.saveNotification(notification);

                        Commission commission = new Commission();
                        commission.setAmmount(commision);
                        commission.setCommissionType(CommisionType.SHARE.getCommisionTypeValue());
                        commission.setTransactionId(t.getTransactionId());
                        commission.setUserIn(seller.getIdentificationNumber());

                        commissionService.saveCommission(commission); 

                        lot.setNumberOfShare(lot.getNumberOfShare()-seller.getNumberOfShare());
                        logger.info("Number of share in lotr is:"+lot.getNumberOfShare());
                        lotDetailService.updateShareCount(lot,seller.getNumberOfShare());

                    }else{
                        model.addAttribute("message","Only "+oldShare+" shares are available, Please Move to other Seller");
                       return "redirect:/seller/"+seller.getIdentificationNumber()+"/"+seller.getShareTransactionId(); 
                    }

                }else{
                    model.addAttribute("message","alrady purchased");
                }
            }else{
                model.addAttribute("message","You can not buy your own share");
                return "redirect:/seller/"+seller.getIdentificationNumber()+"/"+seller.getShareTransactionId();
            }
        }
        return "redirect:/login";
    }
    
    @RequestMapping(value = "/vCreditPurchaseRequest", method = RequestMethod.POST)
    public ModelAndView sellVCredit(@ModelAttribute("seller") Sellers seller,HttpServletRequest request , @RequestParam("file") MultipartFile file,ModelMap model) throws IOException{
        Login user = (Login)model.get("user");
        if(user != null && (user.getUserRole().equals(UserRole.TRADER.getUserRoleValue()) || user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue()))){
            VeeCreditTransaction veeCreditTransaction = veeCreditTransactionService.retrieveVCT(seller.getShareTransactionId());
            Transaction transaction = transactionService.retrieveTransaction(veeCreditTransaction.getVeeCreditTransactionToken());
            transaction.setBuyerIN(user.getIdentificationNumber());
            transactionService.updateTransaction(transaction);
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");
            byte[] fileData = file.getBytes();
            String extension ="";
            if (fileData.length != 0) {
                extension = FilenameUtils.getExtension(file.getOriginalFilename());
                logger.info("Extension is:"+extension);
                String fileName = absoluteFilesystemPath +"\\" +"veecredittransactionproof"+ transaction.getTransactionId()+"."+extension;
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                fileOutputStream.write(fileData);
                fileOutputStream.close();
            }
            
            Notification notification = new Notification();
            notification.setIdentificationNumber(seller.getIdentificationNumber());
            notification.setNotificationStatus(Boolean.FALSE);
            notification.setNotificationDate(new Date());
            String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue();
            notification.setNotificationLink(link);
            notification.setNotification("Your request to sell "+seller.getvCredit()+" has been accepted by some user. Approve this");

            notificationService.saveNotification(notification);
            
            Notification notificationNew = new Notification();
            notificationNew.setIdentificationNumber(seller.getIdentificationNumber());
            notificationNew.setNotificationStatus(Boolean.FALSE);
            notificationNew.setNotificationDate(new Date());
            notificationNew.setNotificationLink(link);
            notificationNew.setNotification("Your request to purchase "+seller.getvCredit()+" has been sent. You will be informed when it is accepted.");

            notificationService.saveNotification(notificationNew);
            
            veeCreditTransaction.setTransactionStatus(TransactionStatus.HOLD.getTransactionStatusValue());
            veeCreditTransaction.setFileName("veecredittransactionproof"+ transaction.getTransactionId()+"."+extension);
            veeCreditTransaction.setBankTransactionId(seller.getBankTransactionId());
            veeCreditTransaction.setVeeCreditTransactionType(VeeCreditTransactionType.FROMTRADER.getVeeCreditTransactionTypeValue());
            veeCreditTransactionService.updateVCT(veeCreditTransaction);
        }
        return new ModelAndView("redirect:/login");
    }    
    
    @RequestMapping(value = "/vcredit/buyers", method = RequestMethod.GET)
    public ModelAndView viewBuyers(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);
            ArrayList<Sellers> buyersList =  new ArrayList<Sellers>();
            List<VeeCreditTransaction> veeCreditTransactionList = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.HOLD.getTransactionStatusValue());
            List<VeeCreditTransaction> veeCreditTransactionListLock = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.LOCK.getTransactionStatusValue());
            for(VeeCreditTransaction veeCreditTransaction : veeCreditTransactionList){
                Transaction transaction = transactionService.retrieveTransaction(veeCreditTransaction.getVeeCreditTransactionToken());
                if(transaction.getSellerIN() == user.getIdentificationNumber()){
                    Sellers buyers = new Sellers();
                    User buyerInfo = userService.retrieveUser(transaction.getBuyerIN());
                    buyers.setContact(buyerInfo.getContact());
                    buyers.setEmail(buyerInfo.getEmail());
                    buyers.setIdentificationNumber(buyerInfo.getIdentificationNumber());
                    buyers.setName(buyerInfo.getName());
                    buyers.setvCredit(transaction.getAmount());
                    buyers.setFileName(veeCreditTransaction.getFileName());
                    buyers.setShareTransactionId(veeCreditTransaction.getVeeCreditTransactionId());
                    buyersList.add(buyers);
                }
            }
            for(VeeCreditTransaction veeCreditTransaction : veeCreditTransactionListLock){
                Transaction transaction = transactionService.retrieveTransaction(veeCreditTransaction.getVeeCreditTransactionToken());
                if(transaction.getSellerIN() == user.getIdentificationNumber()){
                    Sellers buyers = new Sellers();
                    User buyerInfo = userService.retrieveUser(transaction.getBuyerIN());
                    buyers.setContact(buyerInfo.getContact());
                    buyers.setEmail(buyerInfo.getEmail());
                    buyers.setIdentificationNumber(buyerInfo.getIdentificationNumber());
                    buyers.setName(buyerInfo.getName());
                    buyers.setvCredit(transaction.getAmount());
                    buyers.setFileName("no_file_yet");
                    buyers.setShareTransactionId(veeCreditTransaction.getVeeCreditTransactionId());
                    buyersList.add(buyers);
                }
            }
            List<VeeCreditTransaction> veeCreditTransactionListDispute = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.DISPUTE.getTransactionStatusValue());
            for(VeeCreditTransaction veeCreditTransaction : veeCreditTransactionListDispute){
                Transaction transaction = transactionService.retrieveTransaction(veeCreditTransaction.getVeeCreditTransactionToken());
                if(transaction.getSellerIN() == user.getIdentificationNumber()){
                    Sellers buyers = new Sellers();
                    User buyerInfo = userService.retrieveUser(transaction.getBuyerIN());
                    buyers.setContact(buyerInfo.getContact());
                    buyers.setEmail(buyerInfo.getEmail());
                    buyers.setIdentificationNumber(buyerInfo.getIdentificationNumber());
                    buyers.setName(buyerInfo.getName());
                    buyers.setvCredit(transaction.getAmount());
                    buyers.setFileName(veeCreditTransaction.getFileName());
                    buyers.setShareTransactionId(veeCreditTransaction.getVeeCreditTransactionId());
                    buyersList.add(buyers);
                }
            }            
            logger.info("Buyers Size is:"+buyersList.size());
            model.addAttribute("buyersList",buyersList);
            return new ModelAndView("acceptbuyer");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/lockTransaction/{in}/{veeCreditTransactionId}", method = RequestMethod.GET)
    public String lockTransaction(@PathVariable Integer in,@PathVariable Integer veeCreditTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null ){
            if(user.getIdentificationNumber() != in){
                VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(veeCreditTransactionId);
                if(user.getIdentificationNumber()!=  in && VCT.getTransactionStatus()== TransactionStatus.LOCK.getTransactionStatusValue()){
                    logger.info("Step 1");
                    model.addAttribute("message", "Already Purchased by Some other user");
                    return "redirect:/login";
                }else{
                    logger.info("Step 2");
                    VCT.setTransactionStatus(TransactionStatus.LOCK.getTransactionStatusValue());
                    veeCreditTransactionService.updateVCT(VCT);

                    Notification notificationNew = new Notification();
                    notificationNew.setIdentificationNumber(in);
                    notificationNew.setNotificationStatus(Boolean.FALSE);
                    notificationNew.setNotificationDate(new Date());
                    String link = "/veepropbeta/notification/"+VCT.getVeeCreditTransactionToken()+"/"+TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue();
                    notificationNew.setNotificationLink(link);
                    notificationNew.setNotification("Your request to sell vCredit has been Locked by "+userService.retrieveUser(user.getIdentificationNumber()).getName());

                    notificationService.saveNotification(notificationNew); 

                    Transaction retrieveTransaction = transactionService.retrieveTransaction(VCT.getVeeCreditTransactionToken());
                    retrieveTransaction.setBuyerIN(user.getIdentificationNumber());
                    transactionService.updateTransaction(retrieveTransaction);
                    if(user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                        return "redirect:/seller/vCredit/"+in+"/"+veeCreditTransactionId;
                    }else if(user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
                        return "redirect:/pendingvcredit";
                    }
                }
            }else{
                logger.info("Step 3");
                model.addAttribute("notAllowed","You Can not lock your own Transaction");
            }
        }
        return "redirect:/login";
    }
    
    @RequestMapping(value = "/retrieveLockedTransaction", method = RequestMethod.GET)
    public ModelAndView retrieveLockedTransaction(ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);            
            ArrayList<Sellers> yourLockedTransaction =  new ArrayList<Sellers>();
            List<VeeCreditTransaction> retrieveLockedTransaction = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.LOCK.getTransactionStatusValue());
            for(VeeCreditTransaction vct : retrieveLockedTransaction){
                Transaction transaction = transactionService.retrieveTransaction(vct.getVeeCreditTransactionToken());
                if(transaction.getBuyerIN() == user.getIdentificationNumber()){
                    Sellers seller = new Sellers();
                    seller.setIdentificationNumber(transaction.getSellerIN());
                    seller.setShareTransactionId(vct.getVeeCreditTransactionId());
                    seller.setName(userService.retrieveUser(transaction.getSellerIN()).getName());
                    seller.setvCredit(transaction.getAmount());
                    seller.setFileName(vct.getFileName());
                    yourLockedTransaction.add(seller);
                }
            }
            List<VeeCreditTransaction> retrieveLockedTransactionHold = veeCreditTransactionService.retrievePendingTransaction(TransactionStatus.HOLD.getTransactionStatusValue());
            for(VeeCreditTransaction vct : retrieveLockedTransactionHold){
                Transaction transaction = transactionService.retrieveTransaction(vct.getVeeCreditTransactionToken());
                if(transaction.getBuyerIN() == user.getIdentificationNumber()){
                    Sellers seller = new Sellers();
                    seller.setIdentificationNumber(transaction.getSellerIN());
                    seller.setShareTransactionId(vct.getVeeCreditTransactionId());
                    seller.setName(userService.retrieveUser(transaction.getSellerIN()).getName());
                    seller.setvCredit(transaction.getAmount());
                    seller.setFileName(vct.getFileName());
                    yourLockedTransaction.add(seller);
                }
            }
            model.addAttribute("yourLockedTransaction",yourLockedTransaction);
            return new ModelAndView("lockedtransaction");
        }
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/accept/{buyerIN}/{veeCreditTransactionId}", method = RequestMethod.GET)
    public ModelAndView acceptTransaction(@PathVariable Integer buyerIN,@PathVariable Integer veeCreditTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            logger.info("seller detail is"+buyerIN);
            
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(veeCreditTransactionId);
            if(VCT.getTransactionStatus() == TransactionStatus.HOLD.getTransactionStatusValue() || VCT.getTransactionStatus() == TransactionStatus.DISPUTE.getTransactionStatusValue()){
                Transaction t = transactionService.retrieveTransaction(VCT.getVeeCreditTransactionToken());
                VCT.setTransactionStatus(TransactionStatus.COMPLETE.getTransactionStatusValue());
                veeCreditTransactionService.updateVCT(VCT);
                String userRole = loginService.retrieveUser(buyerIN).getUserRole();
                if(userRole.equals(UserRole.TRADER.getUserRoleValue())){
                    InGameTraderDetail sellerDetail = inGameCreditDetailsService.retrieveIGCD(buyerIN);
                    InGameTraderDetail buyerDetail = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());

                    sellerDetail.setvCredit(sellerDetail.getvCredit() + t.getAmount());

                    //TraderDynamicData TDD = traderDynamicDataService.retrieveTDD(buyerIN);
                    //LevelDecide ld = new LevelDecide(TDD.getPackageAmount()+t.getAmount());
                    //int oldLevel = sellerDetail.getLevelIN();
                    buyerDetail.setvCredit(buyerDetail.getvCredit()+(t.getAmount()*.1));
                    buyerDetail.setvCreditHold(buyerDetail.getvCreditHold() - (t.getAmount()*.1));
                    //sellerDetail.setLevelIN(ld.getLevelDecided());

                    inGameCreditDetailsService.updateIGTD(buyerDetail);       
                    inGameCreditDetailsService.updateIGTD(sellerDetail);

                    //TDD.setPackageAmount(TDD.getPackageAmount() +t.getAmount());
                    //traderDynamicDataService.updateTDD(TDD);
                    
                    /*
                    if(oldLevel < ld.getLevelDecided()){
                        Notification notificationLevel = new Notification();
                        notificationLevel.setIdentificationNumber(buyerIN);
                        notificationLevel.setNotificationStatus(Boolean.FALSE);
                        notificationLevel.setNotificationDate(new Date());
                        String link = "/veepropbeta/notification/"+(-1)+"/"+0;
                        notificationLevel.setNotificationLink(link);
                        notificationLevel.setNotification("Level updated to "+ld.getLevelDecided());

                        notificationService.saveNotification(notificationLevel);
                    }
                    */
                    
                    VerificationModerator retrieveVM = verificationModeratorService.retrieveVM(t.getTransactionId());
                    verificationModeratorService.deleteVM(retrieveVM);
                }else if(userRole.equals(UserRole.MODERATOR.getUserRoleValue())){
                    
                    VerificationModerator retrieveVM = verificationModeratorService.retrieveVM(t.getTransactionId());
                    
                    retrieveVM.setVerificationDate(new Date());
                    retrieveVM.setIdentificationNumber(buyerIN);
                    retrieveVM.setVerificationStatus(Boolean.TRUE);
                    retrieveVM.setVerificationTime(new Date(System.currentTimeMillis()));
                    verificationModeratorService.updateVM(retrieveVM); 
                    
                    InGameTraderDetail buyerDetail = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
                    buyerDetail.setvCreditHold(buyerDetail.getvCreditHold() - (t.getAmount()*.1));
                    buyerDetail.setvCredit(buyerDetail.getvCredit()+t.getAmount()*.1);
                    inGameCreditDetailsService.updateIGTD(buyerDetail); 
                }
                
                

                Notification notification = new Notification();
                notification.setIdentificationNumber(buyerIN);
                notification.setNotificationStatus(Boolean.FALSE);
                notification.setNotificationDate(new Date());
                String link = "/veepropbeta/notification/"+VCT.getVeeCreditTransactionToken()+"/"+TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue();
                notification.setNotificationLink(link);
                notification.setNotification("Your request to purchase "+t.getAmount()+" has been Completed. Amont credited to your account");

                notificationService.saveNotification(notification);
                
            }else{
                logger.info("already purchased");
                model.addAttribute("message","alrady purchased");
            }
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/reject/{buyerIN}/{veeCreditTransactionId}", method = RequestMethod.GET)
    public ModelAndView rejectTransaction(@PathVariable Integer buyerIN,@PathVariable Integer veeCreditTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(veeCreditTransactionId);
            if(VCT.getTransactionStatus() == TransactionStatus.HOLD.getTransactionStatusValue()){
                Transaction t = transactionService.retrieveTransaction(VCT.getVeeCreditTransactionToken());
                VCT.setTransactionStatus(TransactionStatus.PENDING.getTransactionStatusValue());
                VCT.setBankTransactionId(null);
                VCT.setFileName(null);
                VCT.setVeeCreditTransactionType(null);
                veeCreditTransactionService.updateVCT(VCT);
                
                t.setBuyerIN(null);
                
                transactionService.updateTransaction(t);
                
               
                Notification notification = new Notification();
                notification.setIdentificationNumber(buyerIN);
                notification.setNotificationStatus(Boolean.FALSE);
                notification.setNotificationDate(new Date());
                String link = "/veepropbeta/notification/"+VCT.getVeeCreditTransactionToken()+"/"+TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue();
                notification.setNotificationLink(link);
                notification.setNotification("Your request purchase "+t.getAmount()+" has been Rejected ");

                notificationService.saveNotification(notification);
                
            }else{
                logger.info("already purchased");
                model.addAttribute("message","alrady purchased");
            }
        }
        return new ModelAndView("redirect:/login");
    }    
    
    @RequestMapping(value = "/bandetail/{userIn}", method = RequestMethod.GET)
    public ModelAndView bankDetail(@PathVariable Integer userIn,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            UserBankDetail userBank = profileService.retriveBank(userIn);
            model.addAttribute("userBank",userBank);
            return new ModelAndView("bankdetail");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/dispute/{veeCreditTransactionId}", method = RequestMethod.GET)
    public ModelAndView showDispute(@ModelAttribute("dispute") Dispute dispute,
            @PathVariable Integer veeCreditTransactionId,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(veeCreditTransactionId);
            Transaction transaction = transactionService.retrieveTransaction(VCT.getVeeCreditTransactionToken());
            dispute.setTransactionId(transaction.getTransactionId());
            dispute.setProofOfTransaction(VCT.getFileName());
            dispute.setSellerIN(transaction.getSellerIN());
            dispute.setBuyerIN(transaction.getBuyerIN());
            dispute.setVeeCreditTransactionId(veeCreditTransactionId);
            dispute.setVeeCreditAmount(VCT.getVeeCredit());
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);               
            return new ModelAndView("dispute");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/search/trader/{str1}", method = RequestMethod.GET )
    public ModelAndView findTraderInLine(@PathVariable String str1,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            String str = str1.replace('&', '.');
            int indexOf = str.indexOf('@');
            logger.info("indexOf"+indexOf);
            Integer IN = 0;
            if(indexOf < 0){
                Login retrieveUser = loginService.retrieveUser(str);
                if(retrieveUser != null){
                    IN = retrieveUser.getIdentificationNumber();
                }else{
                    model.addAttribute("note","No User Found");
                }
            }else{
                logger.info("Email is:"+str);
                User userByMail = userService.retrieveUserByMail(str);
                if(userByMail != null){
                    IN = userByMail.getIdentificationNumber();
                }else{
                    model.addAttribute("note","No User Found");
                }
            }
            logger.info("identified user is:"+IN);
            User userInfo = null;
            if(IN != 0){
                TraderRelativeData retrieveBinaryParent = traderRelativeDataService.retrieveBinaryParent(IN);
                Integer BP = retrieveBinaryParent.getbP();
                Boolean check = checkParent(BP, user.getIdentificationNumber(),model,retrieveBinaryParent.getIdentificationNumber());
                logger.info("value returned by function is:"+check);
                if(check == true){
                    userInfo = userService.retrieveUser(IN);
                    logger.info("In Child");
                }else{
                    logger.info("Not In Child");
                    model.addAttribute("note","Not In Child");
                }
            }else{
                model.addAttribute("note","No User Found");
            }
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);   
            model.addAttribute("userInfo",userInfo); 
            return new ModelAndView("tree");
        }
        return new ModelAndView("redirect:/login",model);
    }
    
    Boolean checkParent(Integer BP, Integer IN,ModelMap model, Integer searchedUser){
        if(BP != 0){
            logger.info("if 1");
            //calculate position
            String position = null; 
            Integer ptp = traderRelativeDataService.retrieveBinaryParent(searchedUser).getpTP();
            logger.info("PTP is:"+ptp);
            if(ptp == 0){
                logger.info("true 1");
                position = "Left";
            }else{
                logger.info("true 2");
                position = "Right";
            }            
            logger.info("Position is:"+position);
            model.addAttribute("position",position);
            
            //find in tree
            if(BP == IN){
                logger.info("if 2");
                return true;
            }else{
                logger.info("else 2");
                checkParent(traderRelativeDataService.retrieveBinaryParent(BP).getbP(), IN , model, BP);
            }
            return true;
       }else{
            logger.info("else 1");
            return false;
        }
    }
    
    @RequestMapping(value = "/dispute/trader", method = RequestMethod.POST)
    public ModelAndView traderDisputeRequest(@ModelAttribute("dispute")Dispute dispute,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null){
            
            List<Integer> userList = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
            Random rand = new Random();
            int randomNum = rand.nextInt(userList.size());
            Integer moderatorIdentificationNumber = userList.get(randomNum);            
            
            VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCT(dispute.getVeeCreditTransactionId());
            VCT.setTransactionStatus(TransactionStatus.DISPUTE.getTransactionStatusValue());
            veeCreditTransactionService.updateVCT(VCT);
            dispute.setModeratorIN(moderatorIdentificationNumber);
            disputeService.saveDispute(dispute);
            return new ModelAndView("redirect:/retrieveLockedTransaction");
        }
        return new ModelAndView("redirect:/login");
    }
}