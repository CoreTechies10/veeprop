/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import javax.persistence.Column;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
public class Address {

    private Integer IdentificationNumber;
    
    @NotBlank(message = "Please Enter a Houser Number")
    private String houseNumber;

    @NotBlank(message = "Please Enter a Street Name1")
    private String streetName1;

    @NotBlank(message = "Please Enter a Street Name2")
    private String streetName2;

    @NotBlank(message = "Please Enter a City Name")
    private String city;
    
    @NotBlank(message = "please Enter a LandMark Name")
    private String landmark;

    @NotBlank(message = "Please Enter a UserState")
    private String userstate;

    @NotBlank(message = "Please Enter a Country")
    private String country;

    @NotBlank(message = "Please Enter a ZipCode")
    private String zipcode;

    public Integer getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(Integer IdentificationNumber) {
        this.IdentificationNumber = IdentificationNumber;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName1() {
        return streetName1;
    }

    public void setStreetName1(String streetName1) {
        this.streetName1 = streetName1;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getUserstate() {
        return userstate;
    }

    public void setUserstate(String userstate) {
        this.userstate = userstate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    
    

}
