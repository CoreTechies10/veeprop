/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.algorithm;

/**
 *
 * @author Noppp
 */
public enum UserRole {

    SUPERADMIN("1"),

    ADMIN("2"),

    MODERATOR("3"),

    TRADER("4");

    private final String userRoleValue;

    private UserRole(String userRoleValue) {
        this.userRoleValue = userRoleValue;
    }

    public String getUserRoleValue() {
        return userRoleValue;
    }

    public static UserRole parse(String userRoleValue) {
        UserRole userRole = null;
        for (UserRole item : UserRole.values()) {
            if (item.getUserRoleValue().equals(userRoleValue)) {
                userRole = item;
                break;
            }
        }
        return userRole;
    }
}

