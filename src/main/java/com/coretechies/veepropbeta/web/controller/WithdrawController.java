/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.InGameTraderDetail;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.UserBankDetail;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.domain.VerificationModerator;
import com.coretechies.veepropbeta.service.ingamecreditdetails.InGameCreditDetailsService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.userbankdetail.UserBankDetailService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.service.verificationmoderator.VerificationModeratorService;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Noppp
 */

@Controller
@SessionAttributes({"user"})
public class WithdrawController {
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private InGameCreditDetailsService inGameCreditDetailsService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private TransactionService transactionService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private UserBankDetailService userBankDetailService;
    
    @Autowired
    private VerificationModeratorService verificationModeratorService;
    
    @RequestMapping(value = "/withdraw", method = RequestMethod.GET)
    public ModelAndView withdraw(@ModelAttribute("withdraw") InGameTraderDetail withdraw,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            
            UserBankDetail bankInfo = userBankDetailService.retriveBank(user.getIdentificationNumber());
            if(bankInfo.getAccountNumber() == null){
                model.addAttribute("update","To withdraw Vee Credit first fill your account Details");
                return new ModelAndView("redirect:/updatebankinfo");
            }
            
            InGameTraderDetail userIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification);            
            model.addAttribute("vCredit", userIGCD.getvCredit());
            return new ModelAndView("withdraw");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public ModelAndView withdrawVCredit(@ModelAttribute("withdraw") InGameTraderDetail withdraw,ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            
            Double vCreditWithdraw = withdraw.getvCredit();
            Double vCreditHold = .1 * vCreditWithdraw;
            InGameTraderDetail userIGCD = inGameCreditDetailsService.retrieveIGCD(user.getIdentificationNumber());
            userIGCD.setvCredit(userIGCD.getvCredit() - vCreditHold - vCreditWithdraw);
            userIGCD.setvCreditHold(vCreditHold);
            inGameCreditDetailsService.updateIGTD(userIGCD);
            
            Transaction transaction = new Transaction();
            transaction.setAmount(vCreditWithdraw);
            transaction.setSellerIN(user.getIdentificationNumber());
            transaction.setTransactionDate(new Date());
            transaction.setTransactionTime(new Date(System.currentTimeMillis()));
            transaction.setTransactionType(TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue());
            
            transactionService.saveTransaction(transaction);
            
            Notification notification = new Notification();
            notification.setIdentificationNumber(user.getIdentificationNumber());
            notification.setNotificationStatus(Boolean.FALSE);
            notification.setNotificationDate(new Date());
            String link = "/veepropbeta/notification/"+transaction.getTransactionId()+"/"+transaction.getTransactionType();
            notification.setNotificationLink(link);
            notification.setNotification("You have requested to withdraw "+vCreditWithdraw+" USD vCredit. You will be informed when transaction get completed");
            
            notificationService.saveNotification(notification);            
            
            VeeCreditTransaction vct = new VeeCreditTransaction();
            vct.setTransactionStatus(TransactionStatus.PENDING.getTransactionStatusValue());
            vct.setVeeCreditTransactionToken(transaction.getTransactionId());
            vct.setRemark("Request to sell vCredit");
            vct.setVeeCredit(transaction.getAmount());
            
            veeCreditTransactionService.saveVCT(vct);
 
            List<Integer> userList = loginService.retrieveUserByRole(UserRole.MODERATOR.getUserRoleValue());
                
            //generate random moderator
            Random rand = new Random();
            int randomNum = rand.nextInt(userList.size());
            Integer moderatorIdentificationNumber = userList.get(randomNum);
                
            VerificationModerator verificationModerator = new VerificationModerator();
            verificationModerator.setIdentificationNumber(moderatorIdentificationNumber);
            verificationModerator.setTransactionId(transaction.getTransactionId());
            verificationModerator.setComment("vCredit Withdraw Request");
            verificationModerator.setVerificationStatus(Boolean.FALSE);

            verificationModeratorService.saveVerificationModerator(verificationModerator);           
    
            return new ModelAndView("redirect:/withdraw");
        }
        return new ModelAndView("redirect:/login");
    }    
}
