/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller;

import com.coretechies.veepropbeta.domain.BonusTransaction;
import com.coretechies.veepropbeta.domain.Login;
import com.coretechies.veepropbeta.domain.Notification;
import com.coretechies.veepropbeta.domain.ShareTradingTransaction;
import com.coretechies.veepropbeta.domain.Transaction;
import com.coretechies.veepropbeta.domain.User;
import com.coretechies.veepropbeta.domain.UserAddressInfo;
import com.coretechies.veepropbeta.domain.VeeCreditTransaction;
import com.coretechies.veepropbeta.service.bonustransaction.BonusTransactionService;
import com.coretechies.veepropbeta.service.login.LoginService;
import com.coretechies.veepropbeta.service.notification.NotificationService;
import com.coretechies.veepropbeta.service.profile.ProfileService;
import com.coretechies.veepropbeta.service.sharetradingtransaction.ShareTradingTransactionService;
import com.coretechies.veepropbeta.service.transaction.TransactionService;
import com.coretechies.veepropbeta.service.user.UserService;
import com.coretechies.veepropbeta.service.veecredittransaction.VeeCreditTransactionService;
import com.coretechies.veepropbeta.web.controller.algorithm.CommisionType;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionStatus;
import com.coretechies.veepropbeta.web.controller.algorithm.TransactionType;
import com.coretechies.veepropbeta.web.controller.algorithm.UserRole;
import com.coretechies.veepropbeta.web.controller.webdomain.TransactionHistory;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Arvind
 */
@Controller
@SessionAttributes("user")
public class TransactionHistoryController {
    
    private final Logger logger =LoggerFactory.getLogger(getClass());
    
    @Autowired
    private TransactionService transactionservice;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private VeeCreditTransactionService veeCreditTransactionService;
    
    @Autowired
    private ShareTradingTransactionService shareTradingTransactionService;
    
    @Autowired
    private BonusTransactionService bonusTransactionService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private LoginService loginService;
    
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
    
    
    @RequestMapping(value="/TranactionHistory",method = RequestMethod.GET)
    public ModelAndView showHistory(@ModelAttribute("transaction") TransactionHistory transactionHistory, ModelMap model)throws IOException{
        Login user  =  (Login) model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            List<Notification> notification = notificationService.retreiveNotification(user.getIdentificationNumber());
            model.addAttribute("notification", notification); 
            return  new ModelAndView("TranactionHistory");
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value="/TranactionHistory",method = RequestMethod.POST)
    public ModelAndView FilterTransaction(@Valid @ModelAttribute("transaction") TransactionHistory transactionHistory,BindingResult result,HttpServletResponse response,
                                    HttpServletRequest request,ModelMap model) throws BadElementException, IOException {
        Login login=(Login)model.get("user");
        if(login != null && login.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
            if(result.hasErrors()){
                return new ModelAndView("TranactionHistory");
            }
            ArrayList<Transaction> transactionBuy = new ArrayList<Transaction>();
            ArrayList<Transaction> transactionSell =new ArrayList<Transaction>();
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");
            
            //Image image1 = Image.getInstance(absoluteFilesystemPath+"\\img\\ReportLogo1.png");            
            
            if(transactionHistory.getTransactionType() == TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue()){
                List<Transaction> transactionSelled =  transactionservice.retrieveSellTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                List<Transaction> transactionBaught =  transactionservice.retrieveBuyTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                if(transactionBaught != null){
                    for(Transaction transaction : transactionBaught){
                        VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCTByTransaction(transaction.getTransactionId());
                        if(VCT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionBuy.add(transaction);
                        }
                    }
                }
                if(transactionSelled != null){
                    for(Transaction transaction : transactionSelled){
                        VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCTByTransaction(transaction.getTransactionId());
                        if(VCT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionSell.add(transaction);
                        }
                    }
                }
                PdfPTable table = new PdfPTable(5);

                PdfPCell cell1 = new PdfPCell(new Phrase("vCredit"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Seller"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Buyer"));
                PdfPCell cell5 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(login.getIdentificationNumber());
                UserAddressInfo retriveAddress = profileService.retriveAddress(login.getIdentificationNumber());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);

                for (Transaction transaction: transactionBuy) {
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId())); 
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
            }else if(transactionHistory.getTransactionType() == TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue()){
                List<Transaction> transactionSelled = transactionservice.retrieveSellTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                List<Transaction> transactionBaught = transactionservice.retrieveBuyTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                
                if(transactionBaught != null){
                    for(Transaction transaction : transactionBaught){
                        ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                        if(STT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionBuy.add(transaction);
                        }
                    }
                }
                if(transactionSelled != null){
                    for(Transaction transaction : transactionSelled){
                        ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                        if(STT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionSell.add(transaction);
                        }
                    }
                }

                PdfPTable table = new PdfPTable(7);

                PdfPCell cell1 = new PdfPCell(new Phrase("IGC"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Share"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Price"));
                PdfPCell cell5 = new PdfPCell(new Phrase("Seller"));
                PdfPCell cell6 = new PdfPCell(new Phrase("Buyer"));
                PdfPCell cell7 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                cell6.setPadding(5);
                cell7.setPadding(5);
                
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                table.addCell(cell6);
                table.addCell(cell7);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(login.getIdentificationNumber());
                UserAddressInfo retriveAddress = profileService.retriveAddress(login.getIdentificationNumber());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);



                for (Transaction transaction: transactionBuy) {
                    ShareTradingTransaction retrieveSTTByTransaction = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getNumberOfShare()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getCostPerShare()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    ShareTradingTransaction retrieveSTTByTransaction = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getNumberOfShare()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getCostPerShare()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
                
            }else{
                transactionSell = (ArrayList<Transaction>) transactionservice.retrieveSellTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                transactionBuy = (ArrayList<Transaction>) transactionservice.retrieveBuyTransactionByDates(login.getIdentificationNumber(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());

                PdfPTable table = new PdfPTable(4);

                PdfPCell cell1 = new PdfPCell(new Phrase("Bonus"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Bonus Type"));
                PdfPCell cell4 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(login.getIdentificationNumber());
                UserAddressInfo retriveAddress = profileService.retriveAddress(login.getIdentificationNumber());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);



                for (Transaction transaction: transactionBuy) {
                    BonusTransaction bonus = bonusTransactionService.retrieveBonusTransactionByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(CommisionType.BONUS.getCommisionType(bonus.getBonusType())));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    BonusTransaction bonus = bonusTransactionService.retrieveBonusTransactionByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(CommisionType.BONUS.getCommisionType(bonus.getBonusType())));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
            }
            return new ModelAndView("redirect:/TranactionHistory");
        }    
        return new ModelAndView("redirect:/login");   
    }
    
    @RequestMapping(value = "/generate/report/{in}", method = RequestMethod.GET)
    public ModelAndView generateReport(@ModelAttribute("transaction") TransactionHistory transactionHistory,
            @PathVariable Integer in, ModelMap model){
        Login user = (Login)model.get("user");
        if(user != null && user.getUserRole().equals(UserRole.MODERATOR.getUserRoleValue())){
            Login retrieveUser = loginService.retrieveUser(in);
            if(retrieveUser.getUserRole().equals(UserRole.TRADER.getUserRoleValue())){
                transactionHistory.setUserIN(in);
                model.addAttribute("name",userService.retrieveUser(in).getName());
                return new ModelAndView("report");
            }
        }
        return new ModelAndView("redirect:/login");
    }
    
    @RequestMapping(value = "/generate/report", method = RequestMethod.POST)
    public ModelAndView generateReport(@ModelAttribute("transaction") TransactionHistory transactionHistory,
            HttpServletResponse response, HttpServletRequest request,ModelMap model) throws BadElementException, IOException{
        
        
            ArrayList<Transaction> transactionBuy = new ArrayList<Transaction>();
            ArrayList<Transaction> transactionSell =new ArrayList<Transaction>();
            
            ServletContext servletContext = request.getSession().getServletContext();
            String absoluteFilesystemPath = servletContext.getRealPath("/");
            
            //Image image1 = Image.getInstance(absoluteFilesystemPath+"\\img\\ReportLogo1.png");            
            
            if(transactionHistory.getTransactionType() == TransactionType.VEECREDITTRANSACTION.getTransactionTypeValue()){
                List<Transaction> transactionSelled =  transactionservice.retrieveSellTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                List<Transaction> transactionBaught =  transactionservice.retrieveBuyTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                if(transactionBaught != null){
                    for(Transaction transaction : transactionBaught){
                        VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCTByTransaction(transaction.getTransactionId());
                        if(VCT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionBuy.add(transaction);
                        }
                    }
                }
                if(transactionSelled != null){
                    for(Transaction transaction : transactionSelled){
                        VeeCreditTransaction VCT = veeCreditTransactionService.retrieveVCTByTransaction(transaction.getTransactionId());
                        if(VCT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionSell.add(transaction);
                        }
                    }
                }
                PdfPTable table = new PdfPTable(5);

                PdfPCell cell1 = new PdfPCell(new Phrase("vCredit"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Seller"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Buyer"));
                PdfPCell cell5 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(transactionHistory.getUserIN());
                UserAddressInfo retriveAddress = profileService.retriveAddress(transactionHistory.getUserIN());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);

                for (Transaction transaction: transactionBuy) {
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId())); 
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
            }else if(transactionHistory.getTransactionType() == TransactionType.SHARETRADINGTRANSACTION.getTransactionTypeValue()){
                List<Transaction> transactionSelled = transactionservice.retrieveSellTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                List<Transaction> transactionBaught = transactionservice.retrieveBuyTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                
                if(transactionBaught != null){
                    for(Transaction transaction : transactionBaught){
                        ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                        if(STT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionBuy.add(transaction);
                        }
                    }
                }
                if(transactionSelled != null){
                    for(Transaction transaction : transactionSelled){
                        ShareTradingTransaction STT = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                        if(STT.getTransactionStatus() == TransactionStatus.COMPLETE.getTransactionStatusValue()){
                            transactionSell.add(transaction);
                        }
                    }
                }

                PdfPTable table = new PdfPTable(7);

                PdfPCell cell1 = new PdfPCell(new Phrase("IGC"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Share"));
                PdfPCell cell4 = new PdfPCell(new Phrase("Price"));
                PdfPCell cell5 = new PdfPCell(new Phrase("Seller"));
                PdfPCell cell6 = new PdfPCell(new Phrase("Buyer"));
                PdfPCell cell7 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                cell5.setPadding(5);
                cell6.setPadding(5);
                cell7.setPadding(5);
                
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell5);
                table.addCell(cell6);
                table.addCell(cell7);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(transactionHistory.getUserIN());
                UserAddressInfo retriveAddress = profileService.retriveAddress(transactionHistory.getUserIN());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);



                for (Transaction transaction: transactionBuy) {
                    ShareTradingTransaction retrieveSTTByTransaction = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getNumberOfShare()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getCostPerShare()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    ShareTradingTransaction retrieveSTTByTransaction = shareTradingTransactionService.retrieveSTTByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getNumberOfShare()));
                    table.addCell(String.valueOf(retrieveSTTByTransaction.getCostPerShare()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getSellerIN()).getName()));
                    table.addCell(String.valueOf(userService.retrieveUser(transaction.getBuyerIN()).getName()));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
                
            }else{
                transactionSell = (ArrayList<Transaction>) transactionservice.retrieveSellTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());
                transactionBuy = (ArrayList<Transaction>) transactionservice.retrieveBuyTransactionByDates(transactionHistory.getUserIN(),transactionHistory.getInitialDate(),transactionHistory.getEndDate(),transactionHistory.getTransactionType());

                PdfPTable table = new PdfPTable(4);

                PdfPCell cell1 = new PdfPCell(new Phrase("Bonus"));// we add a cell with colspan 3
                PdfPCell cell2 = new PdfPCell(new Phrase("TransactionDate"));
                PdfPCell cell3 = new PdfPCell(new Phrase("Bonus Type"));
                PdfPCell cell4 = new PdfPCell(new Phrase("TransactionID"));

                table.setWidthPercentage(100);

                cell1.setPadding(5);
                cell2.setPadding(5);
                cell3.setPadding(5);
                cell4.setPadding(5);
                
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                
                Font font1 = new Font(Font.FontFamily.TIMES_ROMAN  ,15, Font.BOLDITALIC);
                Font font3 = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                Font font4 = new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLDITALIC);
                
                User retrieveUser = userService.retrieveUser(transactionHistory.getUserIN());
                UserAddressInfo retriveAddress = profileService.retriveAddress(transactionHistory.getUserIN());
                
                String name="Transaction history Statement";
                Paragraph paragraph = new Paragraph(name.toUpperCase(),font1); 
                Paragraph paraName=new Paragraph(retrieveUser.getName().toUpperCase(),font3);
                Paragraph paraAddress=new Paragraph(retriveAddress.getHouseNumber().toUpperCase()+","+retriveAddress.getStreetName1().toUpperCase(),font4);
                Paragraph paraCity=new Paragraph(retriveAddress.getCity().toUpperCase()+","+retriveAddress.getCountry().toUpperCase(),font4);
                Paragraph paragraph1=new Paragraph(retrieveUser.getEmail().toLowerCase(),font3);
                Paragraph paraPeriod=new Paragraph("TIME PERIOD",font1);
                Paragraph paraDateFrom=new Paragraph(" FROM:- "+(Date)transactionHistory.getInitialDate(),font4);
                Paragraph paraDateTo=new Paragraph(" TO:- "+(Date)transactionHistory.getEndDate(),font4);

                paragraph.setAlignment(Element.ALIGN_RIGHT);
                paragraph.setIndentationRight(1);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);
                paragraph1.setIndentationRight(1);
                paraName.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setAlignment(Element.ALIGN_RIGHT);
                paraAddress.setIndentationLeft(1);
                paraCity.setAlignment(Element.ALIGN_RIGHT);
                paraCity.setIndentationLeft(1);
                paraPeriod.setAlignment(Element.ALIGN_RIGHT);
                paraPeriod.setIndentationRight(50);
                paraDateFrom.setAlignment(Element.ALIGN_RIGHT);
                paraDateFrom.setIndentationLeft(1);
                paraDateTo.setAlignment(Element.ALIGN_RIGHT);
                paraDateTo.setIndentationLeft(1);
                paraDateTo.setSpacingAfter(50);



                for (Transaction transaction: transactionBuy) {
                    BonusTransaction bonus = bonusTransactionService.retrieveBonusTransactionByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(CommisionType.BONUS.getCommisionType(bonus.getBonusType())));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }

                for (Transaction transaction: transactionSell) {
                    BonusTransaction bonus = bonusTransactionService.retrieveBonusTransactionByTransaction(transaction.getTransactionId());
                    table.addCell(String.valueOf(transaction.getAmount()));
                    table.addCell(String.valueOf(transaction.getTransactionDate()));
                    table.addCell(String.valueOf(CommisionType.BONUS.getCommisionType(bonus.getBonusType())));
                    table.addCell(String.valueOf(transaction.getTransactionId()));
                }          

                try {
                    Document document = new Document();
                    PdfWriter.getInstance(document, response.getOutputStream());
                    document.open();
                    //document.add(image1);
                    document.add(paragraph);
                    document.add(paraName);
                    document.add(paraAddress);
                    document.add(paraCity);
                    document.add(paragraph1);
                    document.add(paraPeriod);
                    document.add(paraDateFrom);
                    document.add(paraDateTo);
                    document.add(table);
                    document.close();
               }catch (IOException e) {
               }catch (DocumentException e) {
               }
            }
            return new ModelAndView("redirect:/TranactionHistory");
    }
    
    
}    
  
