/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Arvind
 */
public class ProfileUpgrade {
    
    @NotBlank(message = "Please Enter  Name")
    private String name;
    
    @NotBlank(message = "Please Enter  Email Id")
    private String email;
    
    @NotBlank(message = "Please Enter Mobile Number")
    private String contact;
    
    private Integer securityQuestionId;
    
    @NotBlank(message = "Please Enter Security Answer")
    private String securityAnswer;
    
    @NotNull(message = "Please Select Gender")
    private Integer gender;

    @NotNull(message = "Please Enter DOB")
    private Date dateOfBirth;
    
    private String image;

    private Integer userStatus;

    @Temporal(TemporalType.DATE)
    private Date registrationDate;

    @NotBlank(message = "Please Enter  Screen Name")
    private String screenName;

    @NotBlank(message = "Please Enter OfficeTelNo")
    private String officeTelNo;

    @NotBlank(message = "Please Enter HomeTelNo")
    private String homeTelNo;

    @NotBlank(message = "Please Enter FaxNumber")
     private String faxNo;

    @NotBlank(message = "Please Enter Passport Number")
    private String PassportNo;

    @NotBlank(message = "Please Select Country")
    private String residentCountry;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getOfficeTelNo() {
        return officeTelNo;
    }

    public void setOfficeTelNo(String officeTelNo) {
        this.officeTelNo = officeTelNo;
    }

    public String getHomeTelNo() {
        return homeTelNo;
    }

    public void setHomeTelNo(String homeTelNo) {
        this.homeTelNo = homeTelNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getPassportNo() {
        return PassportNo;
    }

    public void setPassportNo(String PassportNo) {
        this.PassportNo = PassportNo;
    }

    public String getResidentCountry() {
        return residentCountry;
    }

    public void setResidentCountry(String residentCountry) {
        this.residentCountry = residentCountry;
    }

    @Override
    public String toString() {
        return "ProfileUpgrade{" + "name=" + name + ", email=" + email + ", contact=" + contact + ", securityQuestionId=" + securityQuestionId + ", securityAnswer=" + securityAnswer + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", image=" + image + ", userStatus=" + userStatus + ", registrationDate=" + registrationDate + ", screenName=" + screenName + ", officeTelNo=" + officeTelNo + ", homeTelNo=" + homeTelNo + ", faxNo=" + faxNo + ", PassportNo=" + PassportNo + ", residentCountry=" + residentCountry + '}';
    }
    
    
}
