/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.web.controller.webdomain;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
public class ChangePassword {
    
    @NotBlank(message = "Please Enter Current password")
    private String currentPassword;
    
    @NotBlank(message = "Please Enter Password")
    private String password;
    
    @NotBlank(message = "Please Enter reTypePassword")
    private String  reTypePassword;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReTypePassword() {
        return reTypePassword;
    }

    public void setReTypePassword(String reTypePassword) {
        this.reTypePassword = reTypePassword;
    }

    @Override
    public String toString() {
        return "ChangePassword{" + "currentPassword=" + currentPassword + ", password=" + password + ", reTypePassword=" + reTypePassword + '}';
    }
    
    
    
    
}
