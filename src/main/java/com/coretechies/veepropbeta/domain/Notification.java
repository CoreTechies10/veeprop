/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


    /*CREATE  TABLE `veeprop_db`.`notification` (
  `notificationid` INT NOT NULL ,
  `identificationnumber` INT NOT NULL ,
  `notification` VARCHAR(255) NOT NULL ,
  `notificationlink` VARCHAR(255) NOT NULL ,
  `notificationstatus` VARCHAR(45) NOT NULL ,
  `notificationdate` DATETIME NOT NULL ,
  PRIMARY KEY (`notificationid`) ,
  INDEX `fk_notification_user_idx` (`identificationnumber` ASC) ,
  CONSTRAINT `fk_notification_user`
    FOREIGN KEY (`identificationnumber` )
    REFERENCES `veeprop_db`.`user` (`IdentificationNumber` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);*/

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "notification")
@Access(AccessType.FIELD)
public class Notification {
    
    @Id
    @GenericGenerator(name = "notification", strategy = "increment")
    @GeneratedValue(generator = "notification")
    private Integer notificationId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    private String notification;
    
    @Column
    private String notificationLink;
    
    @Column
    private Boolean notificationStatus;
    
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date notificationDate;

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotificationLink() {
        return notificationLink;
    }

    public void setNotificationLink(String notificationLink) {
        this.notificationLink = notificationLink;
    }

    public Boolean isNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(Boolean notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public Date getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    @Override
    public String toString() {
        return "Notification{" + "notificationId=" + notificationId + ", identificationNumber=" + identificationNumber + ", notification=" + notification + ", notificationLink=" + notificationLink + ", notificationStatus=" + notificationStatus + ", notificationDate=" + notificationDate + '}';
    }
}
