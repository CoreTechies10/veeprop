/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "profilechangerequest")
@Access(AccessType.FIELD)
public class ProfileChangeRequest {
    
    @Id
    @GenericGenerator(name = "profilechangerequest" , strategy = "increment")
    @GeneratedValue(generator = "profilechangerequest")
    private Integer requestId;
    
    @Column
    private Integer IdentificationNumber;

    @Column
    private String name;
    
    @Column
    private String email;
    
    @Column
    private String contact;
    
    @Column
    private Integer securityQuestionId;
    
    @Column
    private String securityAnswer;
    
    @Column
    private Integer gender;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    
    @Column
    private String image;
    
    @Column
    private String houseNumber;
    
    @Column
    private String streetName1;
    
    @Column
    private String streetName2;
    
    @Column
    private String city;

    @Column
    private String landmark;
    
    @Column
    private String userState;
    
    @Column
    private String country;
    
    @Column
    private String zipCode;
    
    @Column
    private Boolean requestStatus;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date requestDate;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date verifiedDate;
    
    @Column
    private Integer verifiedBy;
    
     @Column
    private String screenName;
    
    @Column
    private String officeTelNo;
    
    @Column
    private String homeTelNo;
    
    @Column
     private String faxNo;
    
    @Column 
    private String PassportNo;
    
    @Column
    private String residentCountry;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }
    
    public Integer getIdentificationNumber() {
        return IdentificationNumber;
    }

    public void setIdentificationNumber(Integer IdentificationNumber) {
        this.IdentificationNumber = IdentificationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    public Integer isGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName1() {
        return streetName1;
    }

    public void setStreetName1(String streetName1) {
        this.streetName1 = streetName1;
    }

    public String getStreetName2() {
        return streetName2;
    }

    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Boolean isRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Boolean requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getVerificationDate() {
        return verifiedDate;
    }

    public void setVerificationDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public Integer getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(Integer verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Date getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getOfficeTelNo() {
        return officeTelNo;
    }

    public void setOfficeTelNo(String officeTelNo) {
        this.officeTelNo = officeTelNo;
    }

    public String getHomeTelNo() {
        return homeTelNo;
    }

    public void setHomeTelNo(String homeTelNo) {
        this.homeTelNo = homeTelNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getPassportNo() {
        return PassportNo;
    }

    public void setPassportNo(String PassportNo) {
        this.PassportNo = PassportNo;
    }

    public String getResidentCountry() {
        return residentCountry;
    }

    public void setResidentCountry(String residentCountry) {
        this.residentCountry = residentCountry;
    }

    @Override
    public String toString() {
        return "ProfileChangeRequest{" + "requestId=" + requestId + ", IdentificationNumber=" + IdentificationNumber + ", name=" + name + ", email=" + email + ", contact=" + contact + ", securityQuestionId=" + securityQuestionId + ", securityAnswer=" + securityAnswer + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", image=" + image + ", houseNumber=" + houseNumber + ", streetName1=" + streetName1 + ", streetName2=" + streetName2 + ", city=" + city + ", landmark=" + landmark + ", userState=" + userState + ", country=" + country + ", zipCode=" + zipCode + ", requestStatus=" + requestStatus + ", requestDate=" + requestDate + ", verifiedDate=" + verifiedDate + ", verifiedBy=" + verifiedBy + ", screenName=" + screenName + ", officeTelNo=" + officeTelNo + ", homeTelNo=" + homeTelNo + ", faxNo=" + faxNo + ", PassportNo=" + PassportNo + ", residentCountry=" + residentCountry + '}';
    }

    
    
    
}
