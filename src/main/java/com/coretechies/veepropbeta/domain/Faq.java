/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "faq")
@Access(AccessType.FIELD)
public class Faq {
    
    @Id
    @GenericGenerator(name = "faq", strategy = "increment")
    @GeneratedValue(generator = "faq")
    private Integer faqId;
    
    @Column
    private Integer adminId;
    
    @Column 
    @NotBlank(message = "Please Enter Question")
    private String question;
    
    @Column
    @NotBlank(message = "Please Enter Answer")
    private String answer;

    public Integer getFaqId() {
        return faqId;
    }

    public void setFaqId(Integer faqId) {
        this.faqId = faqId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "faq{" + "faqId=" + faqId + ", adminId=" + adminId + ", question=" + question + ", answer=" + answer + '}';
    }
    
    
}
