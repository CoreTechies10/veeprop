/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="traderrelativedata")
@Access(AccessType.FIELD)
public class TraderRelativeData {
    
    @Id
    @GenericGenerator(name = "traderrelativedata", strategy = "increment")
    @GeneratedValue(generator = "traderrelativedata")
    private Integer TraderRelativeDataId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    private Integer bP;
    
    @Column
    private Integer pTP;
    
    @Column
    private Integer rP;
    
    @Column
    private Integer sp;

    public Integer getTraderRelativeDataId() {
        return TraderRelativeDataId;
    }

    public void setTraderRelativeDataId(Integer TraderRelativeDataId) {
        this.TraderRelativeDataId = TraderRelativeDataId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getbP() {
        return bP;
    }

    public void setbP(Integer bP) {
        this.bP = bP;
    }

    public Integer getpTP() {
        return pTP;
    }

    public void setpTP(Integer pTP) {
        this.pTP = pTP;
    }

    public Integer getrP() {
        return rP;
    }

    public void setrP(Integer rP) {
        this.rP = rP;
    }

    public Integer getSp() {
        return sp;
    }

    public void setSp(Integer sp) {
        this.sp = sp;
    }

    @Override
    public String toString() {
        return "traderrelativedata{" + "TraderRelativeDataId=" + TraderRelativeDataId + ", identificationNumber=" + identificationNumber + ", bP=" + bP + ", pTP=" + pTP + ", rP=" + rP + ", sp=" + sp + '}';
    }
    
    
    
    
    
}
