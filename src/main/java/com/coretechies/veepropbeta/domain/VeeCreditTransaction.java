/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="veecredittransaction")
@Access(AccessType.FIELD)
public class VeeCreditTransaction {
    
    @Id
    @GenericGenerator(name = "veecredittransaction", strategy = "increment")
    @GeneratedValue(generator = "veecredittransaction")
    private Integer VeeCreditTransactionId;
    
    @Column
    private Integer veeCreditTransactionToken;
            
    @Column
    private Double veeCredit;
            
    @Column
    private String remark;
            
    @Column 
    private Integer transactionStatus;
      
    @Column
    private Integer VeeCreditTransactionType;
    
    @Column
    private String fileName;
    
    @Column
    private String bankTransactionId;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBankTransactionId() {
        return bankTransactionId;
    }

    public void setBankTransactionId(String bankTransactionId) {
        this.bankTransactionId = bankTransactionId;
    }

    public Integer getVeeCreditTransactionId() {
        return VeeCreditTransactionId;
    }

    public void setVeeCreditTransactionId(Integer VeeCreditTransactionId) {
        this.VeeCreditTransactionId = VeeCreditTransactionId;
    }

    public Integer getVeeCreditTransactionToken() {
        return veeCreditTransactionToken;
    }

    public void setVeeCreditTransactionToken(Integer veeCreditTransactionToken) {
        this.veeCreditTransactionToken = veeCreditTransactionToken;
    }

    public Double getVeeCredit() {
        return veeCredit;
    }

    public void setVeeCredit(Double veeCredit) {
        this.veeCredit = veeCredit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Integer getVeeCreditTransactionType() {
        return VeeCreditTransactionType;
    }

    public void setVeeCreditTransactionType(Integer VeeCreditTransactionType) {
        this.VeeCreditTransactionType = VeeCreditTransactionType;
    }

    @Override
    public String toString() {
        return "VeeCreditTransaction{" + "VeeCreditTransactionId=" + VeeCreditTransactionId + ", veeCreditTransactionToken=" + veeCreditTransactionToken + ", veeCredit=" + veeCredit + ", remark=" + remark + ", transactionStatus=" + transactionStatus + ", VeeCreditTransactionType=" + VeeCreditTransactionType + ", fileName=" + fileName + ", bankTransactionId=" + bankTransactionId + '}';
    }
    
}
