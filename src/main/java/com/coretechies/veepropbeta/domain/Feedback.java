/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "feedback")
@Access(AccessType.FIELD)
public class Feedback {
    
    @Id
    @GenericGenerator(name = "feedback", strategy = "increment")
    @GeneratedValue(generator = "feedback")
    private Integer feedbackId;
    
    @Column 
    private Integer identificationNumber;
    
    @Column
    @NotNull(message = "please Select Feedback Type")
    private Integer feedbackType;
    
    @Column
    @NotBlank(message = "Please Enter Content")
    private String content;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(Integer feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "feedback{" + "feedbackId=" + feedbackId + ", identificationNumber=" + identificationNumber + ", feedbackType=" + feedbackType + ", content=" + content + '}';
    }
    
    
}
