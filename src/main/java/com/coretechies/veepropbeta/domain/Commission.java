/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "commission")
@Access(AccessType.FIELD)
public class Commission {
    
    @Id
    @GenericGenerator(name = "commision", strategy = "increment")
    @GeneratedValue(generator = "commision")
    private Integer commisionId;
    
    @Column
    private Integer userIn; //foreign key with user IN with user table
    
    @Column
    private Integer transactionId; // foriegn key with transactionId in transaction Table
    
    @Column
    private Integer commissionType;
    
    @Column 
    private Float amount;

    public Integer getCommisionId() {
        return commisionId;
    }

    public void setCommisionId(Integer commisionId) {
        this.commisionId = commisionId;
    }

    public Integer getUserIn() {
        return userIn;
    }

    public void setUserIn(Integer userIn) {
        this.userIn = userIn;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(Integer commissionType) {
        this.commissionType = commissionType;
    }

    public Float getAmmount() {
        return amount;
    }

    public void setAmmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "commission{" + "commisionId=" + commisionId + ", userIn=" + userIn + ", transactionId=" + transactionId + ", commissionType=" + commissionType + ", amount=" + amount + '}';
    }
    
}
