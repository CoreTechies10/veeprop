/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="transaction")
@Access(AccessType.FIELD)
public class Transaction {
    @Id
    @GenericGenerator(name = "transaction", strategy = "increment")
    @GeneratedValue(generator = "transaction")
    private Integer TransactionId;
    
    @Column
    private Integer TransactionType;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date TransactionDate;
    
    @Column
    @Temporal(TemporalType.TIME)
    private Date TransactionTime;
    
    @Column
    private Double amount;
    
    @Column
    private Integer sellerIN;
    
    @Column
    private Integer buyerIN;

    public Integer getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(Integer TransactionId) {
        this.TransactionId = TransactionId;
    }

    public Integer getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(Integer TransactionType) {
        this.TransactionType = TransactionType;
    }

    public Date getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(Date TransactionDate) {
        this.TransactionDate = TransactionDate;
    }

    public Date getTransactionTime() {
        return TransactionTime;
    }

    public void setTransactionTime(Date TransactionTime) {
        this.TransactionTime = TransactionTime;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getSellerIN() {
        return sellerIN;
    }

    public void setSellerIN(Integer sellerIN) {
        this.sellerIN = sellerIN;
    }

    public Integer getBuyerIN() {
        return buyerIN;
    }

    public void setBuyerIN(Integer buyerIN) {
        this.buyerIN = buyerIN;
    }

    @Override
    public String toString() {
        return "transaction{" + "TransactionId=" + TransactionId + ", TransactionType=" + TransactionType + ", TransactionDate=" + TransactionDate + ", TransactionTime=" + TransactionTime + ", amount=" + amount + ", sellerIN=" + sellerIN + ", buyerIN=" + buyerIN + '}';
    }
    
    
}
