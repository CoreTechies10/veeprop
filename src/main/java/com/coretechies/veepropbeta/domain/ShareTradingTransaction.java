/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="sharetradingtransaction")
@Access(AccessType.FIELD)
public class ShareTradingTransaction {
    
    @Id
    @GenericGenerator(name = "sharetradingtransaction", strategy = "increment")
    @GeneratedValue(generator = "sharetradingtransaction")
    private Integer shareTradingTransactionId;
    
    @Column
    private Integer shareTradingTransactionToken;
            
    @Column
    private Integer numberOfShare;
    
    @Column
    private float costPerShare;
    
    @Column
    private Integer transactionStatus;

    public Integer getShareTradingTransactionId() {
        return shareTradingTransactionId;
    }

    public void setShareTradingTransactionId(Integer shareTradingTransactionId) {
        this.shareTradingTransactionId = shareTradingTransactionId;
    }

    public Integer getShareTradingTransactionToken() {
        return shareTradingTransactionToken;
    }

    public void setShareTradingTransactionToken(Integer shareTradingTransactionToken) {
        this.shareTradingTransactionToken = shareTradingTransactionToken;
    }

    public Integer getNumberOfShare() {
        return numberOfShare;
    }

    public void setNumberOfShare(Integer numberOfShare) {
        this.numberOfShare = numberOfShare;
    }

    public float getCostPerShare() {
        return costPerShare;
    }

    public void setCostPerShare(float costPerShare) {
        this.costPerShare = costPerShare;
    }

    public Integer getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Override
    public String toString() {
        return "sharetradingtransaction{" + "shareTradingTransactionId=" + shareTradingTransactionId + ", shareTradingTransactionToken=" + shareTradingTransactionToken + ", numberOfShare=" + numberOfShare + ", costPerShare=" + costPerShare + ", transactionStatus=" + transactionStatus + '}';
    }
    
    
}
