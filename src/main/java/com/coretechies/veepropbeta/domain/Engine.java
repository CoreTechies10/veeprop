/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "engine")
@Access(AccessType.FIELD)
public class Engine {
    
    @Id
    @GenericGenerator(name = "engine",strategy = "increment")
    @GeneratedValue(generator = "engine")
    private Integer engineId;
    
    @Column
    private Integer generationNumber;
    
    @Column
    private Integer lotNumber;
    
    @Column
    private Integer shareAvailable;
    
    @Column
    private Integer shareSoldInGeneration;
    
    @Column
    private Integer ipo;

    public Integer getEngineId() {
        return engineId;
    }

    public void setEngineId(Integer engineId) {
        this.engineId = engineId;
    }

    public Integer getGenerationNumber() {
        return generationNumber;
    }

    public void setGenerationNumber(Integer generationNumber) {
        this.generationNumber = generationNumber;
    }

    public Integer getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(Integer lotNumber) {
        this.lotNumber = lotNumber;
    }

    public Integer getShareAvailable() {
        return shareAvailable;
    }

    public void setShareAvailable(Integer shareAvailable) {
        this.shareAvailable = shareAvailable;
    }

    public Integer getShareSoldInGeneration() {
        return shareSoldInGeneration;
    }

    public void setShareSoldInGeneration(Integer shareSoldInGeneration) {
        this.shareSoldInGeneration = shareSoldInGeneration;
    }

    public Integer getIpo() {
        return ipo;
    }

    public void setIpo(Integer ipo) {
        this.ipo = ipo;
    }

    @Override
    public String toString() {
        return "engine{" + "engineId=" + engineId + ", generationNumber=" + generationNumber + ", lotNumber=" + lotNumber + ", shareAvailable=" + shareAvailable + ", shareSoldInGeneration=" + shareSoldInGeneration + ", ipo=" + ipo + '}';
    }
    
    
}
