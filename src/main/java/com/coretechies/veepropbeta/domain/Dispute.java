/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name="dispute")
@Access(javax.persistence.AccessType.FIELD)
public class Dispute {
    
    @Id
    @GenericGenerator(name = "dispute", strategy = "increment")
    @GeneratedValue(generator = "dispute")
    private Integer disputeId;
    
    @Column
    private Integer transactionId;
    
    @Column
    private Integer veeCreditTransactionId;
    
    @Column
    private String reason;
    
    @Column
    private Integer sellerIN;
    
    @Column
    private Integer buyerIN;
    
    @Column
    private Double veeCreditAmount;
    
    @Column
    private String sellerName;
    
    @Column
    private String sellerAcountNumber;
    
    @Column 
    private String proofOfTransaction;
    
    @Column
    private String sellerContactNumber;
    
    @Column 
    private String sellerEmail;
    
    @Column
    private Integer moderatorIN;

    public Integer getModeratorIN() {
        return moderatorIN;
    }

    public void setModeratorIN(Integer moderatorIN) {
        this.moderatorIN = moderatorIN;
    }

    public Integer getDisputeId() {
        return disputeId;
    }

    public void setDisputeId(Integer disputeId) {
        this.disputeId = disputeId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getVeeCreditTransactionId() {
        return veeCreditTransactionId;
    }

    public void setVeeCreditTransactionId(Integer veeCreditTransactionId) {
        this.veeCreditTransactionId = veeCreditTransactionId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getSellerIN() {
        return sellerIN;
    }

    public void setSellerIN(Integer sellerIN) {
        this.sellerIN = sellerIN;
    }

    public Integer getBuyerIN() {
        return buyerIN;
    }

    public void setBuyerIN(Integer buyerIN) {
        this.buyerIN = buyerIN;
    }

    public Double getVeeCreditAmount() {
        return veeCreditAmount;
    }

    public void setVeeCreditAmount(Double veeCreditAmount) {
        this.veeCreditAmount = veeCreditAmount;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerAcountNumber() {
        return sellerAcountNumber;
    }

    public void setSellerAcountNumber(String sellerAcountNumber) {
        this.sellerAcountNumber = sellerAcountNumber;
    }

    public String getProofOfTransaction() {
        return proofOfTransaction;
    }

    public void setProofOfTransaction(String proofOfTransaction) {
        this.proofOfTransaction = proofOfTransaction;
    }

    public String getSellerContactNumber() {
        return sellerContactNumber;
    }

    public void setSellerContactNumber(String sellerContactNumber) {
        this.sellerContactNumber = sellerContactNumber;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    @Override
    public String toString() {
        return "Dispute{" + "disputeId=" + disputeId + ", transactionId=" + transactionId + ", veeCreditTransactionId=" + veeCreditTransactionId + ", reason=" + reason + ", sellerIN=" + sellerIN + ", buyerIN=" + buyerIN + ", veeCreditAmount=" + veeCreditAmount + ", sellerName=" + sellerName + ", sellerAcountNumber=" + sellerAcountNumber + ", proofOfTransaction=" + proofOfTransaction + ", sellerContactNumber=" + sellerContactNumber + ", sellerEmail=" + sellerEmail + '}';
    }
}
