/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import java.sql.Time;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "ipo")
@Access(AccessType.FIELD)
public class Ipo {
    
    @Id
    @GenericGenerator(name = "ipo", strategy = "increment")
    @GeneratedValue(generator = "ipo")
    private Integer ipoNumber;
    
    @Column
    private Integer numberOfShare;
    
    @Column
    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    
    @Column
    private Time releaseTime;

    public Integer getIpoNumber() {
        return ipoNumber;
    }

    public void setIpoNumber(Integer ipoNumber) {
        this.ipoNumber = ipoNumber;
    }

    public Integer getNumberOfShare() {
        return numberOfShare;
    }

    public void setNumberOfShare(Integer numberOfShare) {
        this.numberOfShare = numberOfShare;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Time getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(Time releaseTime) {
        this.releaseTime = releaseTime;
    }

    @Override
    public String toString() {
        return "ipo{" + "ipoNumber=" + ipoNumber + ", numberOfShare=" + numberOfShare + ", releaseDate=" + releaseDate + ", releaseTime=" + releaseTime + '}';
    }
    
    
}
