/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="userbankdetail")
public class UserBankDetail {
   
    @Id
    @GenericGenerator(name = "announcement", strategy = "increment")
    @GeneratedValue(generator = "announcement")
    private Integer bankDetailId;
    
    @Column
    private Integer identificationNumber;
    
    @Column
    private String bankName;
    
    @Column
    private String accountNumber;
     
    @Column
    private String iFSC;
      
    @Column
    private String accountType;
     
    @Column
    private String bankAddress;
      
   @Column
    private String branch;
       
   @Column
    private String swift;

    public Integer getBankDetailId() {
        return bankDetailId;
    }

    public void setBankDetailId(Integer bankDetailId) {
        this.bankDetailId = bankDetailId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getiFSC() {
        return iFSC;
    }

    public void setiFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    @Override
    public String toString() {
        return "userbankdetail{" + "bankDetailId=" + bankDetailId + ", identificationNumber=" + identificationNumber + ", bankName=" + bankName + ", accountNumber=" + accountNumber + ", iFSC=" + iFSC + ", accountType=" + accountType + ", bankAddress=" + bankAddress + ", branch=" + branch + ", swift=" + swift + '}';
    }
    
   
    
}
