/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "securityquestion")
@Access(AccessType.FIELD)
public class SecurityQuestion {
    
    @Id
    @GenericGenerator(name = "securityquestion", strategy = "increment")
    @GeneratedValue(generator = "securityquestion")
    private Integer securityQuestionId;
    
    @Column
    private String securityQuestion;

    public Integer getSecurityQuestionId() {
        return securityQuestionId;
    }

    public void setSecurityQuestionId(Integer securityQuestionId) {
        this.securityQuestionId = securityQuestionId;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    @Override
    public String toString() {
        return "SecurityQuestion{" + "securityQuestionId=" + securityQuestionId + ", securityQuestion=" + securityQuestion + '}';
    }


}
