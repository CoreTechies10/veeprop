/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Arvind
 */
@Entity
@Table(name="traderdynamicdata")
@Access(AccessType.FIELD)
public class TraderDynamicData {
    
    @Id
    @GenericGenerator(name = "traderdynamicdata", strategy = "increment")
    @GeneratedValue(generator = "traderdynamicdata")
    private Integer traderDynamicDataId;
    
    @Column
    private Integer identificationNumber;
    
     @Column
    private Integer moneyOnLeft;
     
      @Column
    private Integer moneyOnRight;
      
       @Column
    private Integer matchingBonusPaid;
       
       
        @Column
    private Double packageAmount;

    public Integer getTraderDynamicDataId() {
        return traderDynamicDataId;
    }

    public void setTraderDynamicDataId(Integer traderDynamicDataId) {
        this.traderDynamicDataId = traderDynamicDataId;
    }

    public Integer getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(Integer identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getMoneyOnLeft() {
        return moneyOnLeft;
    }

    public void setMoneyOnLeft(Integer moneyOnLeft) {
        this.moneyOnLeft = moneyOnLeft;
    }

    public Integer getMoneyOnRight() {
        return moneyOnRight;
    }

    public void setMoneyOnRight(Integer moneyOnRight) {
        this.moneyOnRight = moneyOnRight;
    }

    public Integer getMatchingBonusPaid() {
        return matchingBonusPaid;
    }

    public void setMatchingBonusPaid(Integer matchingBonusPaid) {
        this.matchingBonusPaid = matchingBonusPaid;
    }

    public Double getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(Double packageAmount) {
        this.packageAmount = packageAmount;
    }

    @Override
    public String toString() {
        return "traderdynamicdata{" + "traderDynamicDataId=" + traderDynamicDataId + ", identificationNumber=" + identificationNumber + ", moneyOnLeft=" + moneyOnLeft + ", moneyOnRight=" + moneyOnRight + ", matchingBonusPaid=" + matchingBonusPaid + ", packageAmount=" + packageAmount + '}';
    }
        
        
}
