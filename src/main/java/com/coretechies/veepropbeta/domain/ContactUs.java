/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.coretechies.veepropbeta.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Noppp
 */
@Entity
@Table(name = "contactus")
@Access(AccessType.FIELD)
public class ContactUs {
    
    @Id
    @GenericGenerator(name = "contactus", strategy = "increment")
    @GeneratedValue(generator = "contactus")
    private Integer contactId;
    
    @Column
    @NotBlank(message = "Please Enter a Name")
    private String name;
    
    @Column
    @Email(message ="Please Enter valid Email")
    @NotBlank(message = "Please Enter a Email")
    private String email;
    
    @Column
    @NotBlank(message = "Please Enter a Mobile")
    private String contactNumber;
    
    @Column
    @NotBlank(message = "Please Enter a Content")
    private String content;

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "contactUs{" + "contactId=" + contactId + ", name=" + name + ", email=" + email + ", contactNumber=" + contactNumber + ", content=" + content + '}';
    }

    
}
