<%-- 
    Document   : footernew
    Created on : Mar 10, 2014, 2:49:56 PM
    Author     : Arvind
--%>
<style>
    .d1{
        font-size: larger;
        text-decoration: none;
        color: #FFFFFF;
    }
    .d1:hover{
        text-decoration: none;
        color: lightseagreen;
    }
</style>
<div class="footerimage" style="margin-top:1%;">
   <div class="row-fluid">
       <div class="span2 offset1"> 
          <h4 style="color:#999; font-size:24px; margin-left: 4%;">Comapny</h4>
          <ul style="list-style-type: none;">
              <li><a href="#"  class="d1">About</a></li>
              <li><a href="#" class="d1">Policy</a></li>
              <li><a href="#" class="d1">Management</a></li>
              <li><a href="#" class="d1">Terms & Conditions</a></li>
             
              <li><a href="#" class="d1">Be a Partner</a></li>
              <li><a href="#" class="d1">Location</a></li>
              <li><a href="#" class="d1"> Contact</a></li>
          </ul>
         
          
       </div>
       
       <div class="span2">
           <h4 style="color:#999; font-size:24px; margin-left:14%;">Traders</h4>
           <ul style="list-style-type: none;">
              <li><a href="#" class="d1">Inquiry</a></li>
              <li><a href="#" class="d1">privacy Policy</a></li>
              <li><a href="#" class="d1">Legal</a></li>
              <li><a href="#" class="d1">Trade News</a></li>
              <li><a href="#" class="d1">Forex</a></li>
              <li><a href="#" class="d1">Stock Exchange</a></li>
              
          </ul>
       </div>
       <div class="span2">
           <h4 style="color:#999; font-size:24px; margin-left: 14%;">Partners</h4>
          <ul style="list-style-type: none;">
              <li><a href="#" class="d1">our Partners</a></li>
              <li><a href="#" class="d1">Exhibitors</a></li>
              <li><a href="#" class="d1">Be a Partner</a></li>
              <li><a href="#" class="d1">Trade Policy</a></li>
              
          </ul>
       </div>
       <div class="span2">
           <h4 style="color:#999; font-size:24px; margin-left: 16%;">Game</h4>
          <ul style="list-style-type: none;">
              <li><a href="#" class="d1">About Game</a></li>
              <li><a href="#" class="d1">Virtual Tour</a></li>
              <li><a href="#" class="d1">Game policy</a></li>
              <li><a href="#" class="d1">Terms & Conditions</a></li>
              <li><a href="#" class="d1">How it works</a></li>
              
          </ul>
       </div>
       <div class="span3"><h4 style="color:#999; font-size:24px;">Social Networking</h4>
           <ul style="list-style-type: none;">
                    <li><a href="javascript:void(0)" class="d1"><img src="img/example-slide-1sml.jpg" width="40" alt="some alt text" /><span>Facebook</span></a></li>
                <li><a href="javascript:void(0)" class="d1"><img src="img/example-slide-2sml.jpg" width="40" alt="some alt text"/><span>Twitter</span></a></li>
                <li><a href="javascript:void(0)" class="d1"><img src="img/example-slide-3sml.jpg" width="40" alt="some alt text"/><span>Linkedin</span></a></li>
            </ul>
       </div>
   </div>
    
</div>     
    
     <section id="copyright">
    <div class="wrapper">
        <div class="span2">
                <div class="social">
                <a href="javascript:void(0)"><img src="img/Social-Networks-Google-plus-icon.png" alt="google plus" width="25"/></a>
                <a href="javascript:void(0)"><img src="img/Logos-Tumblr-icon.png" alt="tumblr" width="25"/></a>
                <a href="javascript:void(0)"><img src="img/Logos-Youtube-icon.png" alt="youtube" width="25"/></a>
                <a href="javascript:void(0)"><img src="img/Social-Networks-Bebo-icon.png" alt="bebo" width="25"/></a>
                <a href="javascript:void(0)"><img src="img/Social-Networks-Xing-icon.png" alt="xing" width="25"/></a>
            </div>
        </div>
        <div  class="col-md-5">
                &copy; Copyright 2014 by <a href="#">Team</a>. All Rights Reserved. 
        </div>
    </div>
     </section>
