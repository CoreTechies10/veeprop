
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }
        
        .error{
            color: red;
            font-weight: bold;
        }


    </style>
</head>
<body>
    <input type="hidden" id="notification-form" /> 
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach> 
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
         <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                  <li><a href="/veepropbeta/home">Dashboard</a></li>
                 <li><a href="/veepropbeta/profile">Profile</a></li>
                 <li><a href="/veepropbeta/wallet">Wallet</a></li>
                 <li><a href="/veepropbeta/business">Biz Management</a></li>
                 <li><a href="/veepropbeta/feedback">Feedback</a></li>
                 <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
            <a href="/veepropbeta/wallet" class="list-group-item active" style="margin-top: 4%; margin-bottom: 3%;"> Bussiness Summary</a>
            
            <div class="col-md-5 offset"><b>Numbers of User in System :</b></div>
            <div  class="col-md-4 offset2"><p>${traderCount}</p></div>  
            <div class="col-md-4 offset"><b>Your Introducer is :</b></div>
            <div  class="col-md-4 offset3"><p>${referalParentName}</p></div>
            <div class="col-md-5 offset"><b>You are currently on Level :</b></div>
            <div  class="col-md-4 offset2">
            <div class="progress">
                <c:set var="levelRatio" value="${userLevel/8}" />
                <c:set var="levelpercentage" value="${levelRatio*100}" />
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:${levelpercentage}%">
                <span class="sr-only"></span>
                <c:if test="${userLevel==8}" >
                     <b>Premium level</b>
                </c:if>
                     <c:if test="${userLevel<8}" >
                         <b style="color: #000; padding: 0px;">Level ${userLevel}</b>
                     </c:if>
            </div>
            </div>
            </p></div>
            <div class="col-md-5 offset"><b>To Introduce a Trader:</b></div>
            <div  class="col-md-4 offset2"><p><a href="/veepropbeta/introduce">
                        <button type="button" class="btn btn-success">Click Here</button></a></p></div>
            <div class="col-md-5 offset"><b>To View your Child tree:</b></div>
            <div  class="col-md-4 offset2"><p><a href="/veepropbeta/introduce" >
                        <button type="button" class="btn btn-success">Click Here</button></a></p></div>
        </div>
           
            
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
