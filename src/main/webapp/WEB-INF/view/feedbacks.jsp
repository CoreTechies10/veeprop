
<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />    
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
                text-align: center;
            }
            table tr {
                border: 1px black solid;
                text-align: center;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="">Reports</a></li>
                      <li><a href="">Profile</a></li>
                      <li><a href="">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <div>
                    Positive feedback : ${positive}<br />
                    Negative feedback : ${negative}<br /><br />
                </div>
                <div >
                    <c:set var="total" value="${positive+negative}" />
                    <c:set var="percentage" value="${positive/total}" />
                    <c:set var="percentageIs" value="${percentage*100}" />
                    System is ${percentageIs}% good according to feedbacks <br /><br />
                </div>
                <table>
                    <tr>
                        <th style="width: 100px;">Type</th>
                        <th style="width: 300px;">Message</th>
                    </tr>
                    <c:if test="${empty feedbacks}">
                    <tr>
                        <td colspan="6">No Feedback Yet</td>
                    </tr>    
                    </c:if>
                    <c:forEach var="feedback" items="${feedbacks}">
                    <tr>
                        <td><c:if test="${feedback.feedbackType==1}" >Positive</c:if>
                        <c:if test="${feedback.feedbackType==2}" >Negative</c:if></td>
                        <td>${feedback.content}</td>
                    </tr>
                    </c:forEach>
                </table>
                
            </div>
        </div>
    </div>
</body>
</html>