
<!DOCTYPE HTML>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/veepropbeta/css/tree.css" />

<script type="text/javascript">
	function extend(id){
            var url = "/veepropbeta/retrieveChild/"+id+"/";
            $.get(url, function(data) {
                if(data){
                    var len = data.length;
                    if(len===4){
                        if(data[0]===null){
                            var html = '<li><a>+</a></li>'
                                                 +'<li><a id="'+data[2]+'" onclick="return extend(this.id);">'+data[3]+'</a><ul class="'+data[2]+'"></ul></li>';
                            $('.'+id).html(html);
                        }else{
                            var html = '<li><a id="'+data[0]+'" onclick="return extend(this.id);">'+data[1]+'</a><ul class="'+data[0]+'"></ul></li>'
                                                 +'<li><a id="'+data[2]+'" onclick="return extend(this.id);">'+data[3]+'</a><ul class="'+data[2]+'"></ul></li>';
                            $('.'+id).html(html);
                        }
                    }else if(len===2){
                            var html = '<li><a id="'+data[0]+'" onclick="return extend(this.id);">'+data[1]+'</a><ul class="'+data[0]+'"></ul></li>'
                                                 +'<li><a>+</a></li>';
                            $('.'+id).html(html);

                    }else{
                        var html = '<li><a>+</a></li>'
                        +'<li><a>+</a></ul></li>';
                        $('.'+id).html(html);
                    }
                }else{
                    var html = '<li><a>+</a></li>'
		 			 +'<li><a>+</a></ul></li>';
		 $('.'+id).html(html);
                }
            });
	}
</script>        
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li>Profile</li>
                        <li>Edit Profile</li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    Binary Tree of System
                </h3>

                <br class="clear"/>
                <div class="tree" style="  margin-top: 20px; ">
                <ul>
                    <li>
                            <a id="${userInfo.identificationNumber}" onclick="return extend(this.id);"> ${userInfo.name}</a>
                        <ul class="${userInfo.identificationNumber}"></ul>
                    </li>
                </ul>
                </div>
            </article>
        </section>
    </body>
</html>