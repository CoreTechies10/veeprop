
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }
        </style>
         <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>                
 <script>
        function confirmSell(){
            if(${userIGCD.IGC}<${seller.amount}){
                $("#fund").html("Insufficient IGC");
                return false;
            }
            var r = confirm("click Ok to Sell Share else press cancel");
            if(r){
                return true;
            }else{
                return false;
            }
        }

    </script> 
</head>

<body>
    <input type="hidden" id="notification-form" /> 
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>  
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
            <%@include  file="sellboard.jsp" %>
        <div class="span6">
                
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                    <li><a id="projects" class="notification-menu-item first-item list_inline">Notifications</a></li>
               </ul>
            <a href="/veepropbeta/wallet" class="list-group-item active" style="margin-top: 2%;">Seller Share Entry &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your IGC is :&nbsp;&nbsp;${userIGCD.IGC}</a>
            <form:form commandName="seller" action="/veepropbeta/shareSold" method="POST" onsubmit="return confirmSell();">
                <div style="margin-top:2%;">
                    </div>
               <c:if test="${!empty param['message']}">
                   
                            Unavialable :
                        
                       
                            ${param['message']}
                     
                    </c:if>
                 <div class="row">
                <div class="form-group">
                  <form:label path="name" class="col-sm-5 control-label" style="margin-left:3%;">Seller Name</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
                  ${seller.name}<form:hidden path="name"/>
              </div>
             </div>
                </div>
              <div class="row">
              <div class="form-group">
                  <form:label path="email" class="col-sm-5 control-label" style="margin-left:3%;">Seller Email</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
              <form:hidden path="email"/>${seller.email}
              </div>
             </div>
              </div>
              <div class="row">
              <div class="form-group">
                  <form:label path="contact" class="col-sm-5 control-label" style="margin-left:3%;">Seller Contact</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
             ${seller.contact}<form:hidden path="contact"/>
              </div>
             </div>
              </div>
              
              <div class="row">
              <div class="form-group">
                  <form:label path="numberOfShare" class="col-sm-5 control-label" style="margin-left:3%;">Share</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
                ${seller.numberOfShare}<form:hidden path="numberOfShare"/>
              </div>
             </div>
              </div>
              <div class="row">
              <div class="form-group">
                  <form:label path="sharePrice" class="col-sm-5 control-label" style="margin-left:3%;">Price</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
               ${seller.sharePrice}<form:hidden path="sharePrice"/>
              </div>
             </div>
              </div>
              <div class="row">
              <div class="form-group">
                  <form:label path="amount" class="col-sm-5 control-label" style="margin-left:3%;">IGC Required</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
               ${seller.amount}<form:hidden path="amount"/>
              </div>
             </div> 
              </div>
              
             <form:hidden path="shareTransactionId"/>
            <form:hidden path="identificationNumber"/>
             <div class="" style="margin-top: 4%; margin-left: 25%;">
             <form:button class="btn btn-primary" >Purchase</form:button>
             <p style="color:red">       
             <span id="fund"></span></p>     
            </form:form>
            
            
        </div>
        </div>
           
            
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
