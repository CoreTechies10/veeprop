
<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : aamir khan
--%>


<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }
        </style>
         <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
                <script src="/veepropbeta/js/modernizr.js"></script>
                <script src="/veepropbeta/js/respond.min.js"></script>
                <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
                <script src="/veepropbeta/js/lightbox.js"></script>
                <script src="/veepropbeta/js/prefixfree.min.js"></script>
                <script src="/veepropbeta/js/jquery.slides.min.js"></script>
                <script type="text/javascript">
                function alertMessage(){
               alert("We are in Generation 1, you can not sell share");
          }
      </script> 
         
<script type="text/javascript">
        function validateShare(){
            var shareSell = $("#sharehold").val();

            if(shareSell>${shareAvailable}){
                $("#fund").html("InSufficient Share");
                return false;
            }
            if(shareSell<=0){
                $("#fund").html("At Least Select 10 Share");
                return false;
            }
            return true;
        }
        
           
      </script> 
</head>

<body>
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
        <%@include  file="sellboard.jsp" %>
        <div class="span6">
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                    <li><a id="projects" class="notification-menu-item first-item list_inline">Notifications</a></li>
               </ul>
            <a href="#" class="list-group-item active" style="margin-top: 2%;">Sell Share</a>
            <div style="height:30px;"></div>
            
            <div class="form-group">
                <label for="inputEmail3" style="margin-left: 28%;"><button type="button" class="btn btn-large btn-primary disabled" disabled="disabled">Available share:&nbsp;&nbsp;&nbsp;&nbsp;<b>${shareAvailable}</b></button></label>
           </div>
            <div class="form-group">
                <label for="inputEmail3" style="margin-left: 28%;"><button type="button" class="btn btn-large btn-primary disabled" disabled="disabled">Current Price rate in Market: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>${currentPrice}</b></button></label>
             </div>
            <c:if test="${param['availableShare']!=null}">
                <ul><li class="list_inline_form">Available share:</li> <li class="list_inline_form_db"><b>${param['availableShare']}</b></li></ul>
            </c:if>
             
                <form:form  action="/veepropbeta/sellshare" method="POST" modelAttribute="share" onsubmit="return validateShare();" >
                 <div class="form-group" style="margin-left: 25%;">
                      <form:label for="inputEmail3" path="shareHold" class="col-sm-6 control-label">Enter Number Of Share:</form:label>
                   <div class="col-sm-10">
                       <form:input path="shareHold" class="form-control" id="sharehold" placeholder="Enter Share Number" pattern="[0-9]+"/>
                   </div>
                </div>
           
                  <div class="col-lg-5" style="margin-top: 5%; float: left">
                       <form:button  class="btn btn-success">Purchase Share</form:button>
                      <p style="margin-left: 270px; color: red;">
                    <span id="fund"></span>
                   </div>         
                </form:form>
                
                
                
                
          </div>
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
