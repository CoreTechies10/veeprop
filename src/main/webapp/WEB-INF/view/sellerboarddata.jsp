
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <div class="sell_board" style="margin-top: 5px;"><span class="sb">Share Seller's Board</span></div>
   <div style="height:300px;; width:auto; overflow-x: auto;  border: 1px #265D7C solid;">
    <c:if test="${empty sellersShare}">
        <b>No Share sell Available This Time</b>
    </c:if>
    <c:forEach var="sell" items="${sellersShare}" varStatus="status">
        <a href="/veepropbeta/seller/${sell.identificationNumber}/${sell.shareTransactionId}"  class="list-group-item">${sell.name} : ${sell.numberOfShare} share</a>
       
    </c:forEach>
   </div>

<div class="sell_board"><span class="sb">V-Credit Seller's Board</span></div>
<div style="height:300px; width:auto; overflow-y: auto;  border: 1px #265D7C solid;">
    

    <c:if test="${empty sellersVCredit}">
        <b> No vCredit sell Available This Time</b>
    </c:if>    
    <c:forEach var="sells" items="${sellersVCredit}" varStatus="status">
        <a href="/veepropbeta/seller/vCredit/${sells.identificationNumber}/${sells.shareTransactionId}"  class="list-group-item"> ${sells.name} : ${sells.vCredit} vCredit</a>
        
    </c:forEach>


</div>




                     
