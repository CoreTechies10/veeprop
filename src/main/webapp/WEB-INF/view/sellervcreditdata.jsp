
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
<script>
    function confirmSell(){
        if(${userIGCD.IGC}<${seller.amount}){
            $("#fund").html("Insufficient IGC");
            return false;
        }
        var r = confirm("click Ok to Sell Share else press cancel");
        if(r){
            return true;
        }else{
            return false;
        }
    }
</script>    
</head>

<body>
    <input type="hidden" id="notification-form" />   
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>      
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
            <%@include  file="sellboard.jsp" %>
        <div class="span6">
                
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                    <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
            <a href="/veepropbeta/wallet" class="list-group-item active" style="z-index: 1;">Seller bank and Share Amount Details</a>

               <div style="margin-top:5%;"></div>
                <center>
                   <div style="font-weight: bold; color: red;margin-bottom: 10px;">
                   Please Note down Seller Detail for future action if needed.
                   </div>
               </center>
               <div  class="col-md-5 offset"><label ><b>Lock This Transaction</b></label></div>
               <div class="col-md-4 offset2">
                <a href="/veepropbeta/lockTransaction/${seller.identificationNumber}/${seller.shareTransactionId}"><button class="btn btn-danger">Lock</button></a>
               </div>
            <form:form commandName="seller" action="/veepropbeta/vCreditPurchaseRequest" method="POST" enctype="multipart/form-data">

               <div class="col-md-4 offset"><form:label path="name" ><b>Seller Name</b></form:label></div>
               <div class="col-md-4 offset3">
               <form:hidden path="name"></form:hidden><p>${seller.name}</p>
               </div>
               
               <div class="col-md-4 offset">
               <form:label path="email">Seller Email</form:label>
               </div>
               <div class="col-md-4 offset3">
               <form:hidden path="email"/><p>${seller.email}</p>
              </div>
               
              <div class="col-md-4 offset">
              <form:label path="contact" >Seller Contact</form:label>
              </div> 
              <div class="col-md-4 offset3">
                  <p>${seller.contact}</p><form:hidden path="contact"/>
              </div>
          
              <div class="col-md-5 offset">
               <form:label path="contact">Seller's Bank Name</form:label>
              </div>
              <div class="col-md-4 offset2">
                <p>${sellerBankDetail.bankName}<p><form:hidden path="contact"/>
              </div>
            
              <div class="col-md-5 offset">
              <form:label path="contact">Seller's Account Number</form:label>
              </div>
              <div class="col-md-4 offset2">
              <p>${sellerBankDetail.accountNumber}</p><form:hidden path="contact"/>
              </div>
              
              <div class="col-md-5 offset">
              <form:label path="contact">Seller's Swift Code</form:label>
              </div>
              <div class="col-md-4 offset2">
              <p>${sellerBankDetail.swift}</p><form:hidden path="contact"/>
              </div>
              <div class="col-md-5 offset">
              <form:label path="contact">Seller's IFSC Code</form:label>
              </div>
              <div class="col-md-4 offset2">
              <p>${sellerBankDetail.iFSC}</p><form:hidden path="contact"/>
              </div>
              
               <div class="col-md-5 offset">
               <form:label path="contact">Seller's Bank Branch name</form:label>
               </div>
               
                <div class="col-md-4 offset2">
                <form:hidden path="contact"/> ${sellerBankDetail.branch}
               </div>
               
               <div class="row">
              <div class="form-group">
                  <form:label path="fileName" class="col-sm-5 control-label" style="margin-left:3%;"></form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
                
              </div>
             </div>
              </div>
                  <c:if test="${prop.equals('lock')}">
                  <div class="row">
              <div class="form-group">
                  <form:label path="bankTransactionId" class="col-sm-2 control-label" style="margin-left:0%; ">Bank Transaction Id</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
                  <form:input path="bankTransactionId" class="form-control" required="true" cssStyle="width : 200px;"/>
              </div>
             </div>
              </div>
               <div class="row">
              <div class="form-group">
                  <form:label path="fileName" class="col-sm-2 control-label" style="margin-left:0%;">Proof of Transaction</form:label>
                  <div class="col-sm-5" style="margin-left:10%;">
                <input type="file" value="file" name="file" required="true" style="margin-right: -60px;"/>
              </div>
             </div>
              </div>
                  <form:button class="btn btn-success" style="margin-left:30%;margin-top:20px;">Purchase</form:button>
                  </c:if>

              
               
              
            
             <form:hidden path="shareTransactionId"/>
             <form:hidden path="identificationNumber"/>
             
                
            </form:form>
            
            
        </div>
           
            
             <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
