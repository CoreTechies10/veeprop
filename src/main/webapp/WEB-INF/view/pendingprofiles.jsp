
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 5px;
            }
            table tr, table tr td {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li>Profile</li>
                        <li>Edit Profile</li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    Up-gradation Request for Profiles :
                </h3>
                <div >
                    <table style="width: 100%;">
                        <tr>
                            <th style="width: 10%;">Name</th>
                            <th style="width: 10%;">Contact</th>
                            <th style="width: 10%;">D.O.B.</th>
                            <th style="width: 5%;">House</th>
                            <th style="width: 15%;">Street 1</th>
                            <th style="width: 15%;">Street 2</th>
                            <th style="width: 5%;">City</th>
                            <th style="width: 10%;">State</th>
                            <th style="width: 10%;">Country</th>
                            <th style="width: 10%;">Zip code</th>
                            <th style="width: 10%;">Action</th>
                        </tr>
                        <c:if test="${empty changeList}">
                        <tr style="text-align: center; border: #000 solid 1px;">
                            <td colspan="11">No pending Request for you</td>
                        </tr>
                        </c:if>                         
                        <c:forEach var="update" items="${changeList}">
                        <tr>
                            <td>${update.name}</td>
                            <td>${update.contact}</td>
                            <td>${update.dateOfBirth}</td>
                            <td>${update.houseNumber}</td>
                            <td>${update.streetName1}</td>
                            <td>${update.streetName2}</td>
                            <td>${update.city}</td>
                            <td>${update.userState}</td>
                            <td>${update.country}</td>
                            <td>${update.zipCode}</td>
                            <td><a href="/veepropbeta/accept/profile/${update.identificationNumber}/" style="text-decoration: underline; color: blue;">acc</a>
                                <a href="/veepropbeta/reject/profile/${update.identificationNumber}/" style="text-decoration: underline; color: blue;"> rej</a>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                </div>
            </article>
        </section>
    </body>
</html>