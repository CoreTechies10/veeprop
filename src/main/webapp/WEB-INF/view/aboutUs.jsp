
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : CoreTechies
--%>
  <%@page contentType="text/html" pageEncoding="windows-1252"%> 
  <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>About Us</title>
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
     <script src="/veepropbeta/js/modernizr.js"></script>
     <script src="/veepropbeta/js/respond.min.js"></script>
     <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
     <script src="/veepropbeta/js/lightbox.js"></script>
     <script src="/veepropbeta/js/prefixfree.min.js"></script>
     <script src="/veepropbeta/js/jquery.slides.min.js"></script>
     <script src="/veepropbeta/js/jquery.validate.js" type="text/javascript"></script>
 </head>      
  <body>
      <%@include file="header.jsp" %>
      <%@include  file="Lotinformation.jsp" %>
       
    <div class="row">
        <div class="col-md-8">
        <a  class="list-group-item active"><b>About Us</b></a>
        <a class="list-group-item ">
          <p>
              <b>Company Introduction</b><br>
Veeprop is a conceptualised virtual property game which aims to teach the players concepts of investing by having their in game credit work for them in a risk free setting while simultaneously increasing their financial literacy and stressing the imperative nature of accountability. The online game is based on a simplified financial and economic simulation environment. This is neither an investment related company nor any brokerage firm. 

Players must establish that their participation in Veeprop game is lawful in their own jurisdiction. Player's account(s) should not be managed by third-party in any means. Players that do not agree with the game regulations are advised to stop playing immediately. We would like to take this opportunity to thank you for your support and understandings.

            
          </p>
        </a>
        </div>
        <div  class="col-lg-4">
         <a  class="list-group-item active"><b>Location & Contact</b></a> 
         <a class="list-group-item">
             <p>   Contact - phone & email for customer service<br>
         Location - TBA<br>
         Can you put in enquiry form here? Example.<br>
         Name: CORE TECHIES<br>
         E-mail:info@coretechies.org<br>
         Contact:9252233332<br>
         Message:Bikaner<br>
         SUBMIT :Bikaner<br>
             </p>
         </a>
        </div>

       
    </div>   
      <%@include file="footer.jsp" %>      
</body>
</html>