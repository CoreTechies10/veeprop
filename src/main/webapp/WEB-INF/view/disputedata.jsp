
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="/veepropbeta/disputecases">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                        <li><a href="/veepropbeta/moderator/profile/1">Edit Profile</a></li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">                
                <br class="clear"/>
                <div style="  margin-top: 20px; ">
                    <table style="float: left;">
                        <caption style="font-weight: bold;">Dispute Data Request</caption>
                        <tr>
                            <th>Seller ID</th>
                            <td>${dispute.sellerIN}</td>
                        </tr>
                        <tr>
                            <th>Seller Name</th>
                            <td>${dispute.sellerName}</td>
                        </tr>
                        <tr>
                            <th>Buyer ID</th>
                            <td>${dispute.buyerIN}</td>
                        </tr>
                        <tr>
                            <th>Vee Credit</th>
                            <td>${dispute.veeCreditAmount}</td>
                        </tr>
                        <tr>
                            <th>Account Number</th>
                            <td>${dispute.sellerAcountNumber}</td>
                        </tr>
                        <tr>
                            <th>Seller Email</th>
                            <td>${dispute.sellerEmail}</td>
                        </tr>
                        <tr>
                            <th>Seller Contact</th>
                            <td>${dispute.sellerContactNumber}</td>
                        </tr>
                        <tr>
                            <th>Proof of Transaction</th>
                            <td><a href="/veepropbeta/${dispute.proofOfTransaction}" target="_blank" style="color:blue;">check</a></td>
                        </tr>
                        <tr>
                            <th>Reason for Dispute</th>
                            <td style="max-width: 200px;">${dispute.reason}</td>
                        </tr>
                    </table>
                    <table style="float: left; margin-left: 10px;">
                        <caption style="font-weight: bold;">Data According to System</caption>
                        <tr>
                            <th>Seller ID</th>
                            <td>${transaction.sellerIN}</td>
                        </tr>
                        <tr>
                            <th>Seller Name</th>
                            <td>${seller.name}</td>
                        </tr>
                        <tr>
                            <th>Buyer ID</th>
                            <td>${transaction.buyerIN}</td>
                        </tr>
                        <tr>
                            <th>Vee Credit</th>
                            <td>${transaction.amount}</td>
                        </tr>
                        <tr>
                            <th>Account Number</th>
                            <td>${sellerBank.accountNumber}</td>
                        </tr>
                        <tr>
                            <th>Seller Email</th>
                            <td>${seller.email}</td>
                        </tr>
                        <tr>
                            <th>Seller Contact</th>
                            <td>${seller.contact}</td>
                        </tr>
                        <tr>
                            <th>Action</th>
                            <td style="color: blue; text-decoration: underline;">
                                <a href="/veepropbeta/dispute/decline/${dispute.veeCreditTransactionId}">Decline</a>
                                <a href="/veepropbeta/dispute/accept/${dispute.veeCreditTransactionId}" style="padding-left: 20px;">Accept</a>
                            </td>
                    </table>
                            <br />
                    <p style="color: red;float: left; margin-top: 20px; ">Decline In Case dispute data request entry do
                    not match with data in system or data is insufficient according to you.</p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    <p style="color: red;float: left; margin-top: 20px; ">Accept in Case dispute data request entry
                         match with data in system and according to you data is sufficient & correct.</p>
                </div>
            </article>
        </section>
    </body>
</html>