<div align="center">
    <div align="center">
        Bank Name : ${userBank.bankName}
    </div>
    <div align="center">
        Account Number: ${userBank.accountNumber}
    </div>
    <div align="center">
        IFSC code: ${userBank.iFSC}
    </div>
    <div align="center">
        Bank Address: ${userBank.bankAddress}
    </div>
    <div align="center">
        Branch : ${userBank.branch}
    </div>
    <div align="center">
        Swift : ${userBank.swift}
    </div>
    
</div>