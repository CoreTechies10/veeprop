
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : CoreTechies
--%>
  <%@page contentType="text/html" pageEncoding="windows-1252"%> 
  <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>About Us</title>
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    
 </head>      
   <body>
      <%@include file="header.jsp" %>
      <%@include file="Lotinformation.jsp" %>
   
     <div class="row">
       <div class="col-md-8">
       <a  class="list-group-item active"><b>To add into Terms and Conditions</b></a>
       <a class="list-group-item ">
           <b>NO WARRANTY</b>
           <p>The Company will endeavour to provide the online game using its reasonable skill and care. The Company makes no further warranty or representation, whether express or implied, in relation to the online entertainment service. Any implied warranties or conditions relating to satisfactory quality, fitness for purpose, completeness or accuracy are hereby excluded.
The Company makes no warranty that the online game service will meet the Players's requirements or will be uninterrupted, timely, secure or error-free, that defects will be corrected, or that the service or the server that makes it available are free of viruses or bugs, nor does the Company make any warranty as to the full functionality, accuracy, reliability of the materials supplied by the Company or results of the online game service or the accuracy of any information obtained by the Player through the online game service.
The Player should be aware that the quality of the internet connection to the online game service and WAP internet connection varies from customer to customer and may not be absolutely stable. The Company is not responsible for any misplaced actions. All actions taken will be based on the records in the Company's system.
The Company carries out regular maintenance to the online game service to ensure, as much as possible, that an optimum game experience is provided to the Player. During such maintenance sessions, the Player acknowledges and agrees that access to the online game service may be limited to allow for such necessary repairs, maintenance or the introduction of new facilities or services. The Company will make reasonable efforts to notify Players of such downtime in advance of it occurring (whether via an advance schedule of downtime or period notifications on the online game service) and resume the service as soon as it reasonably can.
</p>
<b>LIMITATION OF LIABILITY</b>
<p>The Player agrees that their use of the online game service is at their sole risk.
The Company will not be liable in contract, tort, negligence, or otherwise, for any loss or damage whatsoever arising from or in any way connected with the Player's use of the online game service, whether direct or indirect, including, without limitation, damage for loss of business, loss of profits, business interruption, loss of business information, loss arising from downtime or any other pecuniary or consequential loss (even where the Company has been notified by the Player of the possibility of such loss or damage). Further, liability for such damage will be excluded, even if the exclusive remedies provided for in these terms and conditions fail for their essential purpose.
The Company will not be liable or responsible to the Player for any loss or damage incurred or suffered by the Player as a result of any suspension or stoppage (whether temporary or permanent) of the online game service arising from any governmental order or change in policy by the regulator.
The Company will not be responsible or liable to the Player for any loss of content or material uploaded or transmitted through the online game service and the Player confirms that the Company will not be liable to the Player or any third party for any modification to, suspension of or discontinuance of the online game service.
</p>
<b>INDEMNITY</b>
<p>The Player agrees fully to indemnify, defend and hold the Company, and its officers, directors, employees, agents and suppliers, harmless immediately on demand, from and against all claims, liabilities, damages, losses, costs and expenses, including legal fees, arising out of any breach of these terms and conditions by the Player or any other liabilities arising out of the Player's use of the online game service or use by any other person accessing the online game service using the Player's Account details.
The Player agrees fully to indemnify, defend and hold the Company, and its officers, directors, employees, agents and suppliers, harmless immediately on demand, from and against all claims, liabilities, damages, losses, costs and expenses, including legal fees, arising out of any breach of warranties or representations including, but not limited to, the representations that the Player is not a citizen or a resident of a country which prohibits such gaming activities nor will they access the online game service from a jurisdiction where such betting activity is prohibited by law.
The Player will indemnify the Company for all losses and damages suffered by the Company as a result of wrongdoings and/or fraud committed by the Player or group of Players acting in concert or as a syndicate. "Wrongdoings and/or fraud" will include but not be limited to attempts to beat the online game rules and T&C ,hacking, use of artificial intelligence or bots, providing false personal information, and/or any actions and/or omissions which the Company reasonably deems to be a fraud and/or wrongdoings.
</p>
<b>SETTLEMENT OF DISPUTES</b>
<p>The Player understands and agrees that (without prejudice to their other rights and remedies including the referral to an agreed dispute resolution process) the Company's records and subsequent management decision will be the final authority in determining the terms of the Players's participation in the online game service, the activity resulting therefrom and the circumstances in which they occurred.</p>
       </a>
      </div>
      </div>   
      <%@include file="footernew.jsp" %>      
</body>
</html>