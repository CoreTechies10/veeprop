
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : Arvind
--%>
 <%@page contentType="text/html" pageEncoding="windows-1252"%> 
 <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>Contact Us</title>
   <style>
   label.valid {
   width: 24px;
   height:10px;
   background: url() center center no-repeat;
   display: inline-block;
   text-indent: -9999px;}
   label.error {
   font-weight:bolder;
   color: red;
   padding: 5px 5px;
   margin-top: 3px;}
   
   .error{
       color: red;
       margin-left:-70px;
   }
  </style>
  
  
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
     <script src="/veepropbeta/js/modernizr.js"></script>
     <script src="/veepropbeta/js/respond.min.js"></script>
     <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
     <script src="/veepropbeta/js/lightbox.js"></script>
     <script src="/veepropbeta/js/prefixfree.min.js"></script>
     <script src="/veepropbeta/js/jquery.slides.min.js"></script>
     <script src="/veepropbeta/js/jquery.validate.js" type="text/javascript"></script>
           
     <script>
        $(document).ready(function(){
        $('#contact-form').validate( {
        rules: {
        name: {
        minlength: 5,
        required: true
        },
        email: {
        minlength:5,
        required: true
        },
        contactNumber:{
        minlength: 10,
        required: true
        },
        content: {
        minlength:10,
        required: true
        }
      },
        highlight: function(element) {
        $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function(element) {
        element
        .text('OK!').addClass('valid')
        .closest('.control-group').removeClass('error').addClass('success');
        }
        });
        }); 
    </script>
    <script type="text/javascript"> 
        function showHidePreloader(show){ 
            if(show) document.getElementById('preloader').style.display='block'; 
            else document.getElementById('preloader').style.display='none';
        } 
    </script> 
    <style> 
        #preloader{ 
            display :none;
            position: fixed;
            left: 0px;
            top: 0px;
            width:100%;
            height:100%;
            z-index: 1000;
            background:url(/veepropbeta/img/background.png);
        } 
        #imgdiv{
            position: fixed;
            left: 0px;
            top: 0px;
            width:50px;
            height:50px;
            margin: 300px 0 0 670px;
            z-index: 1001;
            background-repeat: no-repeat;
            margin: 300px 0 0 600px;
        }
    </style> 
 </head>
  
 
<body>
        <div id="preloader">
           <div id="imgdiv">
               <img src="/veepropbeta/img/loading.gif" height="50" width="50"/>
           </div>
       </div>
    <%@include file="header.jsp" %>
    <%@include file="Lotinformation.jsp" %>
      
    <div class="row">
        <center>
        <div class="col-md-4">
            <h2>Address</h2>
            <p align="right">Core Techies<br />
                Office No- 103,1st Floor,<br />
                E 9 &amp; 10,Kanta Khaturia Colony,<br />
                Bikaner-334001,Rajasthan,India<br />
                Email- info@coretechies.org<br />
                Contact Number- +91-151-2233332 
            </p>
        </div>
        <div class="col-md-4">
            <h2>Direction</h2>
           <!-- <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Core+Techies,+Bikaner,+Rajasthan&amp;aq=0&amp;oq=core+techie&amp;sll=28.013735,73.357247&amp;sspn=0.005465,0.010568&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=28.013934,73.35743&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Core+Techies,+Bikaner,+Rajasthan&amp;aq=0&amp;oq=core+techie&amp;sll=28.013735,73.357247&amp;sspn=0.005465,0.010568&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=28.013934,73.35743&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
        --></div>
        <div class="col-md-4"><h2>Contact  Here</h2>
            <form:form action="/veepropbeta/contact" class="form-horizontal" method="POST" modelAttribute="contact" onsubmit="showHidePreloader(true);">  
                <div class="form-group">
                    <form:label for="inputEmail3" path="name" class="col-sm-2 control-label">Name</form:label>
                    <div class="col-sm-10">
                        <form:input path="name" class="form-control" id="name" placeholder="Name"/>
                        <form:errors  path="name" element="span" cssClass="error" />
                    </div>
                </div>

                <div class="form-group">
                    <form:label for="inputEmail3" path="email" class="col-sm-2 control-label">Email</form:label>
                    <div class="col-sm-10">
                        <form:input path="email" class="form-control" id="email" placeholder="Email"/>
                        <form:errors  path="email" element="span" cssClass="error" />
                    </div>
                </div>
                    
                <div class="form-group">
                    <form:label for="inputEmail3" path="contactNumber" class="col-sm-2 control-label">Mobile</form:label>
                    <div class="col-sm-10">
                        <form:input path="contactNumber" class="form-control" id="contactNumber" placeholder="Mobile"/>
                        <form:errors  path="contactNumber" element="span" cssClass="error" />
                    </div>
                </div>
                    
                <div class="form-group">
                    <form:label for="inputEmail3" path="content" class="col-sm-2 control-label">Message</form:label>
                    <div class="col-sm-10">
                        <form:input path="content" class="form-control" id="content" placeholder="Message"/>
                        <form:errors  path="content" element="span" cssClass="error" />
                    </div>
                </div>
                   
                <div class="form-group"  margin-bottom: 4%;">
                    <button type="submit" class="btn btn-primary col-sm-3 offset1">Submit</button>
                    <div class="col-sm-3 ">
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>
                </div>
            </form:form>
        </div>
            </center>
               
    </div>
    <%@include file="footer.jsp" %>      
</body>
</html>