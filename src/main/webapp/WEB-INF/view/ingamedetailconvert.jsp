
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
    <script type="text/javascript">
        function validateVCredit(val){
        var check = ${vCreditMax};
        var x=document.getElementById("VCredit").selectedIndex;
            var y = document.getElementById("VCredit").options;
            var vCreit= (y[x].text);
        if(vCreit <= check){
            var x=document.getElementById("VCredit").selectedIndex;
            var y = document.getElementById("VCredit").options;
            var vCreit= (y[x].text);
            
            $("#vcredit").val(vCreit);
            return true;
        }
          else
          {
            alert("Your have "+${vCreditMax}+" V-Credit only");
            $("#VCredit").val(0);
            return  false;
           }
    }
    </script> 

</head>
<body>
   <input type="hidden" id="notification-form" />   
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
         <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                  <li><a href="/veepropbeta/home">Dashboard</a></li>
                 <li><a href="/veepropbeta/profile">Profile</a></li>
                 <li><a href="/veepropbeta/wallet">Wallet</a></li>
                 <li><a href="/veepropbeta/business">Biz Management</a></li>
                 <li><a href="/veepropbeta/feedback">Feedback</a></li>
      <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
            <a href="/veepropbeta/wallet" class="list-group-item active" style="margin-top: 2%;">Get IGC from V-Credit</a>
            <div style="height:30px;"></div>
            <form:form action="/veepropbeta/ingamedetailconvert" method="post" modelAttribute="convert">
            <div class="form-group">
                <form:label for="inputEmail3" path="IGC" style="margin-left:25%;"><button type="button" class="btn btn-large btn-primary disabled" disabled="disabled">Your V-Credit Amount:&nbsp;&nbsp; ${vCreditMax}</button></form:label>
                     <div class="col-sm-8 offset2" style="margin-top: 5%;">
                       <form:select path="IGC"  onchange="validateVCredit(this.value)" id="VCredit" class="form-control">
                             <form:option value="0">----Convert IGC-----</form:option>
                             <form:option value="50">100</form:option> 
                             <form:option value="105">200</form:option> 
                             <form:option value="270">500</form:option> 
                             <form:option value="560">1000</form:option> 
                             <form:option value="1160">2000</form:option>  
                             <form:option value="3000">5000</form:option> 
                             <form:option value="6400">10000</form:option> 
                             <form:option value="7000">100000</form:option> 
                       </form:select>
                             
                   </div>
                 <form:hidden path="vCredit" id="vcredit" ></form:hidden>  
                 <div class="col-sm-3 offset2" style="margin-top: 5%;">
                 <form:button  class="btn btn-success">Convert IGC</form:button>
                 </div>
                  
                </div>
            </form:form>
          </div>
             <div class="span3" style="float: right; margin-right:-1.7%;">
                <div class="sell_board"><span class="sb">Admin announcment Board</span></div>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
                  <a href="#" class="list-group-item">Morbi leo risus</a>
                  <a href="#" class="list-group-item">Porta ac consectetur ac</a>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
                  <a href="#" class="list-group-item">Morbi leo risus</a>
                  <a href="#" class="list-group-item">Porta ac consectetur ac</a>
                  <a href="#" class="list-group-item">Cras justo odio </a>
                  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
            </div>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>













































































































