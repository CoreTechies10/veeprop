
<!DOCTYPE HTML>
  <html>
    <head>
      <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});
    </script>
   <script type="text/javascript">
   function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
          ['Days', 'Lot'],
         <c:forEach var="g" items="${graph}">
            

          ['${g.lotNumber}',  ${g.days}],
                  
                    </c:forEach>
        ]);
      
        var options = {
          title : 'Lot Entries and Respective Time to Complete',
          vAxis: {title: "Number of Days"},
          hAxis: {title: "Lot Number"},
          seriesType: "bars",
          series: {5: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      google.setOnLoadCallback(drawVisualization);
    </script>
    </head>
<body>
    <input type="hidden" id="notification-form" /> 
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach>  
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
            <%@include  file="sellboard.jsp" %>
        <div class="span6">
                
                <ul class="nav nav-pills">
                    <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbeta/wallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                    <li><a id="projects" class="notification-menu-item first-item list_inline">Notifications</a></li>
               </ul>
          
             <c:if test="${empty image}">
                 <img src="../veepropbeta/img/defaultperson.jpg" width="150" height="150" style="margin-left: 2%; margin-top: 3%; float: left; border:5px #666 solid ">
            </c:if>    
              <c:if test="${!empty image}">
                 <img src="/veepropbeta/img/${image}" width="150" height="150" style="margin-left: 2%; margin-top: 3%; float: left; border:5px #666 solid ">
             </c:if>
            
            <div class="col-md-5 offset1">
               <div id="chart_div" style="width: 400px; height: 300px;"></div>
            </div> 
                 
             
            <div class="col-sm-3 offset" style="margin-top: 3%;">
                <a href="/veepropbeta/vcredit/buyers"><button class="btn btn-primary">Your Vee Credit Buyer</button></a>
            </div>
             <div class="col-sm-3 offset1" style="margin-top: 3%;">
                <a href="/veepropbeta/retrieveLockedTransaction"><button class="btn btn-primary">Your Locked transaction</button></a>
                ${param['note']}
            </div>  
            <div class="col-sm-3 offset1"style="margin-top: 3%;">
                 <a href="/veepropbeta/changepassword"><button class="btn btn-primary">Change password</button></a>
            </div>
            </div>
       
            <%@include  file="noticeboard.jsp" %>
            
            
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
</body>
</html>
