
<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />  
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
                text-align: center;
            }
            table tr {
                border: 1px black solid;
                text-align: center;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
        <script type="text/javascript">
            function showAnnouncement(){
                $("#announcementDiv").show();
                $("#announcementTable").hide();
            }
            function cancelAnnouncement(){
                $("#announcementDiv").hide();
                $("#announcementTable").show();
            }
            $(document).ready(function (e){
                $("#announcementDiv").hide();
            });
        </script>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <h5>Announcements </h5><h4><a onclick="return showAnnouncement();" style="cursor: pointer;">Add New Announcement</a></h4>
                <hr />
                <center>
                    
                    <form:form commandName="announcement" action="/veepropbeta/announcements" method="POST" >
                        <div id="announcementDiv">
                            <h3>New Announcement</h3>
                        <div>
                            <form:label path="heading" cssStyle="margin-bottom: 50px;vertical-align: middle;width: 200px;">Heading</form:label>
                            <form:textarea path="heading" />
                            <form:errors path="heading" cssClass="error" element="span"/>
                        </div>
                        <div>
                            <form:label path="content" cssStyle="margin-bottom: 50px;vertical-align: middle;width: 200px;">Content</form:label>
                            <form:textarea path="content" />
                            <form:errors path="content" cssClass="error" element="span"/>
                        </div>
                        <div>
                            <form:label path="expiryDate" cssStyle="width: 200px;">Expiry Date</form:label>
                            <form:input path="expiryDate" cssStyle="width: 180px;"/>
                            <form:errors path="expiryDate" cssClass="error" element="span"/>
                        </div>
                        <div>
                            <a class="btn btn-primary" style="cursor: pointer;margin: 20px;" onclick="return cancelAnnouncement();">Cancel</a>
                            <button class="btn btn-primary">Announce</button>
                        </div>
                        </div>
                    </form:form>
                    <br/>
                <table id="announcementTable">
                    <tr>
                        <th style="width: 200px;">Title</th>
                        <th style="width: 300px;">Content</th>
                        <th style="width: 150px;">Expiry Date</th>
                    </tr>
                    <c:if test="${empty announcements}">
                    <tr>
                        <td colspan="3">No Announcements currently active</td>
                    </tr>    
                    </c:if>
                    <c:forEach var="announcements" items="${announcements}">
                    <tr>
                        <td>${announcements.heading}</td>
                        <td>${announcements.content}</td>
                        <td>${announcements.expiryDate}</td>
                    </tr>
                    </c:forEach>
                </table>
                </center>
                
            </div>
        </div>
    </div>
</body>
</html>