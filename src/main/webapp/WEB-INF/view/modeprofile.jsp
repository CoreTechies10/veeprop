
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <style>
            <c:if test="${mode==1}">
            table tr th, table tbody tr td {
                padding: 0 20px;
            }
            table {
                text-align: center;
            }
            table tr th{
                font-weight: bold;
            }
            input{
                border: 1px black solid;
                width: 180px;
                height: 18px;
                margin: 3px;
                font-size:14px;
            } 
            select{
                border: 1px black solid;
                height: 30px;
                width : 200px;
            }
            </c:if>
            <c:if test="${mode!=1}">
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
            table tr th{
                font-weight: bold;
            }
            input{
                border: 1px black solid;
                width: 180px;
                height: 18px;
                margin: 3px;
                font-size:14px;
            }                
            </c:if>

        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                        <li><a href="/veepropbeta/moderator/profile/1">Edit Profile</a></li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                     Profile
                </h3>
                <br class="clear"/>
                <div style="  margin-top: 20px;  ">
                    <c:if test="${mode!=1}">
                    <table style="float: left; margin-right: 10px;">
                        <tr>
                            <th style="width: 120px;">Name</th>
                            <td style="width: 200px;">${moderator.name}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>${moderator.email}</td>
                        </tr>
                        <tr>
                            <th>Contact</th>
                            <td>${moderator.contact}</td>
                        </tr>
                        <tr>
                            <th>Gender</th>
                            <td><c:if test="${moderator.gender==0}">Male</c:if>
                                <c:if test="${moderator.gender==1}">Female</c:if>
                            </td>
                        </tr>
                        <tr>
                            <th>Date of Birth</th>
                            <td>${moderator.dateOfBirth}</td>
                        </tr>
                        <tr>
                            <th>Security Question</th>
                            <td>${question}</td>
                        </tr>
                        <tr>
                            <th>Answer</th>
                            <td>${moderator.securityAnswer}</td>
                        </tr>
                    </table>   
                    <table style="float: left;">
                        <tr>
                            <th style="width: 120px;">House No</th>
                            <td style="width: 200px;">${moderator.houseNumber}</td>
                        </tr>
                        <tr>
                            <th>Street Name 1</th>
                            <td>${moderator.streetName1}</td>
                        </tr>
                        <tr>
                            <th>Street Name 2</th>
                            <td>${moderator.streetName1}</td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td>${moderator.city}</td>
                        </tr>
                        <tr>
                            <th>Landmark</th>
                            <td>${moderator.landmark}</td>
                        </tr>
                        <tr>
                            <th>State</th>
                            <td>${moderator.userstate}</td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td>${moderator.country}</td>
                        </tr>
                        <tr>
                            <th>Zip Code</th>
                            <td>${moderator.zipcode}</td>
                        </tr>
                    </table>
                    </c:if>
                    <c:if test="${mode==1}">
                        <form:form action="/veepropbeta/moderator/profile" commandName="moderator" method="POST">
                            <table style="float: left; margin-right: 10px;">
                                <tr>
                                    <th style="width: 150px;">Name</th>
                                    <td style="width: 200px;"><form:input path="name"/></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><form:input path="email"/></td>
                                </tr>
                                <tr>
                                    <th>Contact</th>
                                    <td><form:input path="contact"/></td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td style="text-align: left;">
                                        <form:radiobutton path="gender" value="0" cssStyle="width: 3px;" id="0"/> <form:label path="gender" for="0">Male</form:label>
                                        <form:radiobutton path="gender" value="1" cssStyle="width: 3px;" id="1"/><form:label path="gender" for="1">Female</form:label>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Date of Birth</th>
                                    <td><form:input path="dateOfBirth"/></td>
                                </tr>
                                <tr>
                                    <th>Security Question</th>
                                    <td>
                                        <form:select path="securityQuestionId">
                                            <c:forEach var="item" items="${questions}">
                                                <form:option value="${item.securityQuestionId}">${item.securityQuestion}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Answer</th>
                                    <td><form:input path="securityAnswer"/></td>
                                </tr>
                            </table>   
                            <table style="float: left;">
                                <tr>
                                    <th style="width: 120px;">House No</th>
                                    <td style="width: 200px;"><form:input path="houseNumber"/></td>
                                </tr>
                                <tr>
                                    <th>Street Name 1</th>
                                    <td><form:input path="streetName1"/></td>
                                </tr>
                                <tr>
                                    <th>Street Name 2</th>
                                    <td><form:input path="streetName2"/></td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td><form:input path="city"/></td>
                                </tr>
                                <tr>
                                    <th>Landmark</th>
                                    <td><form:input path="landmark"/></td>
                                </tr>
                                <tr>
                                    <th>State</th>
                                    <td><form:input path="userstate"/></td>
                                </tr>
                                <tr>
                                    <th>Country</th>
                                    <td><form:input path="country"/></td>
                                </tr>
                                <tr>
                                    <th>Zip Code</th>
                                    <td><form:input path="zipcode"/></td>
                                </tr>
                            </table>
                                <button style="    margin-left: 300px;margin-right: 70px;">Update Profile</button>
                        </form:form>
                    </c:if>
                </div>
            </article>
        </section>
    </body>
</html>