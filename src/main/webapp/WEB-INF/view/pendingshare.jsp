 
<!DOCTYPE HTML>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1247px;margin-top: 16px;padding-top: 23px;color: #000;"><a href="/veepropbeta/logout">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks[${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li>Profile</li>
                        <li>Edit Profile</li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    <div>
                        <div >Pending Share Transaction. Take an Action</div>
                        <div style="float: right; margin-top: -25px; margin-right: 25px;">Current Price : ${currentLot.pricePerShare} &nbsp;&nbsp;&nbsp; Share Available : ${currentLot.numberOfShare}</div>
                    </div>
                </h3>
                    <center>
                    <div>
                    <table style="text-align: center; border: #000 solid 1px;" >
                        <thead style="border: #000 solid 1px;">
                            <th style="width: 100px;">Transaction Id</th>
                            <th style="width: 150px;">Seller</th>
                            <th style="width: 150px;">Buyer</th>
                            <th style="width: 100px;">Status</th>
                            <th style="width: 150px;">Transaction Date</th>
                            <th style="width: 100px;">Share</th>
                            <th style="width: 100px;">price</th>
                        </thead>
                        <tbody style="text-align: center; border: #000 solid 1px;">
                            <c:if test="${empty pendingSTT}">
                            <tr style="text-align: center; border: #000 solid 1px;">
                                <td colspan="7">No Transaction pending</td>
                            </tr>
                            </c:if>                            
                            <c:forEach var="stt" items="${pendingSTT}">
                                <tr style="text-align: center; border: #000 solid 1px;">
                                    <td>${stt.transactionId}</td>
                                    <td>${stt.sellerName}</td>
                                    <td>${stt.buyerName}</td>
                                    <td>${stt.transactionStatus}</td>
                                    <td>${stt.transactionDate}</td>
                                    <td>${stt.numberOfShare}</td>
                                    <td>${stt.costPerShare}</td>
                                </tr>
                            </c:forEach>
                        <tbody>
                            
                        </tbody>
                    </table>
                    </div>
                    </center>
                
                <br class="clear"/>
            </article>
        </section>
    </body>
</html>