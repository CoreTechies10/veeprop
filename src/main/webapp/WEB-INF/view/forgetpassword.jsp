
        <!DOCTYPE HTML>
        <%@page contentType="text/html" pageEncoding="windows-1252"%> 
        <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

           <html>
             <head>
               <meta charset="UTF-8">
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Sign In</title>
               
                <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
                <script src="/veepropbeta/js/modernizr.js"></script>
                <script src="/veepropbeta/js/respond.min.js"></script>
                <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
                <script src="/veepropbeta/js/lightbox.js"></script>
                <script src="/veepropbeta/js/prefixfree.min.js"></script>
                <script src="/veepropbeta/js/jquery.slides.min.js"></script>
                <style type="text/css">
                    .error{
                        font-weight: bold;
                        color: red;
                    }
                </style>
           </head>

                <body>
                     <%@include file="header.jsp" %>
                     <%@include file="Lotinformation.jsp" %>
             
                   <div class="container-fluid">
                   <div class="span5" style="margin-left:25%;">
                   <a href="/veepropbeta/login" class="list-group-item active">Reset your password</a>
                   
                <form:form commandName="resetpassword" action="/veepropbeta/forgetpassword" method="POST">
                <center>
                    <span class="error">${message}</span>
                </center>
                <div class="form-group">
                <form:label for="inputEmail3" path="email" class="col-sm-2 control-label">Email</form:label>
                <div class="col-sm-10">
                    <form:input path="email" class="form-control" id="inputEmail3" placeholder="Please Enter your Email "/>
                    <form:errors path="email" element="div" cssClass="error"/>
                </div>
                </div>
                
                <span class="clearfix"></span>
                
                <div class="form-group">
                <form:label for="questionId" path="questionId" class="col-sm-2 control-label">Security Question</form:label>
                <div class="col-sm-10">
                <form:select path="questionId" class="form-control" id="questionId">
                    <form:option value="0"> -- Select Security Question--</form:option>
                    <c:forEach var="question" items="${security}">
                    <form:option value="${question.securityQuestionId}">${question.securityQuestion}</form:option>
                    </c:forEach>
                </form:select>
                    <form:errors path="questionId" element="div" cssClass="error"/>
                </div>
                </div>
                
                <span class="clearfix"></span>
                
                <div class="form-group">
                <form:label for="answer" path="answer" class="col-sm-2 control-label">Answer</form:label>
                <div class="col-sm-10">
                    <form:input path="answer" class="form-control" id="answer" placeholder="Please Enter your Answer"/>
                    <form:errors path="answer" element="div" cssClass="error"/>                    
                </div>
                </div>
                 
                <div class="form-group" >
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-danger" style="margin-top: 20px;">Reset</button>                        
                    </div>
                </div>
                </form:form>

                </div>
                 
                 </div>
<div style="height: 30px;"></div>
          <%@include file="footer.jsp" %>
       </body>
           </html>