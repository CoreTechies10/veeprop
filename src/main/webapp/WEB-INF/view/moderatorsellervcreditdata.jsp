
<!DOCTYPE HTML>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li>Profile</li>
                        <li>Edit Profile</li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    Pay to the seller
                </h3>
                <form:form commandName="seller" action="/veepropbeta/vCreditPurchaseRequest" method="POST" enctype="multipart/form-data">
                    <div style="width: 300px;float: left;"> Seller Name : </div><div style="width: 200px; float: left;">${seller.name}</div><form:hidden path="name"/><br />
                    <div style="width: 300px;float: left;">Seller Email : </div><div style="width: 200px;float: left;">${seller.email}</div><form:hidden path="email"/><br />
                    <div style="width: 300px;float: left;">Seller Contact:</div><div style="width: 200px;float: left;"> ${seller.contact}</div><form:hidden path="contact"/><br />
                    <div style="width: 300px;float: left;">Seller's Bank  :</div> <div style="width: 200px;float: left;">${sellerBankDetail.bankName}</div><form:hidden path="contact"/><br />
                    <div style="width: 300px;float: left;">Seller's Bank Account Number : </div><div style="width: 200px;float: left;">${sellerBankDetail.accountNumber}</div><form:hidden path="contact"/><br />
                    <div style="width: 300px;float: left;">vCredit : </div><div style="width: 200px;float: left;">${seller.vCredit}</div><form:hidden path="vCredit"/> <br />
                    <div style="width: 300px;float: left;">Bank Transaction Id :</div><form:input path="bankTransactionId" required="true" cssStyle="border: 1px red solid;"/> <br/>
                    <div style="width: 300px;float: left;">Proof of Transaction : </div><input type="file" value="file" name="file" required="true" style="margin-right: -60px;"/> <br/>
                            <form:hidden path="shareTransactionId"/>
                            <form:hidden path="identificationNumber"/>
                    <button style=" margin-left: 300px; ">purchase</button>
                </form:form>
                <br class="clear"/>
            </article>
        </section>
    </body>
</html>