
<!DOCTYPE HTML>
<%@page contentType="text/html" pageEncoding="windows-1252"%> 
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<html>
     <head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home</title>

        <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <link href="/veepropbeta/js/bootstrap.css" rel="stylesheet" type="text/css">
       
   </head>
        
        <body>
            <c:set var="loginValue" value="false" />
             <%@include file="header.jsp" %>
             <%@include file="Lotinformation.jsp" %>

             

             <div class="col-md-12">
            <div id="myCarousel" class="carousel slide homCar">
		<div class="carousel-inner" style="border-top:18px solid #000; border-bottom:1px solid #222; border-radius:4px;">
		  <div class="item active">
                      <img src="/veepropbeta/img/slider2.jpg" alt="#" style="height: 300px; width: 100%; "/>
			<div class="carousel-caption">
                           <h4>How To Invest in Share Market</h4>
	                    <p>No matter how big and how small your business is. We are giving the best solution for your best value of money.</p>
			</div>
		  </div>
		  <div class="item">
                      <img src="/veepropbeta/img/slider1.jpg" alt="#" style="height:300px;width: 100%; "/>
			<div class="carousel-caption">
	                 <h4>How To Invest in Share Market</h4>
                          <p>No matter how big and how small your business is. We are giving the best solution for your best value of money.</p>
			</div>
		  </div>
		  <div class="item">
                      <img src="/veepropbeta/img/slider3.jpg" alt="#" style="height:300px; width: 100%;"/>
			<div class="carousel-caption">
		         <h4>How To Invest in Share Market </h4>
		          <p>No matter how big and how small your business is. We are giving the best solution for your best value of money.</p>
		      </div>
		  </div>
		</div>
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">~</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">~</a>
       </div>
 </div>
             <div class="row">
             <div class="col-md-5">
                 <a href="" class="list-group-item active"><b>About Veeprop</b></a>
                 <a  class="list-group-item "><p>Veeprop is a conceptualised virtual property game which aims to teach the players concepts
                                                of investing by having their in game credit work for them in a risk free setting while simultaneously
                                                increasing their financial literacy and stressing the imperative nature of accountability.<br>
                                                The online game is based on a simplified financial and economic simulation environment.
                                                This is neither an investment related company nor any brokerage firm. 
                                                Players must establish that their participation in Veeprop game is lawful
                                                in their own jurisdiction. <br>
                                                Player's account(s) should not be managed by third-party in any means. 
                                                Players that do not agree with the game regulations are advised to stop playing immediately.
                                                We would like to take this opportunity to thank you for your support and understandings.
</p></a>
             </div>
             <div class="col-md-4 offset">
                <a href="" class="list-group-item active" ><b>Latest News</b></a>
                <a  class="list-group-item"><b>Veeprop Game has been Launched</b>
                    <p>Veeprop is a conceptualised virtual property game which aims to teach the players concepts
                                                of investing by having their in game credit work for them in a risk free setting while simultaneously
                                                increasing their financial literacy and stressing the imperative nature of accountability.<br>
                                                The online game is based on a simplified financial and economic simulation environment.
                                                This is neither an investment related company nor any brokerage firm. 
                                                Players must establish that their participation in Veeprop game is lawful
                                                in their own jurisdiction.</p></a>
             </div>
             <div class="col-md-3 offset">
               <a href="" class="list-group-item active"><b>Admin updates</b></a>
               <a class="list-group-item"><b>IPO has been launched....!!!</b></a>
             </div>
         </div>
          

         
             
  <%@include file="footer.jsp" %>
   <script src="/veepropbeta/js/jquery-1.8.3.min.js"></script>
    <script src="/veepropbeta/js/bootstrap.min.js"></script>

</body>
   </html>