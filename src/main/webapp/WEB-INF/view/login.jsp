
        <!DOCTYPE HTML>
        <%@page contentType="text/html" pageEncoding="windows-1252"%> 
        <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

           <html>
             <head>
               <meta charset="UTF-8">
               <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Sign In</title>
               <style>
                    label.valid {
                    width: 24px;
                    height:10px;
                    background: url() center center no-repeat;
                    display: inline-block;
                    text-indent: -9999px;}
                    label.error {
                    font-weight:bolder;
                    color: red;
                    padding: 5px 5px;
                    margin-top: 3px;}
            </style>
                <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
                <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
                <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
                <script src="/veepropbeta/js/modernizr.js"></script>
                <script src="/veepropbeta/js/respond.min.js"></script>
                <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
                <script src="/veepropbeta/js/lightbox.js"></script>
                <script src="/veepropbeta/js/prefixfree.min.js"></script>
                <script src="/veepropbeta/js/jquery.slides.min.js"></script>
                <script>
                $(document).ready(function(){
                $('#login-form').validate( {
                rules: {
                userName: {
                minlength:5,
                required: true
                },
                password: {
                minlength:5,
                required: true
                }
                
              },
                highlight: function(element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
                },
                success: function(element) {
                element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
                }
                });
                }); 
    </script>
           </head>

                <body>
                     <%@include file="header.jsp" %>
                     <%@include file="Lotinformation.jsp" %>
                <center>
                   <div class="container-fluid">
                   <div class="span5 offset4">
                   <a href="/veepropbeta/login" class="list-group-item active">Sign In Here</a>
                   
                   <form:form commandName="login" action="/veepropbeta/loginSubmit" method="POST" id="login-form" class="form-horizontal">
                       
                    <div class="form-group">
                        <form:label for="inputEmail3" path="userName" class="col-sm-3 control-label">Login ID</form:label>
                      <div class="col-sm-9">
                        <form:input path="userName" class="form-control" id="userName" placeholder="Login ID"/>
                      </div>
                    </div>
                      
                    <div class="form-group">
                        <form:label path="password" class="col-sm-3 control-label">Password</form:label>
                      <div class="col-sm-9">
                        <form:password path="password" class="form-control" id="password" placeholder="Password"/>
                      </div>
                    </div>
                       
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <a href="/veepropbeta/forgetpassword" style="margin-left:15%; float: left;">Reset your password</a>                          
                        <button type="submit" class="btn btn-primary">Sign in</button>
                      </div>
                    </div>
                   </form:form>

                </div>
                 
                 </div>
                </center>
                     <div style="height: 30px;"></div>
          <%@include file="footer.jsp" %>
       </body>
           </html>