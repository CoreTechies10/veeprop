
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }


    </style>
    <script type="text/javascript">
        $(document).ready(function (e){
            $("#first").show();
            $("#second").hide();
            $("#third").hide();
        });
        function change(item){
            $("#first").hide();

            $("#second").hide();
            $("#third").hide();
            $("#"+item).show();
        }
    </script>
</head>

<body>
    <input type="hidden" id="notification-form" />   
    <%@include file="header.jsp" %>
    <%@include file="information.jsp" %>
    <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach> 
    <div class="container-fluid" style="margin-top: -10px;">
        <div class="row-fluid">
        <%@include  file="sellboard.jsp" %>
        <div class="span6">
              <ul class="nav nav-pills">
                <li><a href="/veepropbeta/home">Dashboard</a></li>
                 <li><a href="/veepropbeta/profile">Profile</a></li>
                 <li><a href="/veepropbeta/wallet">Wallet</a></li>
                 <li><a href="/veepropbeta/business">Biz Management</a></li>
                 <li><a href="/veepropbeta/feedback">Feedback</a></li>
                  <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
              </ul>
           
            <span class="list-group-item active" style="margin-top:2%;background-color: #428BCA;" onclick="return change('first');"><b style="color: #fff;">Your Personal Details </b>
                <b style="margin-left:70%;"><a href="/veepropbeta/updatepersonaldetail" style="color: #fff;">Edit</a></b></span>
                
                
            <div id="first">
                <div class="col-md-4 offset"><b>GAMER ID</b></div>
                <div class="col-md-3 offset4"><p>${us.identificationNumber} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Full Name</b></div>
                <div class="col-md-3 offset4"><p>${us.name} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Email ID:</b></div>
                <div class="col-md-3 offset4"><p>${us.email}&nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Mobile Number:</b></div>
                <div class="col-md-3 offset4"><p>${us.contact}&nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Gender</b></div>
                <div class="col-md-3 offset4">
                    <p>
                        <c:if test="${us.gender==0}">Male</c:if>
                        <c:if test="${us.gender==1}">Female</c:if>
                    </p>
                </div>
                
                <div class="col-md-4 offset"><b>Date Of Birth:</b></div>
                <div class="col-md-3 offset4"><p>${us.dateOfBirth} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Passport Number:</b></div>
                <div class="col-md-3 offset4"><p>${us.passportNo} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Screen Name:</b></div>
                <div class="col-md-3 offset4"><p>${us.screenName}&nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Date Join:</b></div>
                <div class="col-md-3 offset4"><p> ${us.registrationDate} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Country:</b></div>
                <div class="col-md-3 offset4"><p> ${us.residentCountry} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Office Tel No:</b></div>
                <div class="col-md-3 offset4"><p> ${us.officeTelNo} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Home Tel No:</b></div>
                <div class="col-md-3 offset4"><p> ${us.homeTelNo} &nbsp; </p></div>
                
                <div class="col-md-4 offset"><b>Fax No:</b></div>
                <div class="col-md-3 offset4"><p> ${us.faxNo} &nbsp; </p></div>

            </div>
                <div class="clearfix"></div>
                <span class="list-group-item active" style="margin-top: 2%;background-color: #428BCA;" onclick="return change('second');"><b style="color: #fff;">Your Address Details</b> 
               
                    <b style="margin-left:70%;"><a href="/veepropbeta/updatePersonalInformation" style="color: #fff;">  Edit</a></b></span>
            <div id="second">
                <div class="col-md-4 offset"><b>House Number</b></div>
                <div class="col-md-3 offset4"><p>${usradd.houseNumber} &nbsp; </p></div>
                
                <div class="col-md-4 offset" ><b>Street Name1:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.streetName1} &nbsp; </p></div>

                <div class="col-md-4 offset"><b>Street Name2:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.streetName2} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>City:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.city} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Landmark</b></div>
                <div class="col-md-3 offset4"><p>${usradd.landmark} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>State:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.userstate} &nbsp; </p></div>
                
                <div class="col-md-4 offset"><b>Country:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.country} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Zip Code:</b></div>
                <div class="col-md-3 offset4"><p>${usradd.zipcode} &nbsp; </p></div>
            
                <div class="clearfix"></div>
            </div>
                <span class="list-group-item active" style="margin-top:1%;background-color: #428BCA;"  onclick="return change('third');"><b style="color: #fff;">Your Account Details</b>
                    <b style="margin-left: 70%;">  <a href="/veepropbeta/updatebankinfo" style="color: #fff;">Edit</a></b></span>
            <div id="third">
                    
                <div class="col-md-4 offset"><b>Bank Name</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.bankName} &nbsp; </p></div>
            
                <div class="col-md-4 offset"><b>Account Number:</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.accountNumber} &nbsp; </p></div>

                <div class="col-md-4 offset"><b>IFSC Code:</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.iFSC} &nbsp; </p></div>
                
                <div class="col-md-4 offset"><b>Account Type:</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.accountType} &nbsp; </p></div>
                
                <div class="col-md-4 offset"><b>Bank Address</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.bankAddress} &nbsp; </p></div>
                
                <div class="col-md-4 offset"><b>Branch Name</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.branch} &nbsp; </p></div>
             
                <div class="col-md-4 offset"><b>Swift Code</b></div>
                <div class="col-md-3 offset4"><p>${usrbank.swift} &nbsp; </p></div>
            </div>
            
            
          </div>
            <%@include  file="noticeboard.jsp" %>
                 
        </div>
    </div>
  
 
    <%@include file="footer.jsp" %>
    

</body>
</html>
