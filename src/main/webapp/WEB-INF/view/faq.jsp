
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : Arvind
--%>
 <%@page contentType="text/html" pageEncoding="windows-1252"%> 
 <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>Frequently Asked Question</title>
   <style>
   label.valid {
   width: 24px;
   height:10px;
   background: url() center center no-repeat;
   display: inline-block;
   text-indent: -9999px;}
   label.error {
   font-weight:bolder;
   color: red;
   padding: 5px 5px;
   margin-top: 3px;}
  </style>
  
  
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    
 </head>
  
<body>
    <%@include file="header.jsp" %>
    <%@include file="Lotinformation.jsp" %>
    
    
    
       <div class="row">
    <a  class="list-group-item active"><b><center>Frequently Asked Questions</center></b></a>
    
           
               
                    <c:forEach var="faq" items="${faq}" varStatus="status">
                    
                        <a  class="list-group-item ">
                            <p><strong>Question:${status.count}</strong> ${faq.question}</p>
                            <p><strong> Answer :</strong> ${faq.answer}</p>
                            
                        </a>
                   
                   
                  
                    
                    </c:forEach>
       </div>
    <%@include file="footer.jsp" %>      
</body>
</html>