
<!DOCTYPE HTML>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function(e){
                $( "#datepickerto" ).datepicker({dateFormat: 'yy-mm-dd' ,maxDate:"new Date()"});
                $( "#datepickerfrom" ).datepicker({dateFormat: 'yy-mm-dd',maxDate:"new Date()"});
            });
        </script>        
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li>Profile</li>
                        <li>Edit Profile</li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    Generate Report for user ${name}
                </h3>

                <br class="clear"/>
                <div style="  margin-top: 20px; ">
                <form:form method="POST" action="/veepropbeta/generate/report" commandName="transaction">
                    <form:hidden path="userIN" />
                    <div style="float: left;">Transaction Type</div>
                    <div style="float: left;">
                        <form:radiobutton path="transactionType" value="0" />V-CREDIT TRANSACTION <br />
                        <form:radiobutton path="transactionType" value="1"/>SHARE TRADING TRANSACTION <br/>
                        <form:radiobutton path="transactionType" value="2"/>BONUS TRANSACTION<br/>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <div style="float: left;">
                        <span style="width: 60px;">From Date</span>
                        <form:input path="initialDate" id="datepickerto" cssStyle="border : 1px red solid;"></form:input>
                    </div>
                    
                    <div >
                        <span style="width: 60px;">to Date</span>
                        <form:input path="endDate" id="datepickerfrom" cssStyle="border : 1px red solid;"></form:input>
                    </div>
                        
                    <button style="margin-left: 200px;">Filter Transaction History</button>
                </form:form>
                </div>
            </article>
        </section>
    </body>
</html>