
<%-- 
    Document   : contact
    Created on : Jan 29, 2014, 4:39:42 PM
    Author     : CoreTechies
--%>
  <%@page contentType="text/html" pageEncoding="windows-1252"%> 
  <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<html>
 <head>
  <title>About Us</title>
     <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
     <link href="/veepropbeta/css/lightbox.css" rel="stylesheet" />
     <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
     <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
    
 </head>      
   <body>
      <%@include file="header.jsp" %>
      <%@include file="Lotinformation.jsp" %>
     
     <div class="row">
       <div class="col-md-8">
       <a  class="list-group-item active"><b>V-Credit Transaction Terms & Conditions</b></a>
       <a class="list-group-item ">
           <p>
           <ol>
               <li>The Company held no responsibility for any legal dispute due to error arising from any online payment or face to face transactions (e.g. exchange wrong amount, wrong card number, fake currency and etc) dealing between willing buyer and willing seller.	</li>
               <li>The Company is not responsible to date recovery or any losses arise from error due to individual�s mistake such as choosing wrong order, internet connection interruption and etc.		</li>
               <li>Any losses or disputes of V-Credit happened in the "V-credit buy/sell market" shall carry no liability to the Company. The Company is not reliable and responsible for any legal issues. The Company will only provide assistance in order to trace back the trail of transaction to solve the dispute to best possible outcome. Note: Kindly provide all the transaction slip, such as bank slip / receipt (MUST in color scan) by email to customer service, other than this will not accepted.</li>
               <li>All personal information such as personal phone number, bank account number, bank name, bank branch name, bank regional, international swift code and etc are correct. Inaccurate of information given by seller or buyer will lead to unsuccessful payment. The Company will not be liable for any losses arising from this inaccurate information.</li>
               <li>Buying V-credit transaction is restricted to 6 star package / 5000 AP. Maximum purchase of V-credit is 5000 V-credit for each transaction. The next transaction is allow once the first transaction has completed. Purchasing of V-credit is restricted within the same country ONLY.	</li>
               <li>Buyer must has at least 1 V-credit in his/her V-credit Wallet before purchasing V-credit from the market.</li>
               <li>Buyer will receive his/her V-credit purchased in V-credit wallet.</li>
               <li>Once buyer confirmed the purchase, buyer is given 3 WORKING DAYS to complete the payment transaction to Seller. Failure of doing this, will receive a warning letter for FIRST CONDUCT from the Company, a PENALTY of 100 V-credit FOR SECOND MISCONDUCT and accounts will be FROZEN PERMANENTLY for the THIRD ATTEMPT.	</li>
               <li>Upon completion of payment, buyer MUST click on �confirm payment bank in� in the system and upload the COLOURED copy of evidence. If buyer intentionally clicked on �confirm payment bank in� without any payment made to seller, The Company reserved its right to freeze accounts until the payment is made to the seller. Account will be unfrozen when issue is settled with the customer service.</li>
               <li>Once buyer has confirmed payment bank-in, seller will receive a �E-mail/SMS notification�. Seller is urged to CHECK his /her bank account IMMEDIATELY to confirm the payment. Once confirmed, seller has to click �Approve� in the system, otherwise the Company will not be reliable for any losses and complaints are not be entertained.	</li>
               <li>Seller is given 3 working days to click �Approve� in the system from the day payment is received or SMS notification is received. V-credit will be transfered to the buyer account in the event the seller has no action on this. Failure to this, The Company reserved its right to freeze the accounts from any transactions in V-credit trading market.	</li>
               <li>The seller has his/her right not to �Approve� the sales & purchase of M Credit transaction if payment is not received arising from wrong amount of payment from buyer or any other matters. The Company has no responsible for any losses arising from human error. Please contact the customer service IMMEDIATELY when the discrepancy arises.	</li>
               <li>Company does not allow any V-credit transaction done by third party or artificial intelligence program, as it only restricted to personal control. Once this has been found out, Company will take the action to freeze the account until the completion of investigations.</li>
               <li>Contact number of buyers and sellers must be correct and reachable. Failure to this, both buyers and sellers are unable to receive SMS notification on the sell and buy V-credit transaction. If Company found out that any of fake number by intention, the Company reserved its right to freeze the accounts for further investigation</li>
               <li>Company will take action against buyers or sellers if Company found any fraudulent cases, improper trading and etc by temporary freezing of accounts and buyers or sellers will be prohibited from any trading transactions in V-credit trading market	</li>
               <li>The Company reserved its right to change, amend and remove all the above terms & conditions without any early notice given</li>
               <li>To maintain a high level of security to protect the Player's funds, the Company performs random security checks. The Player hereby accepts that the Company maintains the right to demand additional documentation in order to verify the Player as the account holder in the event of such a security check.</li>
               <li>The Player has the responsibility for checking the available amount in their Account ("V-credit" / "IGC") before or after each transaction and prior to departing of any action. If the Player suspects any errors, they must report it immediately to the Company. Failure to do so will result in the Player waiving their rights to raise future disputes and acceptance of all previous initiated records as true and correct.</li>
               <li>Unless otherwise stated in these terms and conditions, no transaction can be cancelled once confirmed by the Website</li>
           </ol>
           </p>
       
       </a>
      </div>
      </div>   
      <%@include file="footernew.jsp" %>      
</body>
</html>