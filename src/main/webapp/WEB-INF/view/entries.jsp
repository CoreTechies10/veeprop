
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty users}"> <tr><td colspan="8">No user found</td></tr></c:if>
<c:forEach var="item" items="${users}">
    <tr>
        <td> ${item.identificationNumber} </td>
        <td> ${item.name} </td>
        <td> ${item.email} </td>
        <td> <c:if test="${item.gender==0}">male</c:if><c:if test="${item.gender==1}">female</c:if> </td>
        <td> ${item.contact} </td>
        <td> <c:if test="${item.userStatus==0}">Active</c:if><c:if test="${item.userStatus==1}">Blocked</c:if> </td>
        <td> ${item.registrationDate} </td>
        <td> <c:if test="${item.userStatus==0}"><a href="/veepropbeta/trader/block/${item.identificationNumber}">Block </a></c:if>
            <c:if test="${item.userStatus==1}">Blocked</c:if>
        </td>
        <td> <a href="/veepropbeta/generate/report/${item.identificationNumber}">Report</a> </td>
    </tr>
</c:forEach>