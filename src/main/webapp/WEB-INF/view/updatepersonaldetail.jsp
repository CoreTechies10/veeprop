
<%-- 
    Document   : profile
    Created on : 11 Feb, 2014, 3:02:47 PM
    Author     : Core Techies
--%>

  <%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
   <!DOCTYPE HTML>
   <html>
    <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/veepropbeta/css/style.css" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.min.css" type="text/css" media="screen" />
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/veepropbeta/css/style_light.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="/veepropbeta/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="/veepropbeta/js/demo.js" type="text/javascript"></script>      
    <style type="text/css">
        .tooltip {
            width: 250px;
            font-size: 11px;
            font-family: Arial, sans-serif;
            background: #444;
            border: 1px solid #090909;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            position: absolute;
            z-index: 1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
            color:#fff;
            padding:12px 24px;
            line-height:18px;
        }

        .tooltip:after {
            content: '';
            position: absolute;
            border-color: transparent  #444 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .tooltip:before {
            content: '';
            position: absolute;
            border-color:   transparent #090909 transparent transparent;
            border-style: solid;
            border-width: 10px;
            height: 0;
            width: 0;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
            margin-left: -20px;
        }

        .ttw-notification-menu{
                width: 276px;
        }
        .error{
                color: red;
                font-weight: bold;
            }


    </style>
</head>

     <body>
        <input type="hidden" id="notification-form" />   
       <%@include file="header.jsp" %>
       <%@include file="information.jsp" %>
        <c:forEach var="notification" items="${notification}" varStatus="status">
        <script type="text/javascript">
            $(document).ready(function(e){
            $("#notification-form").val('<a href="${notification.notificationLink}/${notification.notificationId}">${notification.notification}</a>');
            $("#notification-form").click();
        });
        </script>
    </c:forEach> 
          
         <div class="container-fluid" style="margin-top: -10px;">
           <div class="row-fluid">
             <%@include  file="sellboard.jsp" %>
              <div class="span6">
                <ul class="nav nav-pills">
                   <li><a href="/veepropbeta/home">Dashboard</a></li>
                    <li><a href="/veepropbeta/profile">Profile</a></li>
                    <li><a href="/veepropbetawallet">Wallet</a></li>
                    <li><a href="/veepropbeta/business">Biz Management</a></li>
                    <li><a href="/veepropbeta/feedback">Feedback</a></li>
                     <li><a id="projects" class="notification-menu-item first-item list_inline" style="float: right;">Notifications</a></li>
               </ul>
               <a href="#" class="list-group-item active" style="margin-top: 2%;">Your Personal Details <b style="margin-left: 60%;">Update</b></a>
               
                <form:form action="/veepropbeta/updatepersonaldetail"  class="form-horizontal" method="POST" modelAttribute="updatepersonal"  enctype="multipart/form-data">
                   <div class="form-group">
                   <form:label for="inputEmail3" path="name" class="col-sm-3 control-label">Name</form:label>
                   <div class="col-sm-8">
                   <form:input path="name" class="form-control" id="name" placeholder="Enter your Name"/>
                   <form:errors path="name" cssClass="error" element="span"/>
                   </div>
                   </div>
                   
                   <form:hidden path="email"/>
                   <div class="form-group">
                   <form:label for="inputEmail3" path="screenName" class="col-sm-3 control-label">Screen Name:</form:label>
                   <div class="col-sm-8">
                   <form:input path="screenName" class="form-control" id="name" placeholder="screenName"/>
                   <form:errors path="screenName" element="span" cssClass="error"/>
                   </div>
                   </div>
                
                   <div class="form-group">
                   <form:label for="inputEmail3" path="passportNo" class="col-sm-3 control-label">Passport Number</form:label>
                   <div class="col-sm-8">
                   <form:input path="passportNo" class="form-control" id="name" placeholder="passport No"/>
                   <form:errors path="passportNo" element="span" cssClass="error"/>
                   </div>
                   </div>
                 
                   <div class="form-group">
                   <form:label for="inputEmail3" path="officeTelNo" class="col-sm-3 control-label">Office Tel No</form:label>
                   <div class="col-sm-8">
                   <form:input path="officeTelNo" class="form-control" id="name" placeholder="Office Tel No"/>
                   <form:errors path="officeTelNo" cssClass="error" element="span"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="homeTelNo" class="col-sm-3 control-label">Home Tel No</form:label>
                   <div class="col-sm-8">
                   <form:input path="homeTelNo" class="form-control" id="name" placeholder="Home Tel No"/>
                   <form:errors path="homeTelNo" cssClass="error" element="span"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="contact" class="col-sm-3 control-label">Contact Number</form:label>
                   <div class="col-sm-8">
                   <form:input path="contact" class="form-control" id="name" placeholder="Contact Number"/>
                   <form:errors path="contact" element="span" cssClass="error"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="faxNo" class="col-sm-3 control-label">Fax Number</form:label>
                   <div class="col-sm-8">
                   <form:input path="faxNo" class="form-control" id="name" placeholder="Fax Number"/>
                   <form:errors path="faxNo" element="span" cssClass="error"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="residentCountry" class="col-sm-3 control-label">Country</form:label>
                   <div class="col-sm-8">
                   <form:select path="residentCountry" class="form-control" >
                      <form:option value="">-Select Country Here</form:option>
                      <form:option value="1">INDIA</form:option>
                      <form:option value="2">US</form:option>
                      <form:option value="3">AUS</form:option>
                      <form:option value="4">BRAZIL</form:option>
                      <form:option value="5">NEW</form:option>
                   </form:select>
                   <form:errors path="residentCountry" cssClass="error" element="span"/>
                   </div>
                   </div>
                  
                   <div class="form-group">
                   <form:label for="inputEmail3" path="securityQuestionId" class="col-sm-3 control-label">Security Question</form:label>
                   <div class="col-sm-8">
                    <form:select path="securityQuestionId" class="form-control" >
                        <form:option value="">--Please Select Security Question--</form:option>
                      <c:forEach var="security" items="${securityQuestions}">
                      <form:option value="${security.securityQuestionId}" >${security.securityQuestion}</form:option>
                      </c:forEach>
                   </form:select>
                   <form:errors element="span" cssClass="error" path="securityQuestionId"/>
                   </div>
                 </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="securityAnswer" class="col-sm-3 control-label">Security Answer</form:label>
                   <div class="col-sm-8">
                   <form:input path="securityAnswer" class="form-control" id="name" placeholder="Name"/>
                   <form:errors path="securityAnswer" element="span" cssClass="error"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="gender" class="col-sm-3 control-label">Gender</form:label>
                   <div class="col-sm-8">
                   <form:radiobutton path="gender" value="0"/>Male
                   <form:radiobutton path="gender" cssStyle="margin-left:10%;" value="1"/>Female
                   <form:errors path="gender" element="span" cssClass="error"/>
                   </div>
                   </div>
                   
                   <div class="form-group">
                   <form:label for="inputEmail3" path="dateOfBirth" class="col-sm-3 control-label">Date Of Birth</form:label>
                   <div class="col-sm-8">
                   <form:input path="dateOfBirth" class="form-control" id="name" placeholder="DOb"/>
                   <form:errors path="dateOfBirth" element="span" cssClass="error"/>
                   </div>
                   </div>
                
                   <div class="form-group">
                   <form:label for="inputEmail3" path="image" class="col-sm-3 control-label">Upload Image</form:label>
                   <div class="col-sm-8">
                   <input type="file" value="file" name="file" style="margin-left:2%;"/>
                   </div>
                 </div>
                  <div class="form-group" style="margin-bottom:4%;">
                       
                  <div class="col-sm-2 offset3">
                  <form:button  class="btn btn-success">Update</form:button>
                  </div>
                  <div class="col-sm-2">
                   <form:button type="button" class="btn btn-danger">Cancel</form:button>
                   </div>
                  </div>
           </form:form>
        </div>
            <%@include  file="noticeboard.jsp" %>
     </div>
  </div>
  <%@include file="footer.jsp" %>
    
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('.input-daterange').datepicker({
                    todayBtn: "linked"
                });
            
            });
        </script>
    
</body>
   
</html>
</html>
