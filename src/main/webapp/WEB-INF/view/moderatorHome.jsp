
<!DOCTYPE HTML>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Veeprop</title>
        <link rel="stylesheet" href="/veepropbeta/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/style_admin.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />   
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            function searchTrader(){
                var x = $("#search").val();
                var url ="/veepropbeta/trader/"+x;
                $.get(url, function(data){
                   if(data){
                      $("#tbody").html(data);
                      $("#tableData").show();
                   }else{
                       alert("No user found with "+x);
                   } 
                });
            }
            $(document).ready(function(e){
               $("#tableData").hide(); 
            });
        </script>
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
            }
            table tr {
                border: 1px black solid;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <section id="spacer">
            <p>Veeprop Moderator Panel</p>
            <div style="    margin-left: 1200px;margin-top: 16px;padding-top: 20px;color: #000;"><a href="/veepropbeta/logout" style="margin-left: 30px;">Sign Out</a></div>
        </section>
        <section id="text_column_admin">
            <section id="boxcontent_admin">
                <div class="sell_board"><span class="sb">Moderator Tasks [${name}]</span></div>
                <div class="ctg"><span class="ctg_p">Daily Task</span></div>  
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/home">Overview</a></li>
                        <li><a href="/veepropbeta/pendingshare">View Share Transaction</a></li>
                        <li><a href="/veepropbeta/pendingvcredit">Pending vCredit Transaction</a></li>
                        <li><a href="/veepropbeta/pendingprofiles">Pending Profile Upgrade Request</a></li>
                        <li><a href="/veepropbeta/introduce">Binary Tree</a></li>
                        <li><a href="/veepropbeta/home">Block Player</a></li>
                        <li><a href="">Complaints</a></li>
                    </ul>
                </article>
                
                <div class="ctg"><span class="ctg_p">Moderator Profile</span></div>
                
                <article class="link_grp">
                    <ul>
                        <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                        <li><a href="/veepropbeta/moderator/profile/1">Edit Profile</a></li>
                        <li>Report Problem</li>
                    </ul>
                </article>
                
                <br class="clear"/>
            </section>
            
            <article class="column1">
                <h3>
                    Block Player and Generate Report
                </h3>
                <div style="width: 350px;float: left; font-size: 20px; margin-top: 5px;">Search a Trader By Name or Email</div><input type="text" style="border: 1px red solid; float: left;" id="search"/><button style="    background-color: #4181FF;border-radius: 17px;font-size: 22px;height: 40px;margin-left: 30px;" onclick="return searchTrader();">Search</button>
                
                <br class="clear"/>
                <div style="  margin-top: 20px; ">
                    <table id="tableData">
                        <tr>
                            <th>Identification</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Contact</th>
                            <th>Status</th>
                            <th>Reg. Date</th>
                            <th>Block</th>
                            <th>Report</th>
                        </tr>
                        <tbody id="tbody">
                            
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
    </body>
</html>