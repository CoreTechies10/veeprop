
<!DOCTYPE HTML>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
    <script>
        function divert()
        {
            var e = document.getElementById("alternative_menu");
            var pageName = e.options[e.selectedIndex].text;
            if(pageName==="Home")
            {window.location.href = "index.jsp";}
            else if(pageName==="About")
            {window.location.href = "about.jsp";}
            else if(pageName==="Contact Us")
            {window.location.href = "/veepropbeta/contact";}
            else if(pageName==="How it works")
            {window.location.href = "/veepropbeta/howitworks";}
            else if(pageName==="Contact Us")
            {window.location.href = "/veepropbeta/makeprofit";}
        }
    </script>    
</head>

<body>
<div class="header_bg">
<header>
    <div style="float: left;">
    <img src="/veepropbeta/img/logo.png"  alt="veeprop" />
    </div>    
    <div  style="float:right; margin-top:2%; margin-right:4%;">
        <c:if test="${loginValue==false}">
                    <a href="/veepropbeta/login"><button class="btn btn-large">Sign In</button></a>
                    </c:if>
                    <c:if test="${loginValue==true}">
                <a href="/veepropbeta/logout"><button class="btn btn-large">Sign Out</button></a>
                    
              </c:if>
        
 </div>
             
          
     
    <select id="alternative_menu" size="1" onchange="divert()">
        <option>Home</option>
        <option>About</option>
        <option>How it works</option>
        <option>make profit</option>
        <option>Contact Us</option>
        <option>FAQ</option>
        
    </select>
        <nav>
            
            <ul>
            <li><a href="/veepropbeta/index">Home</a></li>
            <li><a href="/veepropbeta/aboutUs">About</a></li>
            <li><a href="/veepropbeta/howitworks">How it works</a></li>
            <li><a href="/veepropbeta/makeprofit">Make profit</a></li>
            <li><a href="/veepropbeta/contact">Contact Us</a></li>
            <li><a href="/veepropbeta/faq">FAQ</a></li>
        </ul>
    </nav>
</header>
</div>

</body>
</html>
