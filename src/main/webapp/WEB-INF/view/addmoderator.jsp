
<!DOCTYPE HTML>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Veeprop</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans|Baumans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/veepropbeta/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/veepropbeta/css/bootstrap-responsive.css" type="text/css" media="screen" />   
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <style>
            table tr th, table tbody tr td {
                border: 1px black solid;
                padding: 0 20px;
                text-align: center;
            }
            table tr {
                border: 1px black solid;
                text-align: center;
            }
            table {
                border: 1px black solid;
                text-align: center;
            }
        </style>
        <script type="text/javascript">
            function confirmSubmit(){
                var x = confirm("Click ok to add moderator.")
                if(x){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
</head>
<body style="padding-top: 50px; font-family: sans-serif; color: #999;">
    <div class="container">    
        <div class="navbar navbar-fixed-top" style="padding: 0 20px 15px; background-color: #FDD35F; border-bottom: 1px solid #eea236;">
            <h4 style=" color: #000;">Veeprop Admin Panel</h4>
            <div class="navbar-inner pull-right">
                <ul class="nav nav-pills" style>

                <li><a href="/veepropbeta/home" style=" color: #000;">Dashboard</a></li>
                <li><a href="/veepropbeta/logout" style=" color: #000;">Logout</a></li>
            </ul>
            </div>
        </div>
    </div>
   
    <div class="container-fluid" style="padding-top:10px;">
        <div class="row" style="margin-top: 25px; border-bottom: 1px solid #eeeeee;">
            <div class="span3" style="float: left;border-right: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                <ol class="breadcrumb">
                    <li>Admin Panel</li>
                </ol>

                <div class="navbar">
                  <ul class="nav nav-list">
                      <br class="clear"/>
                      <span class="light_head">Dashboard</span>
                      <hr />
                      <li><a href="/veepropbeta/home">Overview</a></li>
                      <li><a href="/veepropbeta/moderator/report">Reports</a></li>
                      <li><a href="/veepropbeta/moderator/profile/0">Profile</a></li>
                      <li><a href="/veepropbeta/announcements">Announcements</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head"> Moderator</span>
                      <hr />
                      <li><a href="/veepropbeta/addmoderator">Add a Moderator</a></li>
                      <li><a href="/veepropbeta/viewmoderator">Active/Inactive a Moderator</a></li>
                  </ul>
                </div>
                <div class="navbar">
                  <ul class="nav nav-list">
                      <span class="light_head">Analysis</span>
                      <hr />
                      <li><a href="/veepropbeta/admin/feedbacks">Feedbacks</a></li>
                      <li><a href="/veepropbeta/contacts">Contacts</a></li>
                      <li><a href="/veepropbeta/faqadmin">FAQ</a></li>
                  </ul>
                </div>
            </div>
            <div class="span10" style=" overflow:auto; padding: 20px 20px;">
                <center>
                    <h5>Register a Moderator</h5>
                    <hr />
                    <div>
                        <div>${param['success']}</div>
                        <form:form commandName="registration" action="/veepropbeta/addmoderator" method="POST" onsubmit="return confirmSubmit();">
                            <div style="margin: 20px;">
                                <form:label path="name" cssStyle="width:200px;">Name<span style="color: red;">*</span></form:label>
                                <form:input path="name" required="true"/>
                            </div>
                            <div style="margin: 20px;">
                                <form:label path="email" cssStyle="width:200px;">Email<span style="color: red;">*</span></form:label>
                                <form:input path="email" required="true"/>
                            </div>
                            <div style="margin: 20px;">
                                <form:label path="contact" cssStyle="width:200px;">Contact<span style="color: red;">*</span></form:label>
                                <form:input path="contact" required="true"/>
                            </div>
                            <div style="margin: 20px;">
                                <form:label path="country" cssStyle="width:200px;">Country<span style="color: red;">*</span></form:label>
                                <form:select path="country" cssStyle="width:160px;" required="true">
                                    <form:option value="india">India</form:option>
                                    <form:option value="USA">USA</form:option>
                                </form:select>
                            </div>
                            <div style="margin: 20px;">
                                <form:label path="contact" cssStyle="width:200px; margin-right:20px;">Gender<span style="color: red;">*</span></form:label>
                                <form:radiobutton path="gender" value="0" id="male" /><label for="male">Male</label>  &nbsp;&nbsp;&nbsp;&nbsp;
                                <form:radiobutton path="gender" value="0" id="Female"/><label for="male">Female</label>
                            </div>
                            <div style="margin: 20px;">
                                <button>Register Moderator</button>
                            </div>
                        </form:form> 
                    </div>
                </center>
            </div>
        </div>
    </div>
</body>
</html>